/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_COMMON_H
#define LUCCA_COMMON_H

/* Project-wide declarations */

typedef enum {
    LUCCA_SIDE_LEFT=0,
    LUCCA_SIDE_RIGHT=1,
    LUCCA_SIDE_TOP=2,
    LUCCA_SIDE_BOTTOM=3,
} LuccaSide;

#define lucca_side_opposite(side) (side^1)

/* lucca_side_sign: 1 if the coordinates increase in this direction, -1 if they decrease */
#define lucca_side_sign(side) (side&1?1:-1)

/* lucca_side_vertical: returns 1 if this is a vertical direction */
#define lucca_side_vertical(side) (side&2?1:0)

/* lucca_side_horizontal: returns 1 if this is a horizontal direction */
#define lucca_side_horizontal(side) (!lucca_side_vertical(side))

/* lucca_side_perpendicular: returns a perpendicular direction */
#define lucca_side_perpendicular(side) (side^2)

/* lucca_side_coords_le(side, x1, x2): returns 1 if x2 is at least as far in the given direction as x1 */
#define lucca_side_coords_le(side, x1, x2) (lucca_side_sign(side) == 1?x1 <= x2:x1 >= x2)

/* lucca_side_coords_lt(side, x1, x2): returns 1 if x2 is further in the given direction than x1 */
#define lucca_side_coords_lt(side, x1, x2) (lucca_side_sign(side) == 1?x1 < x2:x1 > x2)

/* lucca_side_order_point(side, c1, c2): returns x,y, where c1 is a coordinate in the given direction and c2 is perpendicular */
#define lucca_side_order_point(side, c1, c2) (lucca_side_horizontal(side)?c1:c2),(lucca_side_vertical(side)?c1:c2)


typedef enum {
    LUCCA_STATE_MODAL = 1<<0,
    LUCCA_STATE_STICKY = 1<<1,
    LUCCA_STATE_MAXIMIZED_VIRT = 1<<2,
    LUCCA_STATE_MAXIMIZED_HORZ = 1<<3,
    LUCCA_STATE_SHADED = 1<<4,
    LUCCA_STATE_SKIP_TASKBAR = 1<<5,
    LUCCA_STATE_SKIP_PAGER = 1<<6,
    LUCCA_STATE_HIDDEN = 1<<7,
    LUCCA_STATE_FULLSCREEN = 1<<8,
    LUCCA_STATE_ABOVE = 1<<9,
    LUCCA_STATE_BELOW = 1<<10,
    LUCCA_STATE_DEMANDS_ATTENTION = 1<<11
} LuccaState;

typedef enum
{
    LUCCA_TYPE_NORMAL=1,
    LUCCA_TYPE_DESKTOP,
    LUCCA_TYPE_DOCK,
    LUCCA_TYPE_TOOLBAR,
    LUCCA_TYPE_MENU,
    LUCCA_TYPE_UTILITY,
    LUCCA_TYPE_SPLASH,
    LUCCA_TYPE_DIALOG,
    LUCCA_TYPE_DROPDOWN_MENU,
    LUCCA_TYPE_POPUP_MENU,
    LUCCA_TYPE_TOOLTIP,
    LUCCA_TYPE_NOTIFICATION,
    LUCCA_TYPE_COMBO,
    LUCCA_TYPE_DND,
    LUCCA_TYPE_INTERNAL
} LuccaType;

#endif
