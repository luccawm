/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_LAYOUT_PRIVATE_H
#define LUCCA_LAYOUT_PRIVATE_H

#include "lucca-layout.h"
#include "lucca-rect.h"

/* Private definitons for the Layout module */

struct _LuccaLayout {
    GObject parent;
    /* instance members */
    guint width;
    guint height;
    guint border_size;
    GSList* tiles;
    
    LuccaRect rect;
    
    gboolean dispose_has_run;
    
    /* temporary variables used when resizing */
    GSList* to_resize;
    GSList* to_delete;
};

struct _LuccaLayoutClass {
    GObjectClass parent;
    /* class members */
    guint new_tile_signal_id;
};

struct _LuccaLayoutTile {
    GObject parent;
    /* instance members */
    gboolean valid;
    LuccaLayout* layout;
    LuccaRect rect;
    GdkGeometry geometry;
    GdkWindowHints geom_mask;
    
    gboolean dispose_has_run;
    
    /* temporary variables used when resizing */
    LuccaRect new_rect;
    gboolean new_valid;
};

struct _LuccaLayoutTileClass {
    GObjectClass parent;
    /* class members */
    guint delete_signal_id;
    guint configure_signal_id;
};

/* Convenience functions */

/* lucca_layout_newtile_at_point: returns the tile at a point using new_rect and new_valid */
LuccaLayoutTile* lucca_layout_newtile_at_point(LuccaLayout* layout, gint x, gint y);

/* tile_rect: returns a LuccaRect containing only the space in a tile that is not part of a border */
static LuccaRect tile_rect(LuccaLayoutTile* t) {
    LuccaRect result = t->rect;
    result.c[LUCCA_SIDE_RIGHT] -= t->layout->border_size;
    result.c[LUCCA_SIDE_BOTTOM] -= t->layout->border_size;
    return result;
}

/* lucca_layouttile_ensure_size: Make sure the given tile is at least as big as its geometry, resizing as necessary
    This function does not emit the 'configure' or 'delete' signals; configured and deleted are linked lists that this function will update with tiles for which signals must be emitted. The caller must unref all deleted tiles.
 */
void lucca_layouttile_ensure_size(LuccaLayoutTile* tile, GSList** configured, GSList** deleted);

/* lucca_layouttile_calc_resize: update temporary variables for a resize */
void lucca_layouttile_calc_resize(LuccaLayoutTile* tile, gint x, gint y, gint width, gint height);

#endif
