/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_RECT_H
#define LUCCA_RECT_H

#include "lucca-common.h"

/*
    Internal rectangle type.
    LuccaRect stores LEFT, RIGHT, TOP, BOTTOM coordinates internally and has
      operations for manipulating/comparing rectangles without hard-coding
      special cases for different orientations.
*/

typedef struct {
    gint c[4];
} LuccaRect;

/* lucca_rect_from_xywh: Create a rectangle from its xywh coordinates */
static LuccaRect lucca_rect_from_xywh(gint x, gint y, gint width, gint height) {
    LuccaRect result = {{x, x+width-1, y, y+height-1}};
    return result;
}

/* lucca_rect_getx, lucca_rect_gety, lucca_rect_getwidth, lucca_rect_getheight:
    Returns an xywh coordinate of the given rectangle */
static gint lucca_rect_getx(LuccaRect r) {
    return r.c[LUCCA_SIDE_LEFT];
}
static gint lucca_rect_gety(LuccaRect r) {
    return r.c[LUCCA_SIDE_TOP];
}
static gint lucca_rect_getwidth(LuccaRect r) {
    return r.c[LUCCA_SIDE_RIGHT]-r.c[LUCCA_SIDE_LEFT]+1;
}
static gint lucca_rect_getheight(LuccaRect r) {
    return r.c[LUCCA_SIDE_BOTTOM]-r.c[LUCCA_SIDE_TOP]+1;
}

/* lucca_rect_contains_point: Test if a rectangle contains a point */
static gboolean lucca_rect_contains_point(LuccaRect r, gint x, gint y) {
    return (r.c[LUCCA_SIDE_LEFT] <= x &&
            r.c[LUCCA_SIDE_RIGHT] >= x &&
            r.c[LUCCA_SIDE_TOP] <= y &&
            r.c[LUCCA_SIDE_BOTTOM] >= y);
}

/* lucca_rect_contains_rect: Test if a rectangle contains another rectangle */
static gboolean lucca_rect_contains_rect(LuccaRect a, LuccaRect b) {
    return (a.c[LUCCA_SIDE_LEFT] <= b.c[LUCCA_SIDE_LEFT] &&
            a.c[LUCCA_SIDE_RIGHT] >= b.c[LUCCA_SIDE_RIGHT] &&
            a.c[LUCCA_SIDE_TOP] <= b.c[LUCCA_SIDE_TOP] &&
            a.c[LUCCA_SIDE_BOTTOM] >= b.c[LUCCA_SIDE_BOTTOM]);
}

/* lucca_rects_intersect: Test if two rectangles intersect */
static gboolean lucca_rects_intersect(LuccaRect a, LuccaRect b) {
    if (a.c[LUCCA_SIDE_LEFT] > b.c[LUCCA_SIDE_RIGHT])
        return FALSE;
    if (b.c[LUCCA_SIDE_LEFT] > a.c[LUCCA_SIDE_RIGHT])
        return FALSE;
    if (a.c[LUCCA_SIDE_TOP] > b.c[LUCCA_SIDE_BOTTOM])
        return FALSE;
    if (b.c[LUCCA_SIDE_TOP] > a.c[LUCCA_SIDE_BOTTOM])
        return FALSE;
    return TRUE;
}

/* lucca_rect_get_thickness: Return a rectangle's thickness in the given direction */
static guint lucca_rect_get_thickness(LuccaRect r, LuccaSide side) {
    return r.c[side|1] - r.c[side&2] + 1;
}

/* lucca_rect_set_thickness: Change rectangle's thickness by changing the given coordinate */
static LuccaRect lucca_rect_set_thickness(LuccaRect r, LuccaSide side, guint thickness) {
    r.c[side] = r.c[lucca_side_opposite(side)] + lucca_side_sign(side) * (thickness - 1);
    return r;
}

#endif

