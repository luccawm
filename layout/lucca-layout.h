/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_LAYOUT_H
#define LUCCA_LAYOUT_H

/* Public declarations for the Layout Module */

#include <glib.h>
#include <glib-object.h>

/* Include GDK headers for the GdkGeometry and GdkWindowHints types, but don't link to GDK. */
#include <gdk/gdk.h>


#include "lucca-common.h"


/* Boiler-plate definition of LuccaLayout */
#define LUCCA_TYPE_LAYOUT lucca_layout_get_type()
#define LUCCA_LAYOUT(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_LAYOUT, LuccaLayout))
#define LUCCA_LAYOUT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_LAYOUT, LuccaLayoutClass))
#define LUCCA_IS_LAYOUT(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_LAYOUT))
#define LUCCA_IS_LAYOUT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_LAYOUT))
#define LUCCA_LAYOUT_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_LAYOUT, LuccaLayoutClass))

typedef struct _LuccaLayout LuccaLayout;
typedef struct _LuccaLayoutClass LuccaLayoutClass;

GType lucca_layout_get_type();

/* Boiler-plate definition of LuccaLayoutTile */
#define LUCCA_TYPE_LAYOUTTILE lucca_layouttile_get_type()
#define LUCCA_LAYOUTTILE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_LAYOUTTILE, LuccaLayoutTile))
#define LUCCA_LAYOUTTILE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_LAYOUTTILE, LuccaLayoutTileClass))
#define LUCCA_IS_LAYOUTTILE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_LAYOUTTILE))
#define LUCCA_IS_LAYOUTTILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_LAYOUTTILE))
#define LUCCA_LAYOUTTILE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_LAYOUTTILE, LuccaLayoutTileClass))

typedef struct _LuccaLayoutTile LuccaLayoutTile;
typedef struct _LuccaLayoutTileClass LuccaLayoutTileClass;

GType lucca_layouttile_get_type();

/* LuccaLayout method declarations */

/* lucca_layout_initialize: set width/height/border_size, and create the first tile */
void lucca_layout_initialize(LuccaLayout* layout, guint width, guint height, guint border_size, GdkGeometry geometry, GdkWindowHints geom_mask);

/* lucca_layout_deinit: destroy all tiles so the layout can be safely destroyed */
void lucca_layout_deinit(LuccaLayout* layout);

/* lucca_layout_resize: change the width/height/border_size of an initialized layout */
void lucca_layout_resize(LuccaLayout* layout, guint width, guint height, guint border_size);

/* lucca_layout_get_tile_at_position: returns the tile at a point */
LuccaLayoutTile* lucca_layout_get_tile_at_position(LuccaLayout* layout, gint x, gint y);

/* lucca_layout_get_tiles_in_rectangle: returns a list of tiles that intersect a rectangle */
GList* lucca_layout_get_tiles_in_rectangle(LuccaLayout* layout, gint x, gint y, gint width, gint height);

/* lucca_layout_get_tiles */
GList* lucca_layout_get_tiles(LuccaLayout* layout);

/* LuccaLayoutTile method declarations */

/* lucca_layouttile_get_layout: returns the LuccaLayout that created a tile */
LuccaLayout* lucca_layouttile_get_layout(LuccaLayoutTile* tile);

/* lucca_layouttile_split: Creates a new tile by splitting the space occupied by an existing tile */
LuccaLayoutTile* lucca_layouttile_split(LuccaLayoutTile* original, LuccaSide side, GdkGeometry geometry, GdkWindowHints geom_mask);

/* lucca_layouttile_resize_preview: Returns the new size that resize() would give to tile */
void lucca_layouttile_resize_preview(LuccaLayoutTile* tile, gint* x, gint* y, gint* width, gint* height);

/* lucca_layouttile_resize_block: Expands in the given direction from the given size to the next "interesting" point */
void lucca_layouttile_resize_block(LuccaLayoutTile* tile, LuccaSide side, guint* x, guint* y, guint* width, guint* height);

/* lucca_layouttile_resize: Makes a tile larger and alters the layout as necessary */
void lucca_layouttile_resize(LuccaLayoutTile* tile, gint x, gint y, gint width, gint height);

/* lucca_layouttile_get_geometry_hints: sets the tile's geometry hints to geometry and geom_mask */
void lucca_layouttile_set_geometry_hints(LuccaLayoutTile* tile, GdkGeometry geometry, GdkWindowHints geom_mask);

/* lucca_layouttile_get_geometry_hints: sets *geometry and *geom_mask to the tile's geometry hints */
void lucca_layouttile_get_geometry_hints(LuccaLayoutTile* tile, GdkGeometry* geometry, GdkWindowHints* geom_mask);


#endif
