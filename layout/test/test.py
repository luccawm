#!/bin/python

import sys
import subprocess
import traceback

proc = subprocess.Popen(sys.argv[1], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

LEFT, RIGHT, TOP, BOTTOM = 0, 1, 2, 3

logfile = open(sys.argv[3], 'w')

tests_failed = False

def send(line):
    logfile.write('> %s\n' % line)
    proc.stdin.write('%s\n' % line)

def getline():
    line = proc.stdout.readline().rstrip('\n')
    logfile.write('< %s\n' % line)
    return line.split(' ')

try:
    line = getline()
    assert(line[0] == 'layout')
    layout = line[1]

    send('getui %s width' % layout)
    line = getline()
    assert(line[0] == '0')

    send('getui %s height' % layout)
    line = getline()
    assert(line[0] == '0')

    send('getui %s border-size' % layout)
    line = getline()
    assert(line[0] == '0')

    send('get_tiles')
    line = getline()
    assert(len(line) == 1)

    send('geom_set_min 80 60')
    send('geom_get_min')
    line = getline()
    assert(line[0] == '80')
    assert(line[1] == '60')

    #initialize layout
    send('init 800 600 4')
    line = getline()
    assert(line[0] == 'new-tile')
    tile1 = line[1]

    send('get_layout %s' % tile1)
    line = getline()
    assert(line[0] == layout)

    send('getui %s width' % layout)
    line = getline()
    assert(line[0] == '800')

    send('getui %s height' % layout)
    line = getline()
    assert(line[0] == '600')

    send('getui %s border-size' % layout)
    line = getline()
    assert(line[0] == '4')

    send('getb %s valid' % tile1)
    line = getline()
    assert(line[0] == '1')

    send('get_geometry_hints %s' % tile1)
    send('geom_get_min')
    line = getline()
    assert(line[0] == '80')
    assert(line[1] == '60')

    send('getui %s x' % tile1)
    line = getline()
    assert(line[0] == '0')

    send('getui %s y' % tile1)
    line = getline()
    assert(line[0] == '0')

    send('getui %s width' % tile1)
    line = getline()
    assert(line[0] == '800')

    send('getui %s height' % tile1)
    line = getline()
    assert(line[0] == '600')

    send('get_tile_at_position 400 300')
    line = getline()
    assert(line[0] == tile1)

    send('get_tile_at_position 801 601')
    line = getline()
    assert(line[0] == '(nil)')

    send('get_tile_at_position -10 300')
    line = getline()
    assert(line[0] == '(nil)')

    send('get_tiles_in_rectangle 400 300 10 10')
    line = getline()
    assert(line[0] == tile1)
    assert(len(line) == 2)

    send('get_tiles_in_rectangle 801 601 10 10')
    line = getline()
    assert(len(line) == 1)

    send('get_tiles')
    line = getline()
    assert(line[0] == tile1)
    assert(len(line) == 2)

    #try to split the layout when tile1 has a minimum size of 800x600
    send('geom_set_min 800 600')
    send('set_geometry_hints %s' % tile1)
    send('geom_unset_min')

    send('split %s %s' % (tile1, LEFT))
    line = getline()
    assert(line[0] == '(nil)')

    send('geom_set_min 80 60')
    send('set_geometry_hints %s' % tile1)

    #try to split the layout creating a new tile with a minimum size of 800x600
    send('geom_set_min 800 600')

    send('split %s %s' % (tile1, LEFT))
    line = getline()
    assert(line[0] == '(nil)')

    send('geom_unset_min')

    #horizontal split
    send('geom_set_min 80 60')
    send('split %s %s' % (tile1, LEFT))
    line = getline()
    assert(line[0] == 'configure' and line[1] == tile1)
    line = getline()
    assert(line[0] == 'new-tile')
    tile2 = line[1]
    line = getline()
    assert(line[0] == tile2)

    send('getui %s width' % tile2)
    tile2_width = int(getline()[0])
    send('getui %s x' % tile1)
    tile1_x = int(getline()[0])
    assert(tile1_x == tile2_width + 4)

    send('getb %s valid' % tile2)
    line = getline()
    assert(line[0] == '1')

    send('get_layout %s' % tile2)
    line = getline()
    assert(line[0] == layout)

    #vertical split
    send('geom_set_min 80 60')
    send('split %s %s' % (tile1, BOTTOM))
    line = getline()
    assert(line[0] == 'configure' and line[1] == tile1)
    line = getline()
    assert(line[0] == 'new-tile')
    tile3 = line[1]
    line = getline()
    assert(line[0] == tile3)

    send('getui %s height' % tile1)
    tile1_height = int(getline()[0])
    send('getui %s y' % tile3)
    tile3_y = int(getline()[0])
    assert(tile3_y == tile1_height + 4)

    #2-corner resize
    send('resize %s 0 0 600 600' % tile2)
    expected_configures = set([tile1, tile2, tile3])
    configured_tiles = set()
    for i in range(len(expected_configures)):
        line = getline()
        assert(line[0] == 'configure')
        configured_tiles.add(line[1])
    assert(configured_tiles == expected_configures)
    
    send('getui %s width' % tile2)
    line = getline()
    assert(line[0] == '600')

    send('getui %s x' % tile1)
    line = getline()
    assert(line[0] == '604')

    send('getui %s x' % tile3)
    line = getline()
    assert(line[0] == '604')
    
    send('getui %s width' % tile1)
    line = getline()
    assert(line[0] == '196')
    
    send('getui %s width' % tile3)
    line = getline()
    assert(line[0] == '196')

    #1-corner resize
    send('resize_preview %s 400 0 400 400' % tile1)
    line = getline()
    assert(line == ['400', '0', '400', '400'])

    send('resize %s 400 0 400 400' % tile1)
    expected_configures = set([tile1, tile2, tile3])
    configured_tiles = set()
    for i in range(len(expected_configures)):
        line = getline()
        assert(line[0] == 'configure')
        configured_tiles.add(line[1])
    assert(configured_tiles == expected_configures)
    
    send('getui %s width' % tile2)
    line = getline()
    assert(line[0] == '396')

    send('getui %s x' % tile1)
    line = getline()
    assert(line[0] == '400')

    send('getui %s x' % tile3)
    line = getline()
    assert(line[0] == '400')
    
    send('getui %s width' % tile1)
    line = getline()
    assert(line[0] == '400')
    
    send('getui %s width' % tile3)
    line = getline()
    assert(line[0] == '400')

    send('getui %s height' % tile1)
    line = getline()
    assert(line[0] == '400')
    
    send('getui %s y' % tile3)
    line = getline()
    assert(line[0] == '404')
    
    send('getui %s height' % tile3)
    line = getline()
    assert(line[0] == '196')

    #4-corner resize
    #send('resize %s 0 0 800 600' % tile1)
    send('resize %s 1 0 799 600' % tile1)
    deleted_tiles = set()
    expected_deletes = set([tile2, tile3])
    for i in range(len(expected_deletes)):
        line = getline()
        assert(line[0] == 'delete')
        deleted_tiles.add(line[1])
    assert(deleted_tiles == expected_deletes)
    line = getline()
    assert(line[0] == 'configure')
    assert(line[1] == tile1)
    
    for i in expected_deletes:
        send('getb %s valid' % i)
        line = getline()
        assert(line[0] == '0')
    
    send('getui %s x' % tile1)
    line = getline()
    assert(line[0] == '0')
    
    send('getui %s y' % tile1)
    line = getline()
    assert(line[0] == '0')
    
    send('getui %s width' % tile1)
    line = getline()
    assert(line[0] == '800')
    
    send('getui %s height' % tile1)
    line = getline()
    assert(line[0] == '600')

    #0/1-corner resize
    send('split %s %s' % (tile1, RIGHT))
    line = getline()
    assert(line[0] == 'configure' and line[1] == tile1)
    line = getline()
    assert(line[0] == 'new-tile')
    tile2 = line[1]
    line = getline()
    assert(line[0] == tile2)
    
    send('split %s %s' % (tile2, BOTTOM))
    line = getline()
    assert(line[0] == 'configure' and line[1] == tile2)
    line = getline()
    assert(line[0] == 'new-tile')
    tile3 = line[1]
    line = getline()
    assert(line[0] == tile3)
    
    send('split %s %s' % (tile3, BOTTOM))
    line = getline()
    assert(line[0] == 'configure' and line[1] == tile3)
    line = getline()
    assert(line[0] == 'new-tile')
    tile4 = line[1]
    line = getline()
    assert(line[0] == tile4)
    
    send('resize %s 200 200 600 300' % tile3)
    expected_configures = set([tile1, tile2, tile3, tile4])
    configured_tiles = set()
    for i in range(len(expected_configures)):
        line = getline()
        assert(line[0] == 'configure')
        configured_tiles.add(line[1])
    assert(configured_tiles == expected_configures)

    send('getui %s width' % tile1)
    line = getline()
    assert(line[0] == '196')

    for t in (tile2, tile3, tile4):
        send('getui %s x' % t)
        line = getline()
        assert(line[0] == '200')

        send('getui %s width' % t)
        line = getline()
        assert(line[0] == '600')

    #0/2-corner resize
    #send('resize %s 0 200 800 300' % tile3)
    send('resize %s 1 200 799 300' % tile3)
    expected_configures = set([tile1, tile3, tile4])
    configured_tiles = set()
    for i in range(len(expected_configures)):
        line = getline()
        assert(line[0] == 'configure')
        configured_tiles.add(line[1])
    assert(configured_tiles == expected_configures)

    for i in (tile1, tile2, tile3, tile4):
        send('getui %s x' % i)
        getline()
        send('getui %s y' % i)
        getline()
        send('getui %s width' % i)
        getline()
        send('getui %s height' % i)
        getline()

    send('getui %s height' % tile1)
    line = getline()
    assert(line[0] == '196')

    send('getui %s x' % tile3)
    line = getline()
    assert(line[0] == '0')

    send('getui %s width' % tile3)
    line = getline()
    assert(line[0] == '800')

    send('getui %s x' % tile4)
    line = getline()
    assert(line[0] == '0')

    send('getui %s width' % tile4)
    line = getline()
    assert(line[0] == '800')
    
    #resize layout
    send('layout_resize 640 480 2')
    expected_configures = set([tile1, tile2, tile3, tile4])
    configured_tiles = set()
    for i in range(len(expected_configures)):
        line = getline()
        assert(line[0] == 'configure')
        configured_tiles.add(line[1])
    assert(configured_tiles == expected_configures)
    
    send('getui %s x' % tile1)
    line = getline()
    assert(line[0] == '0')
    
    send('getui %s width' % tile1)
    line = getline()
    tile1_width = int(line[0])
    
    send('getui %s x' % tile2)
    line = getline()
    x = int(line[0])
    assert(x == tile1_width+2)
    
    prev_bottom = -2
    for t in (tile2, tile3, tile4):
        send('getui %s x' % t)
        line = getline()
        x = int(line[0])
        
        send('getui %s width' % t)
        line = getline()
        width = int(line[0])
        assert(x+width == 640)

        send('getui %s y' % t)
        line = getline()
        y = int(line[0])
        assert(y == prev_bottom+2)
        
        send('getui %s height' % t)
        line = getline()
        height = int(line[0])
        prev_bottom = y+height
    
    assert(prev_bottom == 480)
    
    #block resize (preview)
    send('split %s %s' % (tile3, LEFT))
    line = getline()
    assert(line[0] == 'configure' and line[1] == tile3)
    line = getline()
    assert(line[0] == 'new-tile')
    tile5 = line[1]
    line = getline()
    assert(line[0] == tile5)
    
    send('getui %s x' % tile3)
    line = getline()
    x = int(line[0])
    send('getui %s y' % tile3)
    line = getline()
    y = int(line[0])
    send('getui %s width' % tile3)
    line = getline()
    width = int(line[0])
    send('getui %s height' % tile3)
    line = getline()
    height = int(line[0])
    
    send('getui %s x' % tile2)
    line = getline()
    tile2_x = int(line[0])

    send('resize_block %s %s %s %s %s %s' % (tile3, LEFT, x, y, width, height))
    line = getline()
    newx = int(line[0])
    newwidth = int(line[2])
    assert(tile2_x == newx)
    assert(newx+newwidth == 640)

    send('geom_unset_min')
    send('set_geometry_hints %s' % tile1)
    send('geom_set_min 20 20')
    send('get_geometry_hints %s' % tile1)
    send('geom_get_min')
    line = getline()
    assert(line[0] == 'unset')

    #deinitialize layout
    send('deinit')
    deleted_tiles = set()
    expected_deletes = set([tile1, tile2, tile3, tile4, tile5])
    for i in range(len(expected_deletes)):
        line = getline()
        assert(line[0] == 'delete')
        deleted_tiles.add(line[1])
    assert(deleted_tiles == expected_deletes)

    for i in expected_deletes:
        send('getb %s valid' % i)
        line = getline()
        assert(line[0] == '0')

    send('get_tiles')
    line = getline()
    assert(len(line) == 1)
    
    #test that set_geometry_hints respects minimum sizes
    send('geom_unset_min')
    send('init 500 500 4')
    line = getline()
    assert(line[0] == 'new-tile')
    tile1 = line[1]
    
    send('split %s %s' % (tile1, RIGHT))
    line = getline()
    assert(line[0] == 'configure')
    assert(line[1] == tile1)
    line = getline()
    assert(line[0] == 'new-tile')
    tile2 = line[1]
    line = getline()
    assert(line[0] == tile2)
    
    send('geom_set_min 300 300')
    send('set_geometry_hints %s' % tile1)
    expected_configures = set([tile1, tile2])
    configured_tiles = set()
    for i in range(len(expected_configures)):
        line = getline()
        assert(line[0] == 'configure')
        configured_tiles.add(line[1])
    assert(configured_tiles == expected_configures)
    
    send('getui %s width' % tile1)
    line = getline()
    assert(int(line[0]) >= 300)
    
    send('layout_resize 300 300 4')
    line = getline()
    assert(line[0] == 'delete')
    assert(line[1] == tile2)
    line = getline()
    assert(line[0] == 'configure')
    assert(line[1] == tile1)
    
    send('deinit')
    line = getline()
    assert(line[0] == 'delete')
    assert(line[1] == tile1)

except:
    tests_failed = True
    
    traceback.print_exc(None, logfile)

proc.stdin.close()

result, stdout, stderr = proc.wait(), proc.stdout.read(), proc.stderr.read()

logfile.write("Test process return value: %s\n" % result)

logfile.write("Stdout:\n%s" % stdout)
logfile.write("Stderr:\n%s" % stderr)

logfile.close()

if result != 0 or stdout != '' or stderr != '':
    tests_failed = True

if tests_failed:
    print "Unit test failed; see %s for details" % sys.argv[3]
else:
    f = open(sys.argv[2], 'w')
    f.close()

