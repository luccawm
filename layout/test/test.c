/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <glib.h>

#include <stdio.h>
#include <readline/readline.h>

#include <stdlib.h>

#include "lucca-layout.h"

GList* refs=NULL; /* a list of objects that we hold a reference to; this lets us unref and hopefully free everything when the tests end */

void on_deletetile(LuccaLayoutTile* tile, gpointer user_data) {
    g_print("delete %p\n", tile);
}

void on_configuretile(LuccaLayoutTile* tile, gpointer user_data) {
    g_print("configure %p\n", tile);
}

void on_newtile(LuccaLayout* layout, LuccaLayoutTile* tile, gpointer user_data) {
    g_print("new-tile %p\n", tile);
    
    g_signal_connect(tile, "delete", G_CALLBACK(on_deletetile), NULL);
    g_signal_connect(tile, "configure", G_CALLBACK(on_configuretile), NULL);
    
    g_object_ref(tile); /* store a reference to every tile so they're never destroyed */
    refs = g_list_prepend(refs, tile);
}

int main() {
    LuccaLayout* layout=NULL;
    GdkGeometry hints;
    GdkWindowHints hints_mask;
    char* input_line=NULL;
    gchar** input_words=NULL;
    GObject* obj;
    guint uivalue;
    gboolean bvalue;
    GList* list;
    GList* listi;
    
    g_type_init(); /* initialize the type system */
    
    layout = g_object_new(LUCCA_TYPE_LAYOUT, NULL);
    
    g_signal_connect(layout, "new-tile", G_CALLBACK(on_newtile), NULL);
    
    g_print("layout %p\n", layout);
    
    for (;;) {
        input_line = readline("");
        if (input_line == NULL) {
            break;
        }
        input_words = g_strsplit(input_line, " ", 0);
        if ((input_words[0] == NULL) || (g_ascii_strcasecmp(input_words[0], "quit") == 0)) {
            free(input_line);
            g_strfreev(input_words);
            break;
        }
        else if (g_ascii_strcasecmp(input_words[0], "init") == 0) {
            lucca_layout_initialize(layout, strtol(input_words[1], NULL, 10), strtol(input_words[2], NULL, 10), strtol(input_words[3], NULL, 10), hints, hints_mask);
        }
        else if (g_ascii_strcasecmp(input_words[0], "getui") == 0) {
            obj = G_OBJECT(strtol(input_words[1]+2, NULL, 16));
            g_object_get(obj, input_words[2], &uivalue, NULL);
            g_print("%i\n", uivalue);
        }
        else if (g_ascii_strcasecmp(input_words[0], "getb") == 0) {
            obj = G_OBJECT(strtol(input_words[1]+2, NULL, 16));
            g_object_get(obj, input_words[2], &bvalue, NULL);
            g_print("%i\n", bvalue);
        }
        else if (g_ascii_strcasecmp(input_words[0], "deinit") == 0) {
            lucca_layout_deinit(layout);
        }
        else if (g_ascii_strcasecmp(input_words[0], "layout_resize") == 0) {
            lucca_layout_resize(layout, strtol(input_words[1], NULL, 10), strtol(input_words[2], NULL, 10), strtol(input_words[3], NULL, 10));
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_tile_at_position") == 0) {
            g_print("%p\n", lucca_layout_get_tile_at_position(layout, strtol(input_words[1], NULL, 10), strtol(input_words[2], NULL, 10)));
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_tiles_in_rectangle") == 0) {
            list = lucca_layout_get_tiles_in_rectangle(layout, strtol(input_words[1], NULL, 10), strtol(input_words[2], NULL, 10), strtol(input_words[3], NULL, 10), strtol(input_words[4], NULL, 10));
            for (listi=list; listi != NULL; listi=listi->next) {
                g_print("%p ", listi->data);
            }
            g_list_free(list);
            g_print("\n");
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_tiles") == 0) {
            list = lucca_layout_get_tiles(layout);
            for (listi=list; listi != NULL; listi=listi->next) {
                g_print("%p ", listi->data);
            }
            g_list_free(list);
            g_print("\n");
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_layout") == 0) {
            g_print("%p\n", lucca_layouttile_get_layout(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16))));
        }
        else if (g_ascii_strcasecmp(input_words[0], "split") == 0) {
            g_print("%p\n", lucca_layouttile_split(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16)), strtol(input_words[2], NULL, 10), hints, hints_mask));
        }
        else if (g_ascii_strcasecmp(input_words[0], "resize_preview") == 0) {
            gint x, y, width, height;
            x = strtol(input_words[2], NULL, 10);
            y = strtol(input_words[3], NULL, 10);
            width = strtol(input_words[4], NULL, 10);
            height = strtol(input_words[5], NULL, 10);
            lucca_layouttile_resize_preview(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16)), &x, &y, &width, &height);
            g_print("%i %i %i %i\n", x, y, width, height);
        }
        else if (g_ascii_strcasecmp(input_words[0], "resize_block") == 0) {
            LuccaSide side;
            guint x, y, width, height;
            side = strtol(input_words[2], NULL, 10);
            x = strtol(input_words[3], NULL, 10);
            y = strtol(input_words[4], NULL, 10);
            width = strtol(input_words[5], NULL, 10);
            height = strtol(input_words[6], NULL, 10);
            lucca_layouttile_resize_block(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16)), side, &x, &y, &width, &height);
            g_print("%i %i %i %i\n", x, y, width, height);
        }
        else if (g_ascii_strcasecmp(input_words[0], "resize") == 0) {
            lucca_layouttile_resize(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16)), strtol(input_words[2], NULL, 10), strtol(input_words[3], NULL, 10), strtol(input_words[4], NULL, 10), strtol(input_words[5], NULL, 10));
        }
        else if (g_ascii_strcasecmp(input_words[0], "set_geometry_hints") == 0) {
            lucca_layouttile_set_geometry_hints(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16)), hints, hints_mask);
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_geometry_hints") == 0) {
            lucca_layouttile_get_geometry_hints(LUCCA_LAYOUTTILE(strtol(input_words[1]+2, NULL, 16)), &hints, &hints_mask);
        }
        else if (g_ascii_strcasecmp(input_words[0], "geom_set_min") == 0) {
            hints.min_width = strtol(input_words[1], NULL, 10);
            hints.min_height = strtol(input_words[2], NULL, 10);
            hints_mask |= GDK_HINT_MIN_SIZE;
        }
        else if (g_ascii_strcasecmp(input_words[0], "geom_unset_min") == 0) {
            hints_mask &= ~GDK_HINT_MIN_SIZE;
        }
        else if (g_ascii_strcasecmp(input_words[0], "geom_get_min") == 0) {
            if (hints_mask & GDK_HINT_MIN_SIZE) {
                g_print("%i %i\n", hints.min_width, hints.min_height);
            }
            else {
                g_print("unset\n");
            }
        }
        else {
            g_print("no such command\n");
        }
        free(input_line);
        g_strfreev(input_words);
    };
    
    g_object_unref(layout);
    
    /* free outstanding references to tiles */
    for (listi=refs; listi != NULL; listi=listi->next) {
        g_object_unref(G_OBJECT(listi->data));
    }
    g_list_free(refs);

    return 0;
}

