/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-layout-private.h"

/* Implementation of LuccaLayout */

static GObjectClass* parent_class = NULL;

/* property id's */
enum {
    PROP_WIDTH=1,
    PROP_HEIGHT,
    PROP_BORDERSIZE,
};

static void lucca_layout_init(GTypeInstance* instance, gpointer g_class) {
    LuccaLayout* layout = LUCCA_LAYOUT(instance);
    
    layout->width = 0;
    layout->height = 0;
    layout->border_size = 0;
    layout->tiles = NULL;
    
    layout->dispose_has_run = FALSE;
}

static void _unref_tile(gpointer data, gpointer user_data) {
    LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(data);
    
    g_object_unref(tile);
}
static void lucca_layout_dispose(GObject* obj) {
    LuccaLayout* layout = LUCCA_LAYOUT(obj);
    
    if (layout->dispose_has_run) {
        return;
    }
    layout->dispose_has_run = TRUE;
    
    /* The layout should currently hold no references because of deinit() */
    if (layout->tiles) {
        g_warning("LuccaLayout at %p was disposed before calling lucca_layout_deinit()\n", layout);
        
        g_slist_foreach(layout->tiles, _unref_tile, NULL);
        g_slist_free(layout->tiles);
        layout->tiles = NULL;
    }
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_layout_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaLayout* layout = LUCCA_LAYOUT(object);
    
    switch (property_id) {
    case PROP_WIDTH:
        g_value_set_uint(value, layout->width);
        break;
    case PROP_HEIGHT:
        g_value_set_uint(value, layout->height);
        break;
    case PROP_BORDERSIZE:
        g_value_set_uint(value, layout->border_size);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_layout_class_init(gpointer g_class, gpointer class_data) {
    LuccaLayoutClass* klass = (LuccaLayoutClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_layout_dispose;
    gobclass->get_property = lucca_layout_get_property;
    
    /* properties */
    pspec = g_param_spec_uint(
        "width",
        "Layout width",
        "Get layout width in pixels",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WIDTH, pspec);
    
    pspec = g_param_spec_uint(
        "height",
        "Layout height",
        "Get layout height in pixels",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_HEIGHT, pspec);
    
    pspec = g_param_spec_uint(
        "border-size",
        "Tile border size",
        "Get size of gaps between tiles in pixels",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_BORDERSIZE, pspec);
    
    /* signals */
    klass->new_tile_signal_id = g_signal_new(
        "new-tile",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__OBJECT,
        G_TYPE_NONE,
        1, /* n_params */
        LUCCA_TYPE_LAYOUTTILE
    );
    
    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_layout_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaLayoutClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_layout_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaLayout),
            0, /* n_preallocs */
            lucca_layout_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaLayoutType", &info, 0);
    }
    return type;
}

/*
  Method definitions.
*/

void lucca_layout_initialize(LuccaLayout* layout, guint width, guint height, guint border_size, GdkGeometry geometry, GdkWindowHints geom_mask) {
    LuccaLayoutTile* tile;
    g_assert(layout->tiles == NULL);
    g_assert(width >= 1 && height >= 1);
    
    layout->width = width;
    layout->height = height;
    layout->border_size = border_size;
    
    layout->rect = lucca_rect_from_xywh(0, 0, width+border_size, height+border_size);
    
    /* The given geometry cannot have a minimum size that is larger than the layout */
    if (geom_mask&GDK_HINT_MIN_SIZE) {
        g_assert(geometry.min_width <= width);
        g_assert(geometry.min_height <= height);
    }
    
    tile = g_object_new(LUCCA_TYPE_LAYOUTTILE, NULL);
    tile->layout = layout;
    g_object_ref(layout);
    tile->rect = layout->rect;
    tile->valid = TRUE;
    
    lucca_layouttile_set_geometry_hints(tile, geometry, geom_mask);
    
    layout->tiles = g_slist_prepend(layout->tiles, tile);
    
    /* emit the new-tile signal */
    g_signal_emit(layout, LUCCA_LAYOUT_GET_CLASS(layout)->new_tile_signal_id, 0, tile);
}

static void _invalidate_tile(gpointer data, gpointer user_data) {
    LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(data);
    
    tile->valid = FALSE;
}
static void _emitdelete_tile(gpointer data, gpointer user_data) {
    LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(data);
    
    g_signal_emit(tile, LUCCA_LAYOUTTILE_GET_CLASS(tile)->delete_signal_id, 0);
}
void lucca_layout_deinit(LuccaLayout* layout) {
    GSList* tiles;
    
    tiles = layout->tiles;
    
    /* Mark all tiles as invalid */
    g_slist_foreach(tiles, _invalidate_tile, NULL);
    
    /* "Remove" all tiles from the layout. */
    layout->tiles = NULL;
    
    /* Emit a delete signal on each tile*/
    g_slist_foreach(tiles, _emitdelete_tile, NULL);
    
    /* Actually unref the tiles */
    g_slist_foreach(tiles, _unref_tile, NULL);
    
    /* Delete the linked list of tiles */
    g_slist_free(tiles);
}

void lucca_layout_resize(LuccaLayout* layout, guint width, guint height, guint border_size) {
    GSList* item;
    guint old_width;
    guint new_width;
    guint old_height;
    guint new_height;
    GSList* configured=NULL;
    GSList* deleted=NULL;
    
    g_assert(width >= 1 && height >= 1);
    
    old_width = layout->width+layout->border_size;
    new_width = width+border_size;
    old_height = layout->height+layout->border_size;
    new_height = height+border_size;
    
    for (item=layout->tiles; item != NULL; item=item->next) {
        LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(item->data);
        
        tile->rect.c[LUCCA_SIDE_LEFT] = tile->rect.c[LUCCA_SIDE_LEFT] * new_width / old_width;
        tile->rect.c[LUCCA_SIDE_RIGHT] = (tile->rect.c[LUCCA_SIDE_RIGHT]+1) * new_width / old_width -1;
        tile->rect.c[LUCCA_SIDE_TOP] = tile->rect.c[LUCCA_SIDE_TOP] * new_height / old_height;
        tile->rect.c[LUCCA_SIDE_BOTTOM] = (tile->rect.c[LUCCA_SIDE_BOTTOM]+1) * new_height / old_height -1;
    }
    
    layout->width = width;
    layout->height = height;
    layout->border_size = border_size;
    
    /* check if this would make any tiles too small */
    for (item=layout->tiles; item != NULL; item=item->next) {
        LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(item->data);
        
        /* NOTE: lucca_layout_ensure_size may delete other items from layout->tiles, but this is safe because it's a linked list */
        lucca_layouttile_ensure_size(tile, &configured, &deleted);
    }
    
    /* emit delete events */
    for (item=deleted; item != NULL; item=item->next) {
        LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(item->data);
        
        g_signal_emit(tile, LUCCA_LAYOUTTILE_GET_CLASS(tile)->delete_signal_id, 0);
    }
    
    /* it's unlikely that any tile's size is unchanged so emit a configure event for all of them */
    for (item=layout->tiles; item != NULL; item=item->next) {
        LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(item->data);
        
        g_signal_emit(tile, LUCCA_LAYOUTTILE_GET_CLASS(tile)->configure_signal_id, 0);
    }
    
    /* unref deleted tiles */
    for (item=deleted; item != NULL; item=item->next) {
        LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(item->data);
        
        
        g_object_unref(tile);
    }
}

LuccaLayoutTile* lucca_layout_get_tile_at_position(LuccaLayout* layout, gint x, gint y) {
    GSList* i;
    LuccaLayoutTile* t;
    
    for (i=layout->tiles; i != NULL; i=i->next) {
        t = LUCCA_LAYOUTTILE(i->data);
        
        if (lucca_rect_contains_point(tile_rect(t), x, y))
            return t;
    }
    
    return NULL;
}

GList* lucca_layout_get_tiles_in_rectangle(LuccaLayout* layout, gint x, gint y, gint width, gint height) {
    GList* result=NULL;
    GSList* i;
    LuccaLayoutTile* t;
    LuccaRect r;
    
    r = lucca_rect_from_xywh(x, y, width, height);
    
    for (i=layout->tiles; i != NULL; i=i->next) {
        t = LUCCA_LAYOUTTILE(i->data);
        
        if (lucca_rects_intersect(tile_rect(t), r)) {
            result = g_list_prepend(result, (gpointer)t);
        }
    }
    
    return result;
}

GList* lucca_layout_get_tiles(LuccaLayout* layout) {
    GList* result=NULL;
    GSList* i;
    
    for (i=layout->tiles; i != NULL; i=i->next) {
        result = g_list_prepend(result, i->data);
    }
    
    return result;
}

LuccaLayoutTile* lucca_layout_newtile_at_point(LuccaLayout* layout, gint x, gint y) {
    GSList* i;
    LuccaLayoutTile* t;
    
    for (i=layout->tiles; i != NULL; i=i->next) {
        t = LUCCA_LAYOUTTILE(i->data);
        
        if (!t->new_valid)
            continue;
        
        if (lucca_rect_contains_point(t->new_rect, x, y))
            return t;
    }
    
    return NULL;
}

