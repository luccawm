/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-layout-private.h"

/* Implementation of LuccaLayoutTile */

static GObjectClass* parent_class = NULL;

/* property id's */
enum {
    PROP_VALID=1,
    PROP_X,
    PROP_Y,
    PROP_WIDTH,
    PROP_HEIGHT,
};

static void lucca_layouttile_init(GTypeInstance* instance, gpointer g_class) {
    LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(instance);
    
    tile->valid = FALSE; /* Tiles aren't valid until part of the Layout module says they are. */
    tile->layout = NULL;
    
    tile->dispose_has_run = FALSE;
}

static void lucca_layouttile_dispose(GObject* obj) {
    LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(obj);
    
    if (tile->dispose_has_run) {
        return;
    }
    tile->dispose_has_run = FALSE;
    
    if (tile->valid == TRUE) {
        g_warning("LuccaLayoutTile at %p was disposed while it was still valid\n", tile);
    }
    
    if (tile->layout != NULL) {
        g_object_unref(tile->layout);
    }
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_layouttile_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaLayoutTile* tile = LUCCA_LAYOUTTILE(object);
    
    switch (property_id) {
    case PROP_VALID:
        g_value_set_boolean(value, tile->valid);
        break;
    case PROP_X:
        g_value_set_uint(value, lucca_rect_getx(tile_rect(tile)));
        break;
    case PROP_Y:
        g_value_set_uint(value, lucca_rect_gety(tile_rect(tile)));
        break;
    case PROP_WIDTH:
        g_value_set_uint(value, lucca_rect_getwidth(tile_rect(tile)));
        break;
    case PROP_HEIGHT:
        g_value_set_uint(value, lucca_rect_getheight(tile_rect(tile)));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_layouttile_class_init(gpointer g_class, gpointer class_data) {
    LuccaLayoutTileClass* klass = (LuccaLayoutTileClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec* pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_layouttile_dispose;
    gobclass->get_property = lucca_layouttile_get_property;

    /* properties */
    pspec = g_param_spec_boolean(
        "valid",
        "Tile is valid",
        "TRUE if the tile is part of a layout",
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_VALID, pspec);

    pspec = g_param_spec_uint(
        "x",
        "x",
        "x coordinate of the tile's upper-left corner",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_X, pspec);

    pspec = g_param_spec_uint(
        "y",
        "y",
        "y coordinate of the tile's upper-left corner",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_Y, pspec);

    pspec = g_param_spec_uint(
        "width",
        "width",
        "width of the tile in pixels",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WIDTH, pspec);

    pspec = g_param_spec_uint(
        "height",
        "height",
        "height of the tile in pixels",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_HEIGHT, pspec);
    
    /* signals */
    klass->delete_signal_id = g_signal_new(
        "delete",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );
    klass->configure_signal_id = g_signal_new(
        "configure",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );
    
    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_layouttile_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaLayoutTileClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_layouttile_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaLayoutTile),
            0, /* n_preallocs */
            lucca_layouttile_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaLayoutTileType", &info, 0);
    }
    return type;
}

/*
  Method definitions.
*/

LuccaLayout* lucca_layouttile_get_layout(LuccaLayoutTile* tile) {
    return tile->layout;
}

LuccaLayoutTile* lucca_layouttile_split(LuccaLayoutTile* original, LuccaSide side, GdkGeometry geometry, GdkWindowHints geom_mask) {
    LuccaLayoutTile* result;
    LuccaLayout* layout;
    guint thickness;
    guint orig_thickness;
    guint result_thickness;
    
    layout = original->layout;
    
    /* first, calculate the new sizes */
    thickness = lucca_rect_get_thickness(original->rect, side);
    
    orig_thickness = thickness / 2;
    result_thickness = orig_thickness + (thickness%2);
    
    /* make sure original won't be smaller than its minimum size */
    if ((lucca_side_horizontal(side) && orig_thickness < original->geometry.min_width+layout->border_size) ||
        (lucca_side_vertical(side) && orig_thickness < original->geometry.min_height+layout->border_size))
            return NULL;
    
    /* make sure the new tile won't be smaller than its minimum size */
    if (!(geom_mask&GDK_HINT_MIN_SIZE)) {
        geometry.min_width = 0;
        geometry.min_height = 0;
    }
    if ((lucca_side_horizontal(side) && result_thickness < geometry.min_width+layout->border_size) ||
        (lucca_side_vertical(side) && result_thickness < geometry.min_height+layout->border_size))
            return NULL;
    
    /* create the new tile */
    result = g_object_new(LUCCA_TYPE_LAYOUTTILE, NULL);
    result->layout = layout;
    g_object_ref(layout);
    result->rect = lucca_rect_set_thickness(original->rect, lucca_side_opposite(side), result_thickness);
    result->valid = TRUE;
    
    lucca_layouttile_set_geometry_hints(result, geometry, geom_mask);
    
    layout->tiles = g_slist_prepend(layout->tiles, result);
    
    /* resize the original */
    original->rect = lucca_rect_set_thickness(original->rect, side, orig_thickness);
    
    /* emit signals */
    g_signal_emit(original, LUCCA_LAYOUTTILE_GET_CLASS(result)->configure_signal_id, 0);
    g_signal_emit(layout, LUCCA_LAYOUT_GET_CLASS(layout)->new_tile_signal_id, 0, result);
    
    return result;
}

static void _collapse_side_inward(LuccaLayoutTile* displaced_tile, LuccaSide direction, LuccaLayoutTile* expanding_tile, GQueue* to_calc_resize) {
    LuccaRect new_rect;
    LuccaSide p_direction;
    int i;
    gint min_thickness;
    
    /* calculate the new size of displaced_tile */
    new_rect = displaced_tile->new_rect;
    new_rect.c[direction] = expanding_tile->new_rect.c[lucca_side_opposite(direction)] - lucca_side_sign(direction);
    /* would this make displaced_tile too small? */
    if (lucca_side_horizontal(direction))
        min_thickness = displaced_tile->geometry.min_width;
    else
        min_thickness = displaced_tile->geometry.min_height;
    if (lucca_rect_get_thickness(new_rect, direction) < min_thickness + displaced_tile->layout->border_size) {
        /* displaced_tile would be too small; expand expanding_tile further and don't change displaced_tile for now */
        expanding_tile->new_rect.c[lucca_side_opposite(direction)] = displaced_tile->new_rect.c[lucca_side_opposite(direction)];
        g_queue_push_tail(to_calc_resize, expanding_tile);
        return;
    }
    /* look for gaps to fill with other tiles */
    p_direction = lucca_side_perpendicular(direction);
    for (i=0; i<2; i++) {
        gint c1 = displaced_tile->new_rect.c[direction] + lucca_side_sign(direction);
        gint c2 = expanding_tile->new_rect.c[p_direction] + lucca_side_sign(p_direction);
        gint c2_stop = displaced_tile->new_rect.c[p_direction] + lucca_side_sign(p_direction);
        while (lucca_side_coords_lt(p_direction, c2, c2_stop)) {
            LuccaLayoutTile* gapfiller = lucca_layout_newtile_at_point(displaced_tile->layout, lucca_side_order_point(direction, c1, c2));
            g_assert(gapfiller != NULL);
            gapfiller->new_rect.c[lucca_side_opposite(direction)] = expanding_tile->new_rect.c[lucca_side_opposite(direction)];
            if (lucca_side_coords_lt(p_direction, c2_stop, gapfiller->new_rect.c[p_direction])) {
                g_queue_push_tail(to_calc_resize, gapfiller);
                break;
            }
            else {
                if (!g_slist_find(displaced_tile->layout->to_resize, gapfiller))
                    displaced_tile->layout->to_resize = g_slist_prepend(displaced_tile->layout->to_resize, gapfiller);
            }
            c2 = gapfiller->new_rect.c[p_direction] + lucca_side_sign(p_direction);
        }
        p_direction = lucca_side_opposite(p_direction);
    }
    displaced_tile->new_rect = new_rect;
    if (!g_slist_find(displaced_tile->layout->to_resize, displaced_tile))
        displaced_tile->layout->to_resize = g_slist_prepend(displaced_tile->layout->to_resize, displaced_tile);
}
void lucca_layouttile_calc_resize(LuccaLayoutTile* tile, gint x, gint y, gint width, gint height) {
    LuccaLayout* layout;
    LuccaRect desired_rect;
    GSList* item;
    GQueue* to_calc_resize;
    LuccaLayoutTile* expanding_tile;
    gint intersecting_corners;
    gint intersecting_corners_count;
    LuccaSide moving_edge;
    LuccaSide intersecting_edge;
    gint min_thickness;
    
    layout = tile->layout;
    
    /* reset temporary variables */
    layout->to_resize = NULL;
    layout->to_delete = NULL;
    
    for (item=layout->tiles; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        t->new_rect = t->rect;
        t->new_valid = TRUE;
    }
    
    /* check if the given coordinates are valid */
    desired_rect = lucca_rect_from_xywh(x, y, width+layout->border_size, height+layout->border_size);
    
    if (!lucca_rect_contains_rect(layout->rect, desired_rect))
        return;
    
    if (!lucca_rect_contains_rect(desired_rect, tile->rect))
        return;
    
    /* if a border is moved "close" to an "interesting" point, move it the rest of the way */
    for (moving_edge=0; moving_edge<4; moving_edge++) {
        /* check if this edge moved outward */
        if (tile->rect.c[moving_edge] != desired_rect.c[moving_edge]) {
            /* find out how much further we'd have to go to get to somewhere interesting */
            guint newx, newy, newwidth, newheight;
            LuccaRect block_resize_result;
            
            newx = (guint)x;
            newy = (guint)y;
            newwidth = (guint)width;
            newheight = (guint)height;
            
            lucca_layouttile_resize_block(tile, moving_edge, &newx, &newy, &newwidth, &newheight);
            
            block_resize_result = lucca_rect_from_xywh(newx, newy, newwidth+layout->border_size, newheight+layout->border_size);
            
            if ((block_resize_result.c[moving_edge] - desired_rect.c[moving_edge])*lucca_side_sign(moving_edge) <= 30) {
                desired_rect.c[moving_edge] = block_resize_result.c[moving_edge];
            }
        }
    }
    
    /* calculate the new layout */
    tile->new_rect = desired_rect;
    
    to_calc_resize = g_queue_new();
    g_queue_push_tail(to_calc_resize, tile);
    
    /* clear space for each enlarged tile */
    while ((expanding_tile = g_queue_pop_head(to_calc_resize))) {
        /* add tile to to_resize if necessary */
        if (!g_slist_find(layout->to_resize, expanding_tile))
            layout->to_resize = g_slist_prepend(layout->to_resize, expanding_tile);
        
        /* check for tiles that intersect this one */
        for (item=layout->tiles; item != NULL; item=item->next) {
            LuccaLayoutTile* displaced_tile = LUCCA_LAYOUTTILE(item->data);
            
            if (displaced_tile == expanding_tile || displaced_tile->new_valid == FALSE || !lucca_rects_intersect(expanding_tile->new_rect, displaced_tile->new_rect))
                continue;
            
            /* which corners of displaced_tile are in expanding_tile? */
            intersecting_corners = 0x0;
            intersecting_corners_count = 0;
            if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_LEFT], displaced_tile->new_rect.c[LUCCA_SIDE_TOP])) {
                intersecting_corners |= 0x8;
                intersecting_corners_count++;
            }
            if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_RIGHT], displaced_tile->new_rect.c[LUCCA_SIDE_TOP])) {
                intersecting_corners |= 0x4;
                intersecting_corners_count++;
            }
            if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_LEFT], displaced_tile->new_rect.c[LUCCA_SIDE_BOTTOM])) {
                intersecting_corners |= 0x2;
                intersecting_corners_count++;
            }
            if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_RIGHT], displaced_tile->new_rect.c[LUCCA_SIDE_BOTTOM])) {
                intersecting_corners |= 0x1;
                intersecting_corners_count++;
            }
            
            if (intersecting_corners_count == 0) {
                LuccaSide direction;
                /* find out where expanding_tile was in relation to this one */
                if (displaced_tile->rect.c[LUCCA_SIDE_RIGHT] < expanding_tile->rect.c[LUCCA_SIDE_LEFT])
                    direction = LUCCA_SIDE_RIGHT;
                else if (displaced_tile->rect.c[LUCCA_SIDE_LEFT] > expanding_tile->rect.c[LUCCA_SIDE_RIGHT])
                    direction = LUCCA_SIDE_LEFT;
                else if (displaced_tile->rect.c[LUCCA_SIDE_BOTTOM] < expanding_tile->rect.c[LUCCA_SIDE_TOP])
                    direction = LUCCA_SIDE_BOTTOM;
                else if (displaced_tile->rect.c[LUCCA_SIDE_TOP] > expanding_tile->rect.c[LUCCA_SIDE_BOTTOM])
                    direction = LUCCA_SIDE_TOP;
                else {
                    g_error("lucca_layouttile_calc_resize called on inconsistent layout");
                    return;
                }
                
                if (lucca_side_coords_le(direction, expanding_tile->new_rect.c[lucca_side_opposite(direction)], displaced_tile->new_rect.c[lucca_side_opposite(direction)])) {
                    /* expanding_tile has reached displaced_tile's opposite edge; we need to shrink this one sideways and fill a gap, changing it to a two-corner case */
                    LuccaSide p_direction = lucca_side_perpendicular(direction);
                    gint c1 = displaced_tile->new_rect.c[direction] + lucca_side_sign(direction);
                    gint c2 = expanding_tile->new_rect.c[p_direction] + lucca_side_sign(p_direction);
                    gint c2_stop = displaced_tile->new_rect.c[p_direction] + lucca_side_sign(p_direction);
                    
                    while (lucca_side_coords_lt(p_direction, c2, c2_stop)) {
                        LuccaLayoutTile* gapfiller = lucca_layout_newtile_at_point(layout, lucca_side_order_point(direction, c1, c2));
                        g_assert(gapfiller != NULL);
                        
                        gapfiller->new_rect.c[lucca_side_opposite(direction)] = displaced_tile->new_rect.c[lucca_side_opposite(direction)];
                        if (lucca_side_coords_lt(p_direction, c2_stop, gapfiller->new_rect.c[p_direction])) {
                            g_queue_push_tail(to_calc_resize, gapfiller);
                            break;
                        }
                        else {
                            if (!g_slist_find(layout->to_resize, gapfiller))
                                layout->to_resize = g_slist_prepend(layout->to_resize, gapfiller);
                            c2 = gapfiller->new_rect.c[p_direction] + lucca_side_sign(p_direction);
                        }
                    }
                    /* only shrink displaced_tile enough that two corners intersect expanding_tile */
                    displaced_tile->new_rect.c[p_direction] = expanding_tile->new_rect.c[p_direction];
                    if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_LEFT], displaced_tile->new_rect.c[LUCCA_SIDE_TOP])) {
                        intersecting_corners |= 0x8;
                        intersecting_corners_count++;
                    }
                    if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_RIGHT], displaced_tile->new_rect.c[LUCCA_SIDE_TOP])) {
                        intersecting_corners |= 0x4;
                        intersecting_corners_count++;
                    }
                    if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_LEFT], displaced_tile->new_rect.c[LUCCA_SIDE_BOTTOM])) {
                        intersecting_corners |= 0x2;
                        intersecting_corners_count++;
                    }
                    if (lucca_rect_contains_point(expanding_tile->new_rect, displaced_tile->new_rect.c[LUCCA_SIDE_RIGHT], displaced_tile->new_rect.c[LUCCA_SIDE_BOTTOM])) {
                        intersecting_corners |= 0x1;
                        intersecting_corners_count++;
                    }
                    g_assert(intersecting_corners_count == 2);
                }
                /* else the code for 1 corner will take care of this */
            }
            
            switch (intersecting_corners_count) {
            case 4:
                /* displaced_tile is inside expanding_tile; destroy it */
                layout->to_delete = g_slist_prepend(layout->to_delete, displaced_tile);
                displaced_tile->new_valid = FALSE;
                break;
            case 2:
                /* a rectanglular slice is displaced; move a side in */
                switch (intersecting_corners) {
                case 0xC:
                    intersecting_edge = LUCCA_SIDE_TOP;
                    break;
                case 0x3:
                    intersecting_edge = LUCCA_SIDE_BOTTOM;
                    break;
                case 0xA:
                    intersecting_edge = LUCCA_SIDE_LEFT;
                    break;
                case 0x5:
                    intersecting_edge = LUCCA_SIDE_RIGHT;
                    break;
                default:
                    g_error("internal layout error, intersecting_corners = %x", intersecting_corners);
                    return;
                }
                
                displaced_tile->new_rect.c[intersecting_edge] = expanding_tile->new_rect.c[lucca_side_opposite(intersecting_edge)] - lucca_side_sign(intersecting_edge);
                
                /* if this makes displaced_tile too small, destroy it and make expanding_tile bigger */
                if (lucca_side_horizontal(intersecting_edge))
                    min_thickness = displaced_tile->geometry.min_width;
                else
                    min_thickness = displaced_tile->geometry.min_height;
                if (lucca_rect_get_thickness(displaced_tile->new_rect, intersecting_edge) < min_thickness + layout->border_size) {
                    layout->to_delete = g_slist_prepend(layout->to_delete, displaced_tile);
                    displaced_tile->new_valid = FALSE;
                    expanding_tile->new_rect.c[lucca_side_opposite(intersecting_edge)] = displaced_tile->new_rect.c[lucca_side_opposite(intersecting_edge)];
                    g_queue_push_tail(to_calc_resize, expanding_tile);
                }
                else {
                    if (!g_slist_find(layout->to_resize, displaced_tile))
                        layout->to_resize = g_slist_prepend(layout->to_resize, displaced_tile);
                }
                break;
            case 1: case 0:
                /* if expanding_tile was fully to one side of displaced_tile, we want to move that side inward; this may mean vertically and horizontally */
                if (displaced_tile->rect.c[LUCCA_SIDE_RIGHT] < expanding_tile->rect.c[LUCCA_SIDE_LEFT])
                    _collapse_side_inward(displaced_tile, LUCCA_SIDE_RIGHT, expanding_tile, to_calc_resize);
                else if (displaced_tile->rect.c[LUCCA_SIDE_LEFT] > expanding_tile->rect.c[LUCCA_SIDE_RIGHT])
                    _collapse_side_inward(displaced_tile, LUCCA_SIDE_LEFT, expanding_tile, to_calc_resize);
                if (displaced_tile->rect.c[LUCCA_SIDE_BOTTOM] < expanding_tile->rect.c[LUCCA_SIDE_TOP])
                    _collapse_side_inward(displaced_tile, LUCCA_SIDE_BOTTOM, expanding_tile, to_calc_resize);
                else if (displaced_tile->rect.c[LUCCA_SIDE_TOP] > expanding_tile->rect.c[LUCCA_SIDE_BOTTOM])
                    _collapse_side_inward(displaced_tile, LUCCA_SIDE_TOP, expanding_tile, to_calc_resize);
                break;
            default:
                g_error("internal layout error, intersecting_corner_count = %i", intersecting_corners_count);
            }
        }
    }
    
    g_queue_free(to_calc_resize);
}

void lucca_layouttile_resize_preview(LuccaLayoutTile* tile, gint* x, gint* y, gint* width, gint* height) {
    LuccaLayout* layout = tile->layout;
    
    lucca_layouttile_calc_resize(tile, *x, *y, *width, *height);
    
    g_slist_free(layout->to_resize);
    g_slist_free(layout->to_delete);
    
    *x = lucca_rect_getx(tile->new_rect);
    *y = lucca_rect_gety(tile->new_rect);
    *width = lucca_rect_getwidth(tile->new_rect)-layout->border_size;
    *height = lucca_rect_getheight(tile->new_rect)-layout->border_size;
}

void lucca_layouttile_resize_block(LuccaLayoutTile* tile, LuccaSide side, guint* x, guint* y, guint* width, guint* height) {
    LuccaLayout* layout = tile->layout;
    LuccaRect current_rect = lucca_rect_from_xywh(*x, *y, *width, *height);
    LuccaRect outside_rect;
    GSList* item;
    gint closest_edge;
    
    /* draw a line just outside the given rectangle */
    outside_rect.c[side] = current_rect.c[side] + (layout->border_size+1)*lucca_side_sign(side);
    outside_rect.c[lucca_side_opposite(side)] = current_rect.c[side] + (layout->border_size+1)*lucca_side_sign(side);
    outside_rect.c[lucca_side_perpendicular(side)] = current_rect.c[lucca_side_perpendicular(side)] + (layout->border_size+1)*lucca_side_sign(lucca_side_perpendicular(side));
    outside_rect.c[lucca_side_opposite(lucca_side_perpendicular(side))] = current_rect.c[lucca_side_opposite(lucca_side_perpendicular(side))] - (layout->border_size+1)*lucca_side_sign(lucca_side_perpendicular(side));
    
    /* find the rectangle that intersects the line and has the closest opposite edge */
    closest_edge = lucca_rect_from_xywh(0, 0, layout->width, layout->height).c[side];
    for (item=layout->tiles; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        if (lucca_rects_intersect(outside_rect, tile_rect(t))) {
            if (lucca_side_coords_lt(side, tile_rect(t).c[side], closest_edge)) {
                closest_edge = tile_rect(t).c[side];
            }
        }
    }
    
    current_rect.c[side] = closest_edge;
    
    *x = lucca_rect_getx(current_rect);
    *y = lucca_rect_gety(current_rect);
    *width = lucca_rect_getwidth(current_rect);
    *height = lucca_rect_getheight(current_rect);
}

void lucca_layouttile_resize(LuccaLayoutTile* tile, gint x, gint y, gint width, gint height) {
    LuccaLayout* layout = tile->layout;
    GSList* item;
    
    lucca_layouttile_calc_resize(tile, x, y, width, height);
    
    for (item=layout->to_delete; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        layout->tiles = g_slist_remove(layout->tiles, t);
        t->valid = FALSE;
    }
    
    for (item=layout->to_resize; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        t->rect = t->new_rect;
    }
    
    for (item=layout->to_delete; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        g_signal_emit(t, LUCCA_LAYOUTTILE_GET_CLASS(t)->delete_signal_id, 0);
    }
    
    for (item=layout->to_resize; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        g_signal_emit(t, LUCCA_LAYOUTTILE_GET_CLASS(t)->configure_signal_id, 0);
    }
    
    for (item=layout->to_delete; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        g_object_unref(t);
    }
    
    g_slist_free(layout->to_resize);
    g_slist_free(layout->to_delete);
}

void lucca_layouttile_ensure_size(LuccaLayoutTile* tile, GSList** configured, GSList** deleted) {
    LuccaLayout* layout = tile->layout;
    LuccaRect new_rect;
    LuccaRect layout_rect;
    LuccaSide direction;
    gboolean rect_changed=FALSE;
    GSList* item;
    
    g_assert(tile->geometry.min_width <= layout->width);
    g_assert(tile->geometry.min_height <= layout->height);
    
    new_rect = tile_rect(tile);
    layout_rect = lucca_rect_from_xywh(0, 0, layout->width, layout->height);
    
    for (direction=0; direction<4; direction++) {
        gint min_thickness;
        if (lucca_side_horizontal(direction))
            min_thickness = tile->geometry.min_width;
        else
            min_thickness = tile->geometry.min_height;
        /* is the tile too thin in this direction? */
        if (lucca_rect_get_thickness(new_rect, direction) < min_thickness) {
            /* expand the tile outward in this direction */
            new_rect = lucca_rect_set_thickness(new_rect, direction, min_thickness);
            /* ..but don't go further than the edge of the layout */
            if (lucca_side_coords_lt(direction, layout_rect.c[direction], new_rect.c[direction])) {
                new_rect.c[direction] = layout_rect.c[direction];
            }
            rect_changed=TRUE;
        }
    }
    
    /* now, resize the tile */
    if (rect_changed) {
        
        lucca_layouttile_calc_resize(tile, lucca_rect_getx(new_rect), lucca_rect_gety(new_rect), lucca_rect_getwidth(new_rect), lucca_rect_getheight(new_rect));
        
        /* update resized tiles */
        for (item=layout->to_resize; item != NULL; item=item->next) {
            
            LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
            
            t->rect = t->new_rect;
            
            if (!g_slist_find(*configured, t))
                *configured = g_slist_prepend(*configured, t);
        }
        
        /* update deleted tiles */
        for (item=layout->to_delete; item != NULL; item=item->next) {
            LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
            
            *configured = g_slist_remove(*configured, t);
            *deleted = g_slist_prepend(*deleted, t);
            
            layout->tiles = g_slist_remove(layout->tiles, t);
            t->valid = FALSE;
        }
    }
}

void lucca_layouttile_set_geometry_hints(LuccaLayoutTile* tile, GdkGeometry geometry, GdkWindowHints geom_mask) {
    GSList* configured=NULL;
    GSList* deleted=NULL;
    GSList* item;
    
    tile->geometry = geometry;
    tile->geom_mask = geom_mask;
    
    if (!(geom_mask&GDK_HINT_MIN_SIZE)) {
        /* internally, all tiles have a minimum size of at least 0 */
        tile->geometry.min_width = 0;
        tile->geometry.min_height = 0;
    }
    
    /* resize tile if necessary for the new minimum size */
    lucca_layouttile_ensure_size(tile, &configured, &deleted);
    
    for (item=deleted; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        g_signal_emit(t, LUCCA_LAYOUTTILE_GET_CLASS(t)->delete_signal_id, 0);
    }
    
    for (item=configured; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        g_signal_emit(t, LUCCA_LAYOUTTILE_GET_CLASS(t)->configure_signal_id, 0);
    }
    
    for (item=deleted; item != NULL; item=item->next) {
        LuccaLayoutTile* t = LUCCA_LAYOUTTILE(item->data);
        
        g_object_unref(t);
    }
    
    g_slist_free(configured);
    g_slist_free(deleted);
}

void lucca_layouttile_get_geometry_hints(LuccaLayoutTile* tile, GdkGeometry* geometry, GdkWindowHints* geom_mask) {
    *geometry = tile->geometry;
    *geom_mask = tile->geom_mask;
}

