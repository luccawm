/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-display-private.h"

/* Implementation of LuccaWindow */

static GObjectClass* parent_class = NULL;

static GData* state_atom_constants;
static GHashTable* state_constant_atoms;

static GData* type_atom_constants;

GdkAtom netwm_state;
GdkAtom gdkatom_atom;

/* property id's */
enum {
    PROP_VALID=1,
    PROP_REQUESTED_TITLE,
    PROP_WINDOW,
    PROP_FOCUSED,
    PROP_TYPE,
};

static void lucca_window_init(GTypeInstance* instance, gpointer g_class) {
    LuccaWindow* window = LUCCA_WINDOW(instance);

    window->valid = FALSE;
    
    window->dispose_has_run = FALSE;
    
    window->display = NULL;
    window->window = NULL;
    window->xwindow = 0;
    window->xwmhints = NULL;
    window->properties_set = NULL;
    window->window_parent = NULL;
    window->requested_title = NULL;
    window->owner_callback = NULL;
    window->bin = NULL;
    window->focused = FALSE;
    window->ignore_focus = FALSE;
    
    window->supports_delete_window = FALSE;
}

static void lucca_window_dispose(GObject* obj) {
    LuccaWindow* window = LUCCA_WINDOW(obj);
    
    g_assert(window->internal || !window->valid);
    
    if (window->dispose_has_run) {
        return;
    }
    window->dispose_has_run = TRUE;
    
    g_object_unref(window->display);
    window->display = NULL;
    
    g_object_unref(window->window);
    window->window = NULL;
    
    if (window->bin)
        g_object_unref(window->bin);
    window->bin = NULL;
    
    window->xwindow = 0;
    
    if (window->xwmhints) {
        XFree(window->xwmhints);
    }
    
    g_datalist_clear(&window->properties_set);
    
    g_object_unref(window->window_parent);
    window->window_parent = NULL;
    
    g_free(window->requested_title);
    window->requested_title = NULL;
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_window_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaWindow* window = LUCCA_WINDOW(object);
    
    switch (property_id) {
    case PROP_VALID:
        g_value_set_boolean(value, window->valid);
        break;
    case PROP_REQUESTED_TITLE:
        g_value_set_string(value, window->requested_title);
        break;
    case PROP_WINDOW:
        g_value_set_object(value, window->bin);
        break;
    case PROP_FOCUSED:
        g_value_set_boolean(value, window->focused);
        break;
    case PROP_TYPE:
        g_value_set_enum(value, window->type);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_window_class_init(gpointer g_class, gpointer class_data) {
    LuccaWindowClass* klass = (LuccaWindowClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_window_dispose;
    gobclass->get_property = lucca_window_get_property;

    /* properties */
    pspec = g_param_spec_boolean(
        "valid",
        "valid",
        "TRUE if this manages a display's windows",
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_VALID, pspec);
    
    pspec = g_param_spec_string(
        "requested-title",
        "requested title",
        "The title the application requested for this window",
        "", /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_REQUESTED_TITLE, pspec);
    
    pspec = g_param_spec_object(
        "window",
        "window",
        "The GTK window for an internal LuccaWindow",
        GTK_TYPE_WINDOW,
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WINDOW, pspec);

    pspec = g_param_spec_boolean(
        "focused",
        "focused",
        "TRUE if this window has the focus",
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_FOCUSED, pspec);

    pspec = g_param_spec_enum(
        "type",
        "type",
        "the type of this window",
        LUCCA_TYPE_TYPE,
        LUCCA_TYPE_NORMAL, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_TYPE, pspec);

    /* signals */

    klass->withdraw_signal_id = g_signal_new(
        "withdraw",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    klass->gain_focus_signal_id = g_signal_new(
        "gain-focus",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    klass->lose_focus_signal_id = g_signal_new(
        "lose-focus",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    klass->mouse_in_signal_id = g_signal_new(
        "mouse-in",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    klass->mouse_out_signal_id = g_signal_new(
        "mouse-out",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    klass->state_request_signal_id = g_signal_new(
        "state-request",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__INT,
        G_TYPE_NONE,
        1, /* n_params */
        G_TYPE_INT
    );

    parent_class = g_type_class_peek_parent(klass);
    
    g_datalist_init(&state_atom_constants);
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_MODAL", GINT_TO_POINTER(LUCCA_STATE_MODAL));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_STICKY", GINT_TO_POINTER(LUCCA_STATE_STICKY));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_MAXIMIZED_VIRT", GINT_TO_POINTER(LUCCA_STATE_MAXIMIZED_VIRT));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_MAXIMIZED_HORZ", GINT_TO_POINTER(LUCCA_STATE_MAXIMIZED_HORZ));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_SHADED", GINT_TO_POINTER(LUCCA_STATE_SHADED));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_SKIP_TASKBAR", GINT_TO_POINTER(LUCCA_STATE_SKIP_TASKBAR));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_SKIP_PAGER", GINT_TO_POINTER(LUCCA_STATE_SKIP_PAGER));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_HIDDEN", GINT_TO_POINTER(LUCCA_STATE_HIDDEN));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_FULLSCREEN", GINT_TO_POINTER(LUCCA_STATE_FULLSCREEN));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_ABOVE", GINT_TO_POINTER(LUCCA_STATE_ABOVE));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_BELOW", GINT_TO_POINTER(LUCCA_STATE_BELOW));
    g_datalist_set_data(&state_atom_constants, "_NET_WM_STATE_DEMANDS_ATTENTION", GINT_TO_POINTER(LUCCA_STATE_DEMANDS_ATTENTION));
    
    state_constant_atoms = g_hash_table_new(g_direct_hash, g_direct_equal);
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_MODAL), "_NET_WM_STATE_MODAL");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_STICKY), "_NET_WM_STATE_STICKY");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_MAXIMIZED_VIRT), "_NET_WM_STATE_MAXIMIZED_VIRT");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_MAXIMIZED_HORZ), "_NET_WM_STATE_MAXIMIZED_HORZ");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_SHADED), "_NET_WM_STATE_SHADED");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_SKIP_TASKBAR), "_NET_WM_STATE_SKIP_TASKBAR");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_SKIP_PAGER), "_NET_WM_SKIP_PAGER");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_HIDDEN), "_NET_WM_STATE_HIDDEN");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_FULLSCREEN), "_NET_WM_STATE_FULLSCREEN");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_ABOVE), "_NET_WM_STATE_ABOVE");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_BELOW), "_NET_WM_STATE_BELOW");
    g_hash_table_insert(state_constant_atoms, GINT_TO_POINTER(LUCCA_STATE_DEMANDS_ATTENTION), "_NET_WM_STATE_DEMANDS_ATTENTION");
    
    g_datalist_init(&type_atom_constants);
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_NORMAL", GINT_TO_POINTER(LUCCA_TYPE_NORMAL));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_DESKTOP", GINT_TO_POINTER(LUCCA_TYPE_DESKTOP));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_DOCK", GINT_TO_POINTER(LUCCA_TYPE_DOCK));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_TOOLBAR", GINT_TO_POINTER(LUCCA_TYPE_TOOLBAR));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_MENU", GINT_TO_POINTER(LUCCA_TYPE_MENU));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_UTILITY", GINT_TO_POINTER(LUCCA_TYPE_UTILITY));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_SPLASH", GINT_TO_POINTER(LUCCA_TYPE_SPLASH));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_DIALOG", GINT_TO_POINTER(LUCCA_TYPE_DIALOG));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_DROPDOWN_MENU", GINT_TO_POINTER(LUCCA_TYPE_DROPDOWN_MENU));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_POPUP_MENU", GINT_TO_POINTER(LUCCA_TYPE_POPUP_MENU));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_TOOLTIP", GINT_TO_POINTER(LUCCA_TYPE_TOOLTIP));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_NOTIFICATION", GINT_TO_POINTER(LUCCA_TYPE_NOTIFICATION));
    g_datalist_set_data(&type_atom_constants, "_NET_WM_WINDOW_TYPE_DND", GINT_TO_POINTER(LUCCA_TYPE_DND));
    
    netwm_state = gdk_atom_intern("_NET_WM_STATE", FALSE);
    gdkatom_atom = gdk_atom_intern("ATOM", FALSE);
}

GType lucca_window_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaWindowClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_window_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaWindow),
            0, /* n_preallocs */
            lucca_window_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaWindowType", &info, 0);
    }
    return type;
}

/* Method definitions */

gboolean _withdraw_window(gpointer data) {
    LuccaWindow* window = LUCCA_WINDOW(data);

    lucca_window_withdraw_finish(window);
    
    return FALSE;
}

/* retrieves a window's title from the X server; the result should be freed with g_free() */
static gchar* _get_requested_title(LuccaWindow* window) {
    gchar* result;
    
    result = lucca_window_get_utf8(window, "_NET_WM_NAME");
    
    if (result == NULL) {
        result = lucca_window_get_utf8(window, "WM_NAME");
    }
    
    if (result == NULL) {
        result = g_strdup("");
    }
    
    return result;
}

static XWMHints* _get_wm_hints(LuccaWindow* window) {
    XWMHints* result;
    
    if (window->badwindow || !g_datalist_get_data(&window->properties_set, "WM_HINTS")) {
        /* we need xwmhints to exist for each window; make something up */
        result = XAllocWMHints();
        result->flags = 0;
        return result;
    }
    
    gdk_error_trap_push();
    result = XGetWMHints(window->display->xdisplay, window->xwindow);
    #ifdef LUCCA_WM_HACKS
    /* SDL's X code has a bug where, when it sets an icon, it unsets the
     * input hint. SDL windows always want input, but the ICCCM tells me not to
     * focus windows unless the input hint is set. And for xclock, this makes
     * sense. Most window managers will respond to this situation in a way that
     * makes it possible for that window to get input anyway, either setting
     * focus to PointerRoot or to an ancestor of the window. Thus, SDL gets away
     * with it. We differentiate between SDL, which wants the focus, and xclock,
     * which doesn't, by checking whether any client has selected for keyboard
     * input on this window.
     */
    if (result && !(result->flags & InputHint) && window->xattr.all_event_masks & (KeyPressMask|KeyReleaseMask))
    {
        result->flags |= InputHint;
        result->input = True;
    }
    #endif
    gdk_flush();
    if (gdk_error_trap_pop() == BadWindow) {
        window->badwindow = TRUE;
    }
    
    return result;
}

static void _update_normal_hints(LuccaWindow* window) {
    Status status;
    
    if (window->badwindow || !g_datalist_get_data(&window->properties_set, "WM_NORMAL_HINTS")) {
        window->xsizehints_supplied = 0;
        return;
    }
    
    gdk_error_trap_push();
    status = XGetWMNormalHints(window->display->xdisplay, window->xwindow, &window->xsizehints, &window->xsizehints_supplied);
    gdk_flush();
    if (gdk_error_trap_pop() == BadWindow) {
        window->badwindow = TRUE;
        window->xsizehints_supplied = 0;
    }
}

static void _update_protocols(LuccaWindow* window) {
    Atom* proto_atoms;
    guint proto_atom_count;
    guint i;
    
    g_assert(window->valid);
    
    proto_atoms = lucca_window_get_atoms(window, "WM_PROTOCOLS", &proto_atom_count);
    
    if (proto_atoms != NULL) {
        for (i=0; i<proto_atom_count; i++) {
            char* name = XGetAtomName(window->display->xdisplay, proto_atoms[i]);
            if (strcmp(name, "WM_DELETE_WINDOW") == 0) {
                window->supports_delete_window = TRUE;
            }
            XFree(name);
        }
        XFree(proto_atoms);
    }
}

static void _update_type(LuccaWindow* window) {
    Atom* type_atoms;
    guint type_atom_count;
    guint i;
    LuccaType type=0;
    
    g_assert(window->valid);
    
    type_atoms = lucca_window_get_atoms(window, "_NET_WM_WINDOW_TYPE", &type_atom_count);
    
    if (type_atoms != NULL) {
        for (i=0; i<type_atom_count; i++) {
            char* name = XGetAtomName(window->display->xdisplay, type_atoms[i]);
            LuccaType type = GPOINTER_TO_INT(g_datalist_get_data(&type_atom_constants, name));
            
            XFree(name);
            if (type != 0) {
                XFree(type_atoms);
                window->type = type;
                return;
            }
        }
        XFree(type_atoms);
    }
    else {
        /* EWMH states that transients are _NET_WM_WINDOW_TYPE_DIALOG and others are _NET_WM_WINDOW_TYPE_NORMAL */
        if (g_datalist_get_data(&window->properties_set, "WM_TRANSIENT_FOR"))
            type = LUCCA_TYPE_DIALOG;
        else
            type = LUCCA_TYPE_NORMAL;
    }
    
    if (type == 0) {
        type = LUCCA_TYPE_NORMAL;
    }
    
    window->type = type;
}

static void _update_struts(LuccaWindow* window) {
    int* result;
    guint result_count;
    
    g_assert(window->valid);
    
    if ((result = lucca_window_get_cardinals(window, "_NET_WM_STRUT_PARTIAL", &result_count))) {
        if (result_count >= 12) {
            window->strut_left = result[0];
            window->strut_right = result[1];
            window->strut_top = result[2];
            window->strut_bottom = result[3];
            window->left_start_y = result[4];
            window->left_end_y = result[5];
            window->right_start_y = result[6];
            window->right_end_y = result[7];
            window->top_start_x = result[8];
            window->top_end_x = result[9];
            window->bottom_start_x = result[10];
            window->bottom_end_x = result[11];
        }
        else {
            window->strut_left = 0;
            window->strut_right = 0;
            window->strut_top = 0;
            window->strut_bottom = 0;
        }
        
        XFree(result);
    }
    else if ((result = lucca_window_get_cardinals(window, "_NET_WM_STRUT", &result_count))) {
        gint width, height;
        
        g_object_get(window->display->default_screen, "width", &width, "height", &height, NULL);
    
        if (result_count >= 4) {
            window->strut_left = result[0];
            window->strut_right = result[1];
            window->strut_top = result[2];
            window->strut_bottom = result[3];
            window->left_start_y = 0;
            window->left_end_y = height-1;
            window->right_start_y = 0;
            window->right_end_y = height-1;
            window->top_start_x = 0;
            window->top_end_x = width-1;
            window->bottom_start_x = 0;
            window->bottom_end_x = width-1;
        }
        else {
            window->strut_left = 0;
            window->strut_right = 0;
            window->strut_top = 0;
            window->strut_bottom = 0;
        }
        
        XFree(result);
    }
    else {
        window->strut_left = 0;
        window->strut_right = 0;
        window->strut_top = 0;
        window->strut_bottom = 0;
    }
}

static LuccaState _get_states(LuccaWindow* window) {
    Atom* state_atoms;
    guint state_atom_count;
    guint i;
    
    g_assert(window->valid);
    
    state_atoms = lucca_window_get_atoms(window, "_NET_WM_STATE", &state_atom_count);
    
    if (state_atoms == NULL) {
        /* no _NET_WM_STATE, fall back on WM_HINTS */
        if ((window->xwmhints->flags & StateHint) && (window->xwmhints->initial_state == IconicState)) {
            return LUCCA_STATE_HIDDEN;
        }
        else {
            return 0;
        }
    }
    else {
        LuccaState result=0;
        for (i=0; i<state_atom_count; i++) {
            char* name = XGetAtomName(window->display->xdisplay, state_atoms[i]);
            result |= GPOINTER_TO_INT(g_datalist_get_data(&state_atom_constants, name));
            XFree(name);
        }
        XFree(state_atoms);
        return result;
    }
}

LuccaWindow* lucca_window_new(Window xwindow, LuccaScreen* screen, gboolean existing) {
    XWindowAttributes xattr;
    LuccaWindow* result;
    GData* properties_set;
    Atom* property_atoms;
    int property_count;
    int i;
    Status status;
    GdkWindow* gdkwindow;
    
    gdkwindow = gdk_window_foreign_new_for_display(screen->display->display, xwindow);
    if (g_hash_table_lookup(screen->display->valid_windows, gdkwindow)) {
        return NULL;
    }
    
    /* make sure it wants to be managed */
    gdk_error_trap_push();
    status = XGetWindowAttributes(screen->display->xdisplay, xwindow, &xattr);
    gdk_flush();
    if (gdk_error_trap_pop()) {
        /* the window doesn't exist anymore */
        return NULL;
    }
    if ((existing && xattr.map_state != IsViewable) || xattr.override_redirect) {
        /* this is an override_redirect or withdrawn window; ignore it */
        return NULL;
    }
    
    /* get a list of properties for the window; knowing if a property exists makes requesting various information faster later */
    g_datalist_init(&properties_set);
    gdk_error_trap_push();
    property_atoms = XListProperties(screen->display->xdisplay, xwindow, &property_count);
    gdk_flush();
    if (gdk_error_trap_pop()) {
        /* the window doesn't exist anymore */
        g_datalist_clear(&properties_set);
        return NULL;
    }
    for (i=0; i<property_count; i++) {
        char* name = XGetAtomName(screen->display->xdisplay, property_atoms[i]);
        g_datalist_set_data(&properties_set, name, GINT_TO_POINTER(1));
        XFree(name);
    }
    XFree(property_atoms);
    
    result = g_object_new(LUCCA_TYPE_WINDOW, NULL);
    
    result->valid = TRUE;
    result->badwindow = FALSE;
    result->internal = FALSE;
    result->window = gdkwindow;
    result->xwindow = xwindow;
    result->xattr = xattr;
    result->display = g_object_ref(screen->display);
    result->properties_set = properties_set;
    
    if (xattr.map_state == IsViewable) {
        result->icccm_state = NormalState;
    }
    else {
        result->icccm_state = WithdrawnState;
    }
    
    /* get other information about the window from the server */
    result->requested_title = _get_requested_title(result);
    result->xwmhints = _get_wm_hints(result);
    _update_normal_hints(result);
    result->window_parent = g_object_ref(screen->root);
    _update_protocols(result);
    _update_type(result);
    _update_struts(result);
    result->netwm_states = result->requested_states = _get_states(result);
    
    /* select for events we need to report about this window */
    lucca_display_add_x_events(screen->display, gdkwindow, FocusChangeMask|EnterWindowMask|LeaveWindowMask|PropertyChangeMask);

    g_hash_table_insert(screen->display->valid_windows, result->window, result);
    
    /* add this window to the save-set so it won't be destroyed if we reparent it and crash */
    XAddToSaveSet(screen->display->xdisplay, xwindow);
        
    g_signal_emit(screen->display, LUCCA_DISPLAY_GET_CLASS(screen->display)->new_window_signal_id, 0, result);
    
    return result;
}

LuccaWindow* lucca_window_new_internal(LuccaBin* bin, LuccaScreen* screen) {
    GdkWindow *window;
    Window xwindow;
    XWindowAttributes xattr;
    LuccaWindow* result;
    GData* properties_set;
    Atom* property_atoms;
    int property_count;
    int i;
    
    window = g_object_ref(GTK_WIDGET(bin)->window);
    xwindow = GDK_WINDOW_XWINDOW(window);
    
    XGetWindowAttributes(screen->display->xdisplay, xwindow, &xattr);

    /* get a list of properties for the window; knowing if a property exists makes requesting various information faster later */
    g_datalist_init(&properties_set);
    gdk_error_trap_push();
    property_atoms = XListProperties(screen->display->xdisplay, xwindow, &property_count);
    gdk_flush();
    if (gdk_error_trap_pop()) {
        /* the window doesn't exist anymore */
        g_datalist_clear(&properties_set);
        return NULL;
    }
    for (i=0; i<property_count; i++) {
        char* name = XGetAtomName(screen->display->xdisplay, property_atoms[i]);
        g_datalist_set_data(&properties_set, name, GINT_TO_POINTER(1));
        XFree(name);
    }
    XFree(property_atoms);
    
    result = g_object_new(LUCCA_TYPE_WINDOW, NULL);
    
    result->valid = TRUE;
    result->badwindow = FALSE;
    result->internal = TRUE;
    result->xattr = xattr;
    result->window = window;
    result->xwindow = xwindow;
    result->bin = bin;
    result->display = g_object_ref(screen->display);
    result->properties_set = properties_set;
    result->icccm_state = WithdrawnState;
    result->strut_left = 0;
    result->strut_right = 0;
    result->strut_top = 0;
    result->strut_bottom = 0;
    result->requested_states = 0;
    
    /* get other information about the window from the server */
    result->requested_title = _get_requested_title(result);
    result->xwmhints = _get_wm_hints(result);
    _update_normal_hints(result);
    result->window_parent = g_object_ref(screen->root);
    _update_protocols(result);
    
/*  This is not a "managed" window so don't add it to valid_windows
    hash_key = g_malloc(sizeof(Window));
    *hash_key = xwindow;
    g_hash_table_insert(screen->display->valid_windows, hash_key, result);*/
    
    return result;
}

gchar* lucca_window_get_utf8(LuccaWindow* window, gchar*  property) {
    int status;
    Atom prop;
    Atom xactual_type;
    int actual_format;
    unsigned long nitems;
    unsigned long length;
    unsigned long bytes_after;
    unsigned char* data=NULL;
    gchar* value=NULL;
    gchar** list=NULL;
    
    /* if the property isn't set, return no strings */
    if (window->badwindow || !g_datalist_get_data(&window->properties_set, property)) {
        return NULL;
    }
    
    prop = XInternAtom(window->display->xdisplay, property, True);
    
    gdk_error_trap_push();
    status = XGetWindowProperty(
        window->display->xdisplay, /* display */
        window->xwindow, /* window */
        prop, /* property */
        0, /* long_offset */
        0, /* long_length */
        False, /* delete */
        AnyPropertyType, /* req_type */
        &xactual_type, /* actual_type_return */
        &actual_format, /* actual_format_return */
        &nitems, /* nitems_return */
        &length, /* bytes_after_return */
        &data /* prop_return */
        );
    if (data != NULL) {
        XFree(data);
    }
    
    if (status == Success) {
        status = XGetWindowProperty(
            window->display->xdisplay, /* display */
            window->xwindow, /* window */
            prop, /* property */
            0, /* long_offset */
            (length+3)/4, /* long_length */
            False, /* delete */
            AnyPropertyType, /* req_type */
            &xactual_type, /* actual_type_return */
            &actual_format, /* actual_format_return */
            &nitems, /* nitems_return */
            &bytes_after, /* bytes_after_return */
            &data /* prop_return */
            );
        
        if (status == Success) {
            char* encoding = XGetAtomName(window->display->xdisplay, xactual_type);
            nitems = gdk_text_property_to_utf8_list_for_display(
                window->display->display, /* display */
                gdk_atom_intern(encoding, FALSE), /* encoding */
                actual_format, /* format */
                data, /* text */
                length, /* length */
                &list /* list */
                );
            if (nitems >= 1) {
                value = g_strdup(list[0]);
            }
            g_strfreev(list);
            XFree(data);
            XFree(encoding);
        }
    }
    
    gdk_flush();
    if (gdk_error_trap_pop() == BadWindow) {
        window->badwindow = TRUE;
    }
    
    return value;
}

Atom* lucca_window_get_atoms(LuccaWindow* window, gchar*  property, guint* length) {
    int status;
    Atom prop;
    Atom xactual_type;
    int actual_format;
    unsigned long nitems;
    unsigned long size;
    unsigned long bytes_after;
    unsigned char* data=NULL;
    
    *length = 0;
    
    /* if the property isn't set, return no strings */
    if (window->badwindow || !g_datalist_get_data(&window->properties_set, property)) {
        return NULL;
    }
    
    prop = XInternAtom(window->display->xdisplay, property, True);
    
    gdk_error_trap_push();
    status = XGetWindowProperty(
        window->display->xdisplay, /* display */
        window->xwindow, /* window */
        prop, /* property */
        0, /* long_offset */
        0, /* long_length */
        False, /* delete */
        window->display->xatom_atom, /* req_type */
        &xactual_type, /* actual_type_return */
        &actual_format, /* actual_format_return */
        &nitems, /* nitems_return */
        &size, /* bytes_after_return */
        &data /* prop_return */
        );
    if (data != NULL) {
        XFree(data);
    }
    
    if (status == Success) {
        status = XGetWindowProperty(
            window->display->xdisplay, /* display */
            window->xwindow, /* window */
            prop, /* property */
            0, /* long_offset */
            (size+3)/4, /* long_length */
            False, /* delete */
            window->display->xatom_atom, /* req_type */
            &xactual_type, /* actual_type_return */
            &actual_format, /* actual_format_return */
            &nitems, /* nitems_return */
            &bytes_after, /* bytes_after_return */
            &data /* prop_return */
            );
        
        if (status == Success) {
            *length = size/sizeof(Atom);
        }
    }
    
    gdk_flush();
    if (gdk_error_trap_pop() == BadWindow) {
        window->badwindow = TRUE;
    }
    
    return (Atom*)data;
}

int* lucca_window_get_cardinals(LuccaWindow* window, gchar*  property, guint* length) {
    int status;
    Atom prop;
    Atom xactual_type;
    int actual_format;
    unsigned long nitems;
    unsigned long size;
    unsigned long bytes_after;
    unsigned char* data=NULL;
    
    *length = 0;
    
    /* if the property isn't set, return no strings */
    if (window->badwindow || !g_datalist_get_data(&window->properties_set, property)) {
        return NULL;
    }
    
    prop = XInternAtom(window->display->xdisplay, property, True);
    
    gdk_error_trap_push();
    status = XGetWindowProperty(
        window->display->xdisplay, /* display */
        window->xwindow, /* window */
        prop, /* property */
        0, /* long_offset */
        0, /* long_length */
        False, /* delete */
        window->display->xcardinal_atom, /* req_type */
        &xactual_type, /* actual_type_return */
        &actual_format, /* actual_format_return */
        &nitems, /* nitems_return */
        &size, /* bytes_after_return */
        &data /* prop_return */
        );
    if (data != NULL) {
        XFree(data);
    }
    
    if (status == Success) {
        status = XGetWindowProperty(
            window->display->xdisplay, /* display */
            window->xwindow, /* window */
            prop, /* property */
            0, /* long_offset */
            (size+3)/4, /* long_length */
            False, /* delete */
            window->display->xcardinal_atom, /* req_type */
            &xactual_type, /* actual_type_return */
            &actual_format, /* actual_format_return */
            &nitems, /* nitems_return */
            &bytes_after, /* bytes_after_return */
            &data /* prop_return */
            );
        
        if (status == Success) {
            *length = size/sizeof(Atom);
        }
    }
    
    gdk_flush();
    if (gdk_error_trap_pop() == BadWindow) {
        window->badwindow = TRUE;
    }
    
    return (int*)data;
}

void lucca_window_get_geometry_hints(LuccaWindow* window, GdkGeometry* geometry, GdkWindowHints* geom_mask) {
    g_assert(window->valid);
    
    if (window->xwmhints == NULL) {
        *geom_mask = 0;
        return;
    }
    
    if (window->xsizehints_supplied & USPosition) {
        *geom_mask |= GDK_HINT_USER_POS;
    }

    if (window->xsizehints_supplied & USSize) {
        *geom_mask |= GDK_HINT_USER_SIZE;
    }

    if (window->xsizehints_supplied & PPosition) {
        *geom_mask |= GDK_HINT_POS;
    }

    /* if (window->xsizehints_supplied & PSize) ... no field in GdkWindowHints corresponds to this */
    
    if (window->xsizehints_supplied & PMinSize) {
        *geom_mask |= GDK_HINT_MIN_SIZE;
        geometry->min_width = window->xsizehints.min_width;
        geometry->min_height = window->xsizehints.min_height;
    }
    
    if (window->xsizehints_supplied & PMaxSize) {
        *geom_mask |= GDK_HINT_MAX_SIZE;
        geometry->max_width = window->xsizehints.max_width;
        geometry->max_height = window->xsizehints.max_height;
    }
    
    if (window->xsizehints_supplied & PResizeInc) {
        *geom_mask |= GDK_HINT_RESIZE_INC;
        geometry->width_inc = window->xsizehints.width_inc;
        geometry->height_inc = window->xsizehints.height_inc;
    }
    
    if (window->xsizehints_supplied & PAspect) {
        *geom_mask |= GDK_HINT_ASPECT;
        geometry->min_aspect = (gdouble)window->xsizehints.min_aspect.x / (gdouble)window->xsizehints.min_aspect.y;
        geometry->max_aspect = (gdouble)window->xsizehints.max_aspect.x / (gdouble)window->xsizehints.max_aspect.y;
    }
    
    if (window->xsizehints_supplied & PBaseSize) {
        *geom_mask |= GDK_HINT_BASE_SIZE;
        geometry->base_width = window->xsizehints.base_width;
        geometry->base_height = window->xsizehints.base_height;
    }
    
    if (window->xsizehints_supplied & PWinGravity) {
        *geom_mask |= GDK_HINT_WIN_GRAVITY;
        geometry->win_gravity = (GdkGravity)window->xsizehints.win_gravity;
    }
}

LuccaState lucca_window_get_requested_states(LuccaWindow* window) {
    return window->requested_states;
}

static gboolean _stop_ignoring_crossings(gpointer user_data) {
    LuccaDisplay* display = LUCCA_DISPLAY(user_data);

    display->ignore_crossing = FALSE;
    
    return FALSE;
}

void lucca_window_configure(LuccaWindow* window, GdkWindow* parent, gint x, gint y, gint width, gint height, gboolean visible) {
    ICCCM_STATE new_icccm_state;
    gboolean window_is_mapped;
    gint error;
    
    g_assert(window->valid);
    
    if (visible) {
        new_icccm_state = NormalState;
        lucca_window_set_states(window, window->netwm_states & ~LUCCA_STATE_HIDDEN);
    }
    else {
        new_icccm_state = IconicState;
        lucca_window_set_states(window, window->netwm_states | LUCCA_STATE_HIDDEN);
    }
    
    if (window->badwindow) {
        /* the window doesn't exist anymore; pretend everything worked */
        if (parent != window->window_parent) {
            g_object_unref(window->window_parent);
            window->window_parent = g_object_ref(parent);
        }
        window->icccm_state = new_icccm_state;
        window->xattr.x = x;
        window->xattr.y = y;
        window->xattr.width = width;
        window->xattr.height = height;
    }
    else {
        gdk_error_trap_push();
        
        /* if the window is being reparented, we want to unmap, reparent, resize, and then remap it so that we don't end up with an intermediate step where the window is reparented with the wrong size; also, unmap it if we're asked to make it not visible */
        if (((parent != window->window_parent) && (window->icccm_state == NormalState)) || !visible) {
            if (window->internal) {
                gtk_widget_hide(GTK_WIDGET(window->bin));
            }
            else {
                XUnmapWindow(window->display->xdisplay, window->xwindow);
            }
            window_is_mapped = FALSE;
            
            if (window->focused && visible) {
                /* unmapping the window has the unwelcome side-effect of making the window lose focus, which shouldn't happen if it's moving from a visible state to a visible state (because, from a user's perspective, the intermediate steps don't exist); therefore, we surpress focus change events until the window is mapped and we can focus it again */
                window->ignore_focus = TRUE;
            }
            
            /* FIXME: (un)mapping windows can have a side-effect of triggering a crossing event even though the mouse did not move; it's not clear whether these should be reported, but in practice they are usually unwanted so they are not reported for now; we avoid reporting them by ignoring all crossing events after a configure */
            if (!window->display->ignore_crossing) {
                window->display->ignore_crossing = TRUE;
                g_idle_add(_stop_ignoring_crossings, window->display);
            }
        }
        else if (window->icccm_state == NormalState) {
            window_is_mapped = TRUE;
        }
        else {
            window_is_mapped = FALSE;
        }
        
        if ((new_icccm_state != window->icccm_state) && (!window->internal)) {
            ICCCM_WM_STATE new_wm_state;
            GdkAtom wm_state_atom = gdk_atom_intern("WM_STATE", TRUE);
            new_wm_state.state = new_icccm_state;
            new_wm_state.icon = None;
            
            gdk_property_change(
                window->window, /* window */
                wm_state_atom, /* property */
                wm_state_atom, /* type */
                32, /* format */
                GDK_PROP_MODE_REPLACE, /* mode */
                (guchar*)&new_wm_state, /* data */
                2 /* nelements */
                );
            window->icccm_state = new_icccm_state;
        }
        
        if (parent != window->window_parent) {
            if (window->internal) {
                lucca_bin_set_parent(window->bin, parent);
            }
            else {
                /* if the parent is being changed, we need to make sure we get appropriate events on the new parent */
                lucca_display_add_x_events(window->display, parent, SubstructureRedirectMask|SubstructureNotifyMask);
            
                XReparentWindow(window->display->xdisplay, window->xwindow, GDK_WINDOW_XWINDOW(parent), x, y);
            }
            g_object_unref(window->window_parent);
            window->window_parent = g_object_ref(parent);
        }
        
        if (window->internal) {
            lucca_bin_move_resize(window->bin, x, y, width, height);
        }
        else {
            XMoveResizeWindow(window->display->xdisplay, window->xwindow, x, y, (unsigned)width, (unsigned)height);
            window->xattr.x = x;
            window->xattr.y = y;
            window->xattr.width = width;
            window->xattr.height = height;
        }
        
        if (visible && !window_is_mapped) {
            if (window->internal) {
                gtk_widget_show(GTK_WIDGET(window->bin));
            }
            else {
                XMapWindow(window->display->xdisplay, window->xwindow);
            }
            /* FIXME: see above note about (un)mapping windows */
            if (!window->display->ignore_crossing) {
                window->display->ignore_crossing = TRUE;
                g_idle_add(_stop_ignoring_crossings, window->display);
            }
        }
        
        gdk_flush();
        
        error = gdk_error_trap_pop();
        if (error == BadWindow) {
            window->badwindow = TRUE;
        }
        else if (error != 0) {
            g_error("Unexpected X error: %i\n", error);
        }
        
        if (window->ignore_focus)
            lucca_window_give_focus(window);
    }
}

void lucca_window_change_owner(LuccaWindow* window, GClosure* callback) {
    if (window->owner_callback != NULL) {
        GValue value={0,};
        
        g_value_init(&value, LUCCA_TYPE_WINDOW);
        
        g_value_set_object(&value, window);
        
        g_closure_invoke(window->owner_callback,
                            NULL,
                            1,
                            &value,
                            NULL);
        
        g_closure_unref(window->owner_callback);
        
        g_value_unset(&value);
    }
    
    window->owner_callback = callback;
    
    if (callback != NULL) {
        g_closure_ref(callback);
        g_closure_sink(callback);
    }
}

void lucca_window_withdraw_finish(LuccaWindow* window) {
    g_object_ref(window); /* this is needed because removing the window from the list of valid windows may remove the only other reference to this window */

    if (!window->badwindow) {
        GdkAtom wmstate;
        gint error;
        
        gdk_error_trap_push();
        
        if (window->window_parent != window->display->default_screen->root)
            XReparentWindow(window->display->xdisplay, window->xwindow, window->display->default_screen->xroot, 0, 0);
        
        XRemoveFromSaveSet(window->display->xdisplay, window->xwindow);

        wmstate = gdk_atom_intern("WM_STATE", FALSE);
        
        gdk_property_delete(window->window, wmstate);
        
        gdk_flush();
        
        error = gdk_error_trap_pop();
        if (error == BadWindow) {
            window->badwindow = TRUE;
        }
        else if (error != 0) {
            g_error("Unexpected X error: %i\n", error);
        }
    }
    
    window->badwindow = TRUE;
    lucca_window_change_owner(window, NULL);

    g_hash_table_remove(window->display->valid_windows, window->window);
    
    g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->withdraw_signal_id, 0);
    
    g_object_unref(window);
}

void lucca_window_withdraw(LuccaWindow* window) {
    if (window->internal) {
        gtk_widget_hide(GTK_WIDGET(window->bin));
    }
    else {
        /* withdraw() does not make sense for a normal managed window; if you want to force a window to close, you should XKillClient the client */
        g_warning("lucca_window_withdraw is only implemented for internal windows\n");
    }
}

void lucca_window_close(LuccaWindow* window) {
    if (window->internal) {
        /* FIXME: For internal windows, this should emit an event on the corresponding GtkWindow */
    }
    else {
        if (window->supports_delete_window) {
            gint error;
            
            /* send a WM_DELETE_WINDOW message */
            XClientMessageEvent xevent;
            
            xevent.type = ClientMessage;
            xevent.window = window->xwindow;
            
            xevent.message_type = XInternAtom(window->display->xdisplay, "WM_PROTOCOLS", False);
            xevent.format = 32;
            xevent.data.l[0] = XInternAtom(window->display->xdisplay, "WM_DELETE_WINDOW", False);
            xevent.data.l[1] = gdk_x11_display_get_user_time(window->display->display);
            
            gdk_error_trap_push();
            
            XSendEvent(window->display->xdisplay, window->xwindow, False, 0, (XEvent*)&xevent);
            
            gdk_flush();
            
            error = gdk_error_trap_pop();
            if (error == BadWindow) {
                window->badwindow = TRUE;
            }
            else if (error != 0) {
                g_error("Unexpected X error: %i\n", error);
            }
        }
        else {
            /* The ICCCM says we can XKillClient the client in this case */
            gint error;
            
            gdk_error_trap_push();
            
            XKillClient(window->display->xdisplay, window->xwindow);
            
            gdk_flush();
            
            error = gdk_error_trap_pop();
            if (error == BadValue) {
                window->badwindow = TRUE;
            }
            else if (error != 0) {
                g_error("Unexpected X error: %i\n", error);
            }
        }
    }
}

void lucca_window_send_configure(LuccaWindow* window) {
    XConfigureEvent event;
    gint error;
    
    event.type = ConfigureNotify;
    event.send_event = True;
    event.display = window->display->xdisplay;
    event.event = window->xwindow;
    event.window = window->xwindow;
    
    event.x = window->xattr.x; /* FIXME: these should be the coordinates of the window, translated to be relative to the root */
    event.y = window->xattr.y;
    event.width = window->xattr.width;
    event.height = window->xattr.height;
    event.border_width = 0;
    event.above = None;
    event.override_redirect = False;
    
    gdk_error_trap_push();
    
    XSendEvent(window->display->xdisplay, window->xwindow, False, 0, (XEvent*)&event);
    
    gdk_flush();
    
    error = gdk_error_trap_pop();
    if (error == BadWindow) {
        window->badwindow = TRUE;
    }
    else if (error != 0) {
        g_error("Unexpected X error: %i\n", error);
    }
}

void lucca_window_give_focus(LuccaWindow* window) {
    if (!window->internal && !window->badwindow && window->xwmhints->flags&InputHint && window->xwmhints->input == True) {
        guint error;
    
        gdk_error_trap_push();
        
        XSetInputFocus(window->display->xdisplay, window->xwindow, RevertToPointerRoot, gdk_x11_display_get_user_time(window->display->display));
        
        gdk_flush();
        
        error = gdk_error_trap_pop();
        if (error == BadWindow) {
            window->badwindow = TRUE;
        }
        else if (error == BadMatch) {
            /* the window is not currently viewable and therefore cannot be focused; silently ignore the error */
        }
        else if (error != 0) {
            g_error("Unexpected X error: %i\n", error);
        }
    }
}

void lucca_window_property_notify(LuccaWindow* window, XPropertyEvent* event) {
    char* name = XGetAtomName(window->display->xdisplay, event->atom);
    
    if (event->state == PropertyNewValue) {
        g_datalist_set_data(&window->properties_set, name, GINT_TO_POINTER(1));
    }
    else if (event->state == PropertyDelete) {
        g_datalist_remove_data(&window->properties_set, name);
    }
    
    if (strcmp(name, "_NET_WM_NAME") == 0 || strcmp(name, "WM_NAME") == 0) {
        gchar* newname = _get_requested_title(window);
        
        if (strcmp(newname, window->requested_title) != 0) {
            g_free(window->requested_title);
            window->requested_title = newname;
            g_object_notify(G_OBJECT(window), "requested-title");
        }
        else {
            g_free(newname);
        }
    }
    else if (strcmp(name, "WM_PROTOCOLS") == 0) {
        window->supports_delete_window = FALSE;
        _update_protocols(window);
    }
    else if (strcmp(name, "WM_HINTS") == 0) {
        XFree(window->xwmhints);
        window->xwmhints = _get_wm_hints(window);
    }
    else if (strcmp(name, "WM_NORMAL_HINTS") == 0) {
        _update_normal_hints(window);
    }
    else if (strcmp(name, "_NET_WM_WINDOW_TYPE") == 0 || strcmp(name, "WM_TRANSIENT_FOR") == 0) {
        _update_type(window);
    }
    else if (strcmp(name, "_NET_WM_STRUT") == 0 || strcmp(name, "_NET_WM_STRUT_PARTIAL") == 0) {
        _update_struts(window);
        lucca_screen_recalculate_docks(window->display->default_screen); /* FIXME: this will be a problem for multiple screens */
    }
    
    XFree(name);
}

GList* lucca_window_transient_for(LuccaWindow* window) {
    Window* data=NULL;
    GdkWindow* gdkparent;
    LuccaWindow* luccaparent;
    int status;
    Atom prop;
    Atom type;
    Atom xactual_type;
    int actual_format;
    unsigned long nitems;
    unsigned long bytes_after;
    guint error;
    GList* result=NULL;
    
    if (window->badwindow || !g_datalist_get_data(&window->properties_set, "WM_TRANSIENT_FOR")) {
        return NULL;
    }
    
    prop = XInternAtom(window->display->xdisplay, "WM_TRANSIENT_FOR", False);
    type = XInternAtom(window->display->xdisplay, "WINDOW", False);
    
    gdk_error_trap_push();

    status = XGetWindowProperty(
        window->display->xdisplay, /* display */
        window->xwindow, /* window */
        prop, /* property */
        0, /* long_offset */
        1, /* long_length */
        False, /* delete */
        type, /* req_type */
        &xactual_type, /* actual_type_return */
        &actual_format, /* actual_format_return */
        &nitems, /* nitems_return */
        &bytes_after, /* bytes_after_return */
        (unsigned char**)&data /* prop_return */
        );
    
    gdk_flush();
    
    error = gdk_error_trap_pop();
    if (error == BadWindow) {
        window->badwindow = TRUE;
    }
    else if (error != 0) {
        g_error("Unexpected X error: %i\n", error);
    }
    
    if (status == Success && data != NULL && nitems == 1) {
        gdkparent = gdk_xid_table_lookup_for_display(window->display->display, data[0]);
        luccaparent = g_hash_table_lookup(window->display->valid_windows, gdkparent);
        /* FIXME: EWMH says that if the parent is None or Root, we should treat this as transient for all windows sharing a group. */
        
        if (luccaparent != NULL) {
            result = g_list_prepend(result, luccaparent);
        }
    }
    
    if (data != NULL)
        XFree(data);
    
    return result;
}

#define _NET_WM_STATE_REMOVE 0
#define _NET_WM_STATE_ADD 1
#define _NET_WM_STATE_TOGGLE 2
void lucca_window_netwmstate_request(LuccaWindow* window, XClientMessageEvent* event) {
    LuccaState message_bits=0;
    LuccaState requested_states=0;
    int i;
    
    /* event->data.l[0] is the command; l[1] and l[2] are state atoms; l[3] is 1 if the event is from a normal application, 2 if it's from a pager */
    
    /* translate l[1] and l[2] into a bit field */
    for (i=1; i<=2; i++) {
        if (event->data.l[i] != 0) {
            char* state_name = XGetAtomName(window->display->xdisplay, (Atom)event->data.l[i]);
            
            message_bits |= GPOINTER_TO_INT(g_datalist_get_data(&state_atom_constants, state_name));
            
            XFree(state_name);
        }
    }
    
    /* calculate the new "requested" state */
    switch (event->data.l[0]) {
        case _NET_WM_STATE_REMOVE:
            requested_states = window->netwm_states & ~message_bits;
            break;
        case _NET_WM_STATE_ADD:
            requested_states = window->netwm_states | message_bits;
            break;
        case _NET_WM_STATE_TOGGLE:
            /* FIXME: It's possible for _NET_WM_STATE to change between when the client sends the message and the window manager processes it; this could cause the message to be misinterpreted */
            requested_states = window->netwm_states ^ message_bits;
            break;
        default:
            /* invalid message; ignore it */
            return;
    }
    
    window->requested_states = requested_states;
    
    g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->state_request_signal_id, 0, (gint)requested_states);
}

void lucca_window_set_states(LuccaWindow* window, LuccaState states) {
    gulong* state_list = NULL;
    LuccaState temp_states;
    guint state_count = 0;
    gint i, j;
    
    if (states == window->netwm_states)
        return;
    
    if (!window->badwindow) {
        guint error;
        
        /* count the states that are set */
        temp_states = states;
        while (temp_states != 0) {
            state_count += temp_states&1;
            temp_states >>= 1;
        }
        
        /* allocate memory for a list of states */
        state_list = g_malloc(sizeof(gulong) * state_count);
        
        /* add each state to the list */
        j=0; i=1;
        while (j < state_count) {
            if (states & i) {
                state_list[j] = (gulong)gdk_atom_intern(g_hash_table_lookup(state_constant_atoms, GINT_TO_POINTER(i)), FALSE);
                
                j++;
            }
            
            i <<= 1;
        }
        
        gdk_error_trap_push();
        
        /* set the _NET_WM_STATE property */
        gdk_property_change(window->window, /* window */
                            netwm_state, /* property */
                            gdkatom_atom, /* type */
                            32, /* format */
                            GDK_PROP_MODE_REPLACE, /* mode */
                            (guchar*)state_list, /* data */
                            state_count /* nelements */
                            );
        
        gdk_flush();
        error = gdk_error_trap_pop();
        if (error == BadWindow) {
            window->badwindow = TRUE;
        }
        else if (error != 0) {
            g_error("Unexpected X error: %i\n", error);
        }
    }
    
    window->netwm_states = states;
    
    g_free(state_list);
}

LuccaState lucca_window_get_states(LuccaWindow* window) {
    return window->netwm_states;
}

