/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-display-private.h"

/* domain for this module's errors */
GQuark lucca_display_error_quark() {
    static GQuark error_quark=0;
    if (error_quark == 0) {
        error_quark = g_quark_from_string("lucca-display-error-quark");
    }
    return error_quark;
}

/* Registration of LuccaType */
/* FIXME: this should either go somewhere else or be moved out of lucca-common.h */
GType lucca_type_get_type() {
    static GType type = 0;
    static const GEnumValue values[] = {
        {LUCCA_TYPE_NORMAL, "LUCCA_TYPE_NORMAL", "NORMAL"},
        {LUCCA_TYPE_DESKTOP, "LUCCA_TYPE_DESKTOP", "DESKTOP"},
        {LUCCA_TYPE_DOCK, "LUCCA_TYPE_DOCK", "DOCK"},
        {LUCCA_TYPE_TOOLBAR, "LUCCA_TYPE_TOOLBAR", "TOOLBAR"},
        {LUCCA_TYPE_MENU, "LUCCA_TYPE_MENU", "MENU"},
        {LUCCA_TYPE_UTILITY, "LUCCA_TYPE_UTILITY", "UTILITY"},
        {LUCCA_TYPE_SPLASH, "LUCCA_TYPE_SPLASH", "SPLASH"},
        {LUCCA_TYPE_DIALOG, "LUCCA_TYPE_DIALOG", "DIALOG"},
        {LUCCA_TYPE_DROPDOWN_MENU, "LUCCA_TYPE_DROPDOWN_MENU", "DROPDOWN_MENU"},
        {LUCCA_TYPE_POPUP_MENU, "LUCCA_TYPE_POPUP_MENU", "POPUP_MENU"},
        {LUCCA_TYPE_TOOLTIP, "LUCCA_TYPE_TOOLTIP", "TOOLTIP"},
        {LUCCA_TYPE_NOTIFICATION, "LUCCA_TYPE_NOTIFICATION", "NOTIFICATION"},
        {LUCCA_TYPE_COMBO, "LUCCA_TYPE_COMBO", "COMBO"},
        {LUCCA_TYPE_DND, "LUCCA_TYPE_DND", "DND"},
        {LUCCA_TYPE_INTERNAL, "LUCCA_TYPE_INTERNAL", "INTERNAL"}
    };
    if (type == 0) {
        type = g_enum_register_static("LuccaType", values);
    }
    return type;
}

/* Implementation of LuccaDisplay */

static GObjectClass* parent_class = NULL;

/* property id's */
enum {
    PROP_VALID=1,
};

static void _luccawindowdestroy(gpointer data) {
    LUCCA_WINDOW(data)->valid = FALSE;
    g_object_unref(data);
}

static void lucca_display_init(GTypeInstance* instance, gpointer g_class) {
    LuccaDisplay* display = LUCCA_DISPLAY(instance);

    display->valid = FALSE;
    
    display->valid_windows = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, _luccawindowdestroy);
    
    display->hotkey_infos = NULL;
    
    display->wm_name = NULL;
    
    display->dispose_has_run = FALSE;
    
    display->display = NULL;
    display->xdisplay = NULL;
    display->default_screen = NULL;
    
    display->ignore_crossing = FALSE;
}

/* lucca_display_invalidate: remove outstanding references and make valid FALSE without making the display unusable */
static void lucca_display_invalidate(LuccaDisplay* display) {
    if (display->valid) {
        display->valid = FALSE;
        
        g_object_unref(display->display);
        display->display = NULL;
        
        display->xdisplay = NULL;
        
        display->default_screen->valid = FALSE;
        g_object_unref(display->default_screen);
        display->default_screen = NULL;
        
        g_hash_table_remove_all(display->valid_windows);
    }
}

static void lucca_display_dispose(GObject* obj) {
    LuccaDisplay* display = LUCCA_DISPLAY(obj);
    GSList* item;
    
    if (display->dispose_has_run) {
        return;
    }
    display->dispose_has_run = TRUE;
    
    lucca_display_invalidate(display);
    
    for (item = display->hotkey_infos; item!=NULL; item=item->next) {
        g_slice_free(HotkeyInfo, item->data);
    }
    g_slist_free(display->hotkey_infos);
    
    g_free(display->wm_name);
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_display_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaDisplay* display = LUCCA_DISPLAY(object);
    
    switch (property_id) {
    case PROP_VALID:
        g_value_set_boolean(value, display->valid);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_display_class_init(gpointer g_class, gpointer class_data) {
    LuccaDisplayClass* klass = (LuccaDisplayClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_display_dispose;
    gobclass->get_property = lucca_display_get_property;
    
    /* properties */
    pspec = g_param_spec_boolean(
        "valid",
        "valid",
        "TRUE if this manages a display's windows",
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_VALID, pspec);
    
    /* signals */

    klass->new_screen_signal_id = g_signal_new(
        "new-screen",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__OBJECT,
        G_TYPE_NONE,
        1, /* n_params */
        LUCCA_TYPE_SCREEN
    );

    klass->new_window_signal_id = g_signal_new(
        "new-window",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__OBJECT,
        G_TYPE_NONE,
        1, /* n_params */
        LUCCA_TYPE_WINDOW
    );

    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_display_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaDisplayClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_display_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaDisplay),
            0, /* n_preallocs */
            lucca_display_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaDisplayType", &info, 0);
    }
    return type;
}

/* Method definitions */

void lucca_display_connect(LuccaDisplay* display, GdkDisplay* gdk_display, const gchar* name, gboolean replace, GError** error) {
    GError* err=NULL;

    /* make sure we're not already connected */
    g_assert(!display->valid);
    
    display->display = gdk_display;
    g_object_ref(gdk_display);
    display->xdisplay = GDK_DISPLAY_XDISPLAY(gdk_display);
    
    g_free(display->wm_name);
    display->wm_name = g_strdup(name);
    
    /* intern atoms */
    display->xatom_atom = XInternAtom(display->xdisplay, "ATOM", False);
    display->xcardinal_atom = XInternAtom(display->xdisplay, "CARDINAL", False);

    /* manage the default screen */
    display->default_screen = g_object_new(LUCCA_TYPE_SCREEN, NULL);
    lucca_screen_initialize(display->default_screen, gdk_display_get_default_screen(gdk_display), display, replace, &err);
    if (err != NULL) {
        g_object_unref(display->default_screen);
        display->default_screen = NULL;
        g_propagate_error(error, err);
        return;
    }
}

void _disconnect_window(gpointer key, gpointer value, gpointer user_data) {
    LuccaWindow* window = LUCCA_WINDOW(value);
    GdkAtom wmstate;
    gint error;
    
    /* FIXME: we can't use lucca_window_withdraw_finish here because it modifies display->valid_windows */
    gdk_error_trap_push();
    
    /* XRemoveFromSaveSet(window->display->xdisplay, window->xwindow); */

    wmstate = gdk_atom_intern("WM_STATE", FALSE);
    
    gdk_property_delete(window->window, wmstate);
    
    if (window->window_parent != window->display->default_screen->root) {
        gint x, y;
        
        gdk_window_get_root_origin(window->window, &x, &y);
        
        XReparentWindow(window->display->xdisplay, window->xwindow, window->display->default_screen->xroot, x, y);
    }
    
    /* FIXME: remove our event selections for this window? */
    
    gdk_flush();
    
    error = gdk_error_trap_pop();
    if (error == BadWindow) {
        window->badwindow = TRUE;
    }
    else if (error != 0) {
        g_error("Unexpected X error: %i\n", error);
    }
    
    window->badwindow = TRUE;
    lucca_window_change_owner(window, NULL);

    g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->withdraw_signal_id, 0);
}

void lucca_display_disconnect(LuccaDisplay* display) {
    /* remove the WM_STATE property and reparent all managed windows to the root window */
    g_hash_table_foreach(display->valid_windows, _disconnect_window, display);
    
    lucca_screen_disconnect(display->default_screen);
    
    lucca_display_invalidate(display);
}

LuccaScreen* lucca_display_get_default_screen(LuccaDisplay* display) {
    g_assert(display->valid == TRUE);    
    
    return display->default_screen;
}

static void _add_to_glist(gpointer key, gpointer value, gpointer data) {
    GList** list = (GList**)data;
    
    *list = g_list_prepend(*list, value);
}
GList* lucca_display_get_windows(LuccaDisplay* display) {
    GList* result=NULL;
    
    g_hash_table_foreach(display->valid_windows, _add_to_glist, &result);
    
    return result;
}

LuccaWindow* lucca_display_create_internal(LuccaDisplay* display) {
    LuccaBin* bin;
    LuccaWindow* result;

    bin = g_object_new(LUCCA_TYPE_BIN, NULL);

    lucca_bin_set_parent(bin, display->default_screen->root);

    gtk_widget_realize(GTK_WIDGET(bin));

    result = lucca_window_new_internal(bin, display->default_screen);

    return result;
}

void lucca_display_add_hotkey(LuccaDisplay* display, guint keyval, GdkModifierType modifiers, GClosure* callback) {
    HotkeyInfo* info;
    
    g_assert(callback != NULL);
    
    g_closure_ref(callback);
    g_closure_sink(callback);
    
    info = g_slice_new(HotkeyInfo);
    
    info->display = display;
    info->keyval = keyval;
    info->modifiers = modifiers;
    info->callback = callback;
    
    display->hotkey_infos = g_slist_prepend(display->hotkey_infos, info);
    
    if (display->valid)
        lucca_screen_add_hotkey(display->default_screen, info);
}

void lucca_display_add_x_events(LuccaDisplay* display, GdkWindow* window, long event_mask) {
    XWindowAttributes result;
    
    XGetWindowAttributes(display->xdisplay, GDK_WINDOW_XWINDOW(window), &result);
    
    XSelectInput(display->xdisplay, GDK_WINDOW_XWINDOW(window), result.your_event_mask|event_mask);
}

