/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <unistd.h>
#include <signal.h>

#include <stdlib.h>

#include "lucca-display.h"

GSList* refs=NULL; /* a list of objects that we hold a reference to; this lets us unref and hopefully free everything when the tests end */
LuccaDisplay* display;

void on_titlechange(LuccaWindow* window) {
    g_print("newtitle %p\n", window);
}

void on_withdraw(LuccaWindow* window) {
    g_print("withdraw %p\n", window);
}

void on_staterequest(LuccaWindow* window, LuccaState states) {
    g_print("state-request %p %i\n", window, states);
}

void on_newscreen(LuccaDisplay* display, LuccaScreen* screen) {
    g_print("new-screen %p\n", screen);
    g_object_ref(screen);
    refs = g_slist_prepend(refs, screen);
}

void on_newwindow(LuccaDisplay* display, LuccaWindow* window) {
    g_print("new-window %p\n", window);
    g_object_ref(window);
    refs = g_slist_prepend(refs, window);
    
    g_signal_connect(window, "withdraw", G_CALLBACK(on_withdraw), NULL);
    g_signal_connect(window, "notify::requested-title", G_CALLBACK(on_titlechange), NULL);
    g_signal_connect(window, "state-request", G_CALLBACK(on_staterequest), NULL);
}

void segfault_handler(int signum) {
    signal(SIGSEGV, SIG_DFL);

    g_print("segfault\n");
    
    sleep(20);
}

GIOChannel* g_stdin;
static gchar* input_buffer;

gboolean on_input(GIOChannel* source, GIOCondition condition, gpointer data) {
    gchar read_buffer[256];
    gsize length;
    
    /*g_io_channel_read_chars(g_stdin, read_buffer, 256, &length, NULL);*/
    length = read(0, read_buffer, 255); /* FIXME: this should be done using the io channel */
    read_buffer[length] = '\0';
    gchar* new_input_buffer = g_strconcat(input_buffer, read_buffer, NULL);
    gchar** split_result;
    g_free(input_buffer);
    input_buffer = new_input_buffer;
    
    while (input_buffer[0] != '\0' && (split_result=g_strsplit(input_buffer, "\n", 2))) {
        gchar** input_words;
        if (split_result[1] == NULL) {
            g_strfreev(split_result);
            break;
        }
        
        input_words = g_strsplit(split_result[0], " ", 0);

        if ((input_words[0] == NULL) || (g_ascii_strcasecmp(input_words[0], "quit") == 0)) {
            gtk_main_quit();
            return FALSE;
        }
        else if (g_ascii_strcasecmp(input_words[0], "getb") == 0) {
            gboolean bvalue;
            GObject* obj;
            obj = G_OBJECT(strtol(input_words[1]+2, NULL, 16));
            g_object_get(obj, input_words[2], &bvalue, NULL);
            g_print("%i\n", bvalue);
        }
        else if (g_ascii_strcasecmp(input_words[0], "getstr") == 0) {
            gchar* strvalue;
            GObject* obj;
            obj = G_OBJECT(strtol(input_words[1]+2, NULL, 16));
            g_object_get(obj, input_words[2], &strvalue, NULL);
            g_print("%s\n", strvalue);
            g_free(strvalue);
        }
        else if (g_ascii_strcasecmp(input_words[0], "getui") == 0) {
            guint uivalue;
            GObject* obj;
            obj = G_OBJECT(strtol(input_words[1]+2, NULL, 16));
            g_object_get(obj, input_words[2], &uivalue, NULL);
            g_print("%u\n", uivalue);
        }
        else if (g_ascii_strcasecmp(input_words[0], "gettype") == 0) {
            LuccaType typevalue;
            LuccaWindow* window;
            GEnumValue* enumvalue;
            window = LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16));
            g_object_get(window, "type", &typevalue, NULL);
            enumvalue = g_enum_get_value(g_type_class_peek(LUCCA_TYPE_TYPE), typevalue);
            g_print("%s\n", enumvalue->value_name);
        }
        else if (g_ascii_strcasecmp(input_words[0], "connect") == 0) {
            GError* err=NULL;
            lucca_display_connect(display, gdk_display_get_default(), "test wm", strtol(input_words[1], NULL, 10), &err);
            if (err != NULL) {
                g_printerr("error: %s\n", err->message);
                g_error_free(err);
            }
        }
        else if (g_ascii_strcasecmp(input_words[0], "disconnect") == 0) {
            lucca_display_disconnect(display);
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_default_screen") == 0) {
            g_print("%p\n", lucca_display_get_default_screen(display));
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_windows") == 0) {
            GList* list;
            GList* item;
            list = lucca_display_get_windows(display);
            for (item=list; item!=NULL; item=item->next) {
                g_print("%p ", item->data);
            }
            g_print("\n");
            g_list_free(list);
        }
        else if (g_ascii_strcasecmp(input_words[0], "screen_get_display") == 0) {
            g_print("%p\n", lucca_screen_get_display(LUCCA_SCREEN(strtol(input_words[1]+2, NULL, 16))));
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_root") == 0) {
            g_print("%p\n", lucca_screen_get_root(LUCCA_SCREEN(strtol(input_words[1]+2, NULL, 16))));
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_geometry_hints") == 0) {
            GdkGeometry geometry;
            GdkWindowHints geom_mask;
            lucca_window_get_geometry_hints(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16)), &geometry, &geom_mask);
            if (geom_mask & GDK_HINT_MIN_SIZE) {
                g_print("min_size %i %i\n", geometry.min_width, geometry.min_height);
            }
            g_print("end_geometry\n");
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_requested_states") == 0) {
            g_print("%i\n", lucca_window_get_requested_states(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16))));
        }
        else if (g_ascii_strcasecmp(input_words[0], "set_states") == 0) {
            lucca_window_set_states(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16)), (LuccaState)strtol(input_words[2], NULL, 10));
        }
        else if (g_ascii_strcasecmp(input_words[0], "get_states") == 0) {
            g_print("%i\n", lucca_window_get_states(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16))));
        }
        else if (g_ascii_strcasecmp(input_words[0], "configure") == 0) {
            lucca_window_configure(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16)), GDK_WINDOW(strtol(input_words[2]+2, NULL, 16)), strtol(input_words[3], NULL, 10), strtol(input_words[4], NULL, 10), strtol(input_words[5], NULL, 10), strtol(input_words[6], NULL, 10), strtol(input_words[7], NULL, 10));
        }
        else if (g_ascii_strcasecmp(input_words[0], "close") == 0) {
            lucca_window_close(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16)));
        }
        else if (g_ascii_strcasecmp(input_words[0], "transient_for") == 0) {
            GList* result;
            GList* item;
            result = lucca_window_transient_for(LUCCA_WINDOW(strtol(input_words[1]+2, NULL, 16)));
            for (item=result; item!=NULL; item=item->next) {
                g_print("%p ", item->data);
            }
            g_print("\n");
            g_list_free(result);
        }
        else {
            g_print("no such command\n");
        }

        g_strfreev(input_words);
        g_free(input_buffer);
        input_buffer = split_result[1];
        g_free(split_result[0]);
        g_free(split_result);
    }
    return TRUE;
}

gboolean on_hup(GIOChannel* source, GIOCondition condition, gpointer data) {
    gtk_main_quit();
    return FALSE;
}

int main(int argc, char** argv) {
    GSList* listi;
    gtk_init(&argc, &argv);
    
    signal(SIGSEGV, segfault_handler);
    
    g_stdin = g_io_channel_unix_new(0);
    
    input_buffer = g_strdup("");
    
    g_io_add_watch(g_stdin, G_IO_IN, on_input, NULL);
    
    g_io_add_watch(g_stdin, G_IO_ERR|G_IO_HUP|G_IO_NVAL, on_hup, NULL);
    
    display = g_object_new(LUCCA_TYPE_DISPLAY, NULL);
    refs = g_slist_prepend(refs, display);
    
    g_signal_connect(display, "new-screen", G_CALLBACK(on_newscreen), NULL);
    g_signal_connect(display, "new-window", G_CALLBACK(on_newwindow), NULL);
    
    g_print("%p\n", display);
    
    gtk_main();

    for (listi=refs; listi != NULL; listi=listi->next) {
        g_object_unref(G_OBJECT(listi->data));
    }
    g_slist_free(refs);
    
    return 0;
}

