
import subprocess
import sys
import thread
import traceback
import signal
import os
import time

import gtk
import gobject

LUCCA_STATE_HIDDEN = '128'

DEBUG = len(sys.argv) >= 5 and sys.argv[4] == '-debug'

def alarm_signal(signal, stackframe):
    raise Exception("timeout")
signal.signal(signal.SIGALRM, alarm_signal)

def nongtk_thread():
    global proc
    proc = subprocess.Popen(sys.argv[1], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    logfile = open(sys.argv[3], 'w')

    tests_failed = False

    def send(line):
        logfile.write('> %s\n' % line)
        proc.stdin.write('%s\n' % line)

    def getline():
        signal.alarm(5)
        line = proc.stdout.readline().rstrip('\n')
        if line == 'segfault' and DEBUG:
            os.system("cat %s" % (sys.argv[3]))
            os.system("gdb %s %s" % (sys.argv[1], proc.pid))
        signal.alarm(0)
        logfile.write('< %s\n' % line)
        return line.split(' ')

    try:
        line = getline()
        display = line[0]
        
        #create an "existing" window that xwm should map once it's connected to the display
        def create_window():
            global gtkwindow1
            gtkwindow1 = gtk.Window()
            gtkwindow1.set_title("test_window_1")
            gtkwindow1.set_geometry_hints(None, min_width=10, min_height=11)
            gtkwindow1.show()
        do_in_gtk_thread(create_window)
        
        send('getb %s valid' % display)
        line = getline()
        assert(line[0] == '0')
        
        send('connect 0')
        line = getline()
        assert(line[0] == 'new-screen')
        screen = line[1]
        line = getline()
        assert(line[0] == 'new-window')
        luccawindow1 = line[1]
        
        send('getb %s valid' % display)
        line = getline()
        assert(line[0] == '1')
        
        send('get_default_screen')
        line = getline()
        assert(line[0] == screen)
        
        send('getb %s valid' % screen)
        line = getline()
        assert(line[0] == '1')
        
        send('screen_get_display %s' % screen)
        line = getline()
        assert(line[0] == display)
        
        send('getui %s width' % screen)
        line = getline()
        screen_width = int(line[0])
        send('getui %s height' % screen)
        line = getline()
        screen_height = int(line[0])

        send('getui %s workarea-x' % screen)
        line = getline()
        assert(line[0] == '0')
        send('getui %s workarea-y' % screen)
        line = getline()
        assert(line[0] == '0')
        send('getui %s workarea-width' % screen)
        line = getline()
        assert(int(line[0]) == screen_width)
        send('getui %s workarea-height' % screen)
        line = getline()
        assert(int(line[0]) == screen_height)
        
        send('getstr %s requested-title' % luccawindow1)
        line = getline()
        assert(line[0] == "test_window_1")
        
        send('gettype %s' % luccawindow1)
        line = getline()
        assert(line[0] == "LUCCA_TYPE_NORMAL")
        
        send('get_geometry_hints %s' % luccawindow1)
        line = getline()
        assert(line[0] == "min_size")
        assert(line[1] == "10")
        assert(line[2] == "11")
        line = getline()
        assert(line[0] == "end_geometry")
        
        send('get_requested_states %s' % luccawindow1)
        line = getline()
        assert(line[0] == '0')
        
        #configure the existing (already mapped) window
        send('get_root %s' % screen)
        line = getline()
        root = line[0]
        
        configure_mutex = thread.allocate_lock()
        configure_mutex.acquire()
        def set_configure_event():
            def timeout():
                logfile.write("ERROR: timeout waiting for configure event\n")
                tests_failed = True
                thread.interrupt_main()
                configure_mutex.release()
            
            source = gobject.timeout_add(5000, timeout)

            def on_configure(widget, event):
                frame = gtkwindow1.window.get_frame_extents()
                logfile.write(":window configured with xywh %s %s %s %s\n" % (frame.x, frame.y, frame.width, frame.height))
                
                if not (frame.x == 12 and frame.y == 13 and frame.width == 32 and frame.height == 33):
                    tests_failed = True
                    thread.interrupt_main()
            
                gobject.source_remove(source)
                configure_mutex.release()
            
            gtkwindow1.connect('configure-event', on_configure)

        do_in_gtk_thread(set_configure_event)
        
        send('configure %s %s %s %s %s %s %s' % (luccawindow1, root, 12, 13, 32, 33, 1))
        configure_mutex.acquire()

        #ask the wm to make the window fullscreen
        def fullscreen_window():
            gtkwindow1.fullscreen()
        do_in_gtk_thread(fullscreen_window)
        
        line = getline()
        assert(line == ['state-request', luccawindow1, '256'])
        
        send('get_requested_states %s' % luccawindow1)
        line = getline()
        assert(line[0] == '256')
        
        send('get_states %s' % luccawindow1)
        line = getline()
        assert(line[0] == '0')

        fullscreen_mutex = thread.allocate_lock()
        fullscreen_mutex.acquire()
        def set_state_event():
            def timeout():
                logfile.write("ERROR: timeout waiting for state change event\n")
                tests_failed = True
                thread.interrupt_main()
                fullscreen_mutex.release()
            
            source = gobject.timeout_add(5000, timeout)

            def on_fullscreen(widget, event):
                logfile.write(":got state change event\n")
                
                gobject.source_remove(source)
                fullscreen_mutex.release()
            
            gtkwindow1.connect('window-state-event', on_fullscreen)
        
        do_in_gtk_thread(set_state_event)

        send('set_states %s 256' % luccawindow1)
        fullscreen_mutex.acquire()
        
        send('get_states %s' % luccawindow1)
        line = getline()
        assert(line[0] == '256')

        #withdraw the existing window
        def withdraw_window():
            gtkwindow1.hide()
        do_in_gtk_thread(withdraw_window)
        
        line = getline()
        assert(line[0] == 'withdraw')
        assert(line[1] == luccawindow1)
        
        send('getb %s valid' % luccawindow1)
        line = getline()
        assert(line[0] == '0')

        #reuse the existing window
        def reshow_window():
            gtkwindow1.show()
        do_in_gtk_thread(reshow_window)
        
        line = getline()
        assert(line[0] == 'new-window')
        luccawindow1 = line[1]

        #close the existing window
        close_mutex = thread.allocate_lock()
        close_mutex.acquire()
        def set_close_event():
            def timeout():
                logfile.write("ERROR: timeout waiting for close event\n")
                tests_failed = True
                thread.interrupt_main()
                close_mutex.release()
            
            source = gobject.timeout_add(5000, timeout)

            def on_close(widget, event):
                logfile.write(":window closed\n")
            
                gobject.source_remove(source)
                close_mutex.release()
                
                return False
            
            gtkwindow1.connect('delete-event', on_close)

        do_in_gtk_thread(set_close_event)
        
        send('close %s' % (luccawindow1))
        close_mutex.acquire()
        
        line = getline()
        assert(line[0] == 'withdraw')
        assert(line[1] == luccawindow1)
        
        #create a new window that xwm should recognize wants to be mapped
        def create_window():
            global gtkwindow2
            gtkwindow2 = gtk.Window()
            gtkwindow2.iconify()
            gtkwindow2.set_title("test_window_2")
            gtkwindow2.show()
        do_in_gtk_thread(create_window)
        
        line = getline()
        assert(line[0] == 'new-window')
        luccawindow2 = line[1]
        
        send('getstr %s requested-title' % luccawindow2)
        line = getline()
        assert(line[0] == "test_window_2")
        
        send('get_requested_states %s' % luccawindow2)
        line = getline()
        assert(line[0] == LUCCA_STATE_HIDDEN)
                
        #map the new window
        map_mutex = thread.allocate_lock()
        map_mutex.acquire()
        def set_map_event():
            def timeout():
                logfile.write("ERROR: timeout waiting for map event\n")
                tests_failed = True
                thread.interrupt_main()
                map_mutex.release()
            
            source = gobject.timeout_add(5000, timeout)

            def on_map(widget, event):
                frame = gtkwindow2.window.get_frame_extents()
                logfile.write(":window mapped with xywh %s %s %s %s\n" % (frame.x, frame.y, frame.width, frame.height))
                
                if not (frame.x == 10 and frame.y == 11 and frame.width == 30 and frame.height == 31):
                    tests_failed = True
                    thread.interrupt_main()
            
                gobject.source_remove(source)
                map_mutex.release()
            
            gtkwindow2.connect('map-event', on_map)

        do_in_gtk_thread(set_map_event)
        
        send('configure %s %s %s %s %s %s %s' % (luccawindow2, root, 10, 11, 30, 31, 1))
        map_mutex.acquire()
        
        send('transient_for %s' % (luccawindow2))
        line = getline()
        assert(len(line) == 1)
        
        #unmap the new window
        unmap_mutex = thread.allocate_lock()
        unmap_mutex.acquire()
        def set_unmap_event():
            def timeout():
                logfile.write("ERROR: timeout waiting for unmap event\n")
                tests_failed = True
                thread.interrupt_main()
                unmap_mutex.release()
            
            source = gobject.timeout_add(5000, timeout)

            def on_unmap(widget, event):
                frame = gtkwindow2.window.get_frame_extents()
                logfile.write(":window unmapped\n")
            
                gobject.source_remove(source)
                unmap_mutex.release()
            
            gtkwindow2.connect('unmap-event', on_unmap)

        do_in_gtk_thread(set_unmap_event)
        
        send('configure %s %s %s %s %s %s %s' % (luccawindow2, root, 10, 11, 30, 31, 0))
        unmap_mutex.acquire()
        
        #map the new window again
        map_mutex = thread.allocate_lock()
        map_mutex.acquire()
        do_in_gtk_thread(set_map_event)
        send('configure %s %s %s %s %s %s %s' % (luccawindow2, root, 10, 11, 30, 31, 1))
        map_mutex.acquire()
        
        #change the window's title
        def change_title():
            gtkwindow2.set_title("new-title")
        do_in_gtk_thread(change_title)
        
        line = getline()
        assert(line[0] == "newtitle")
        assert(line[1] == luccawindow2)
        
        send('getstr %s requested-title' % luccawindow2)
        line = getline()
        assert(line[0] == "new-title")
        
        #create a transient for this window
        def create_transient():
            global gtktransient
            gtktransient = gtk.Window()
            gtktransient.set_transient_for(gtkwindow2)
            gtktransient.show()
        do_in_gtk_thread(create_transient)
        
        line = getline()
        assert(line[0] == 'new-window')
        luccatransient = line[1]
        
        send('transient_for %s' % luccatransient)
        line = getline()
        assert(line[0] == luccawindow2)
        assert(len(line) == 2)
        
        #create an xclock window, which doesn't set WM_NAME
        xclock = subprocess.Popen("xclock")
        
        try:
            line = getline()
            assert(line[0] == 'new-window')
            xclockwindow = line[1]
            
            send('getstr %s requested-title' % xclockwindow)
            line = getline()
            assert(line[0] == "xclock")
            
            send('get_windows')
            line = getline()
            assert(set(line) == set([luccawindow2, luccatransient, xclockwindow, '']))
        finally:
            os.kill(xclock.pid, signal.SIGTERM)
        
        line = getline()
        assert(line[0] == 'withdraw')
        assert(line[1] == xclockwindow)
        
        send('disconnect')

        withdrawn_windows = set()
        expected_withdrawn_windows = set([luccawindow2, luccatransient])
        
        line = getline()
        assert(line[0] == 'withdraw')
        withdrawn_windows.add(line[1])
                
        line = getline()
        assert(line[0] == 'withdraw')
        withdrawn_windows.add(line[1])
        
        assert(withdrawn_windows == expected_withdrawn_windows)
                
        send('getb %s valid' % screen)
        line = getline()
        assert(line[0] == '0')
        
    except:
        tests_failed = True
        
        traceback.print_exc(None, logfile)

    
    proc.stdin.close()
    
    if tests_failed:
        os.kill(proc.pid, signal.SIGTERM)
    
    result, stdout, stderr = proc.wait(), proc.stdout.read(), proc.stderr.read()

    logfile.write("Test process return value: %s\n" % result)

    logfile.write("Stdout:\n%s" % stdout)
    logfile.write("Stderr:\n%s" % stderr)

    logfile.close()

    if result != 0 or stderr != '':
        tests_failed = True

    do_in_gtk_thread(gtk.main_quit)

    if tests_failed:
        print "Unit test failed; see %s for details" % sys.argv[3]
        sys.exit(1)
    else:
        f = open(sys.argv[2], 'w')
        f.close()


def do_in_gtk_thread(f, *args, **kwargs):
    lock = thread.allocate_lock()
    lock.acquire()
    def idle_func():
        f(*args, **kwargs)
        lock.release()
    gobject.idle_add(idle_func)
    lock.acquire()

def gtk_thread():
    gtk.gdk.threads_enter()
    gtk.main()
    gtk.gdk.threads_leave()

gtk.gdk.threads_init()

thread.start_new_thread(gtk_thread, ())

nongtk_thread()

