/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-display-private.h"

#include <gdk/gdkkeysyms.h>

/* Implementation of LuccaScreen */

static GObjectClass* parent_class = NULL;

/* property id's */
enum {
    PROP_VALID=1,
    PROP_WIDTH,
    PROP_HEIGHT,
    PROP_WORKAREA_X,
    PROP_WORKAREA_Y,
    PROP_WORKAREA_WIDTH,
    PROP_WORKAREA_HEIGHT,
};

static void lucca_screen_init(GTypeInstance* instance, gpointer g_class) {
    LuccaScreen* screen = LUCCA_SCREEN(instance);

    screen->valid = FALSE;
    screen->initialized = FALSE;
    
    screen->dispose_has_run = FALSE;
    
    screen->screen = NULL;
    screen->xscreen = NULL;
    screen->display = NULL;
    screen->root = NULL;
    screen->xroot = 0;
    
    screen->window = NULL;
    screen->xwindow = 0;
    
    screen->previous_wm = NULL;
    
    screen->docks = NULL;
    screen->grab_infos = NULL;
}

static void lucca_screen_dispose(GObject* obj) {
    LuccaScreen* screen = LUCCA_SCREEN(obj);
    GList* item;
    
    if (screen->dispose_has_run) {
        return;
    }
    screen->dispose_has_run = TRUE;
    
    if (screen->screen)
        g_object_unref(screen->screen);
    screen->screen = NULL;
    
    screen->xscreen = NULL;
    
    if (screen->display)
        g_object_unref(screen->display);
    screen->display = NULL;
    
    if (screen->root)
        g_object_unref(screen->root);
    screen->root = NULL;
    
    screen->xroot = 0;
    
    if (screen->window)
        g_object_unref(screen->window);
    screen->window = NULL;
    
    screen->xwindow = 0;
    
    if (screen->previous_wm)
        g_object_unref(screen->previous_wm);
    
    for (item=screen->docks; item!=NULL; item=item->next) {
        g_object_unref(item->data);
    }
    g_list_free(screen->docks);
    
    for (item=screen->grab_infos; item!=NULL; item=item->next) {
        g_slice_free(GrabInfo, item->data);
    }
    g_list_free(screen->grab_infos);
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_screen_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaScreen* screen = LUCCA_SCREEN(object);
    
    switch (property_id) {
    case PROP_VALID:
        g_value_set_boolean(value, screen->valid);
        break;
    case PROP_WIDTH:
        g_value_set_uint(value, (guint)gdk_screen_get_width(screen->screen));
        break;
    case PROP_HEIGHT:
        g_value_set_uint(value, (guint)gdk_screen_get_height(screen->screen));
        break;
    case PROP_WORKAREA_X:
        g_value_set_uint(value, screen->workarea_x);
        break;
    case PROP_WORKAREA_Y:
        g_value_set_uint(value, screen->workarea_y);
        break;
    case PROP_WORKAREA_WIDTH:
        g_value_set_uint(value, screen->workarea_width);
        break;
    case PROP_WORKAREA_HEIGHT:
        g_value_set_uint(value, screen->workarea_height);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_screen_class_init(gpointer g_class, gpointer class_data) {
    LuccaScreenClass* klass = (LuccaScreenClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_screen_dispose;
    gobclass->get_property = lucca_screen_get_property;

    /* properties */
    pspec = g_param_spec_boolean(
        "valid",
        "valid",
        "TRUE if this screen still exists on its display",
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_VALID, pspec);

    pspec = g_param_spec_uint(
        "width",
        "screen width",
        "width of the screen",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WIDTH, pspec);

    pspec = g_param_spec_uint(
        "height",
        "screen height",
        "height of the screen",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_HEIGHT, pspec);

    pspec = g_param_spec_uint(
        "workarea-x",
        "workarea x",
        "x coordinate of the work area",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WORKAREA_X, pspec);

    pspec = g_param_spec_uint(
        "workarea-y",
        "workarea y",
        "y coordinate of the work area",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WORKAREA_Y, pspec);

    pspec = g_param_spec_uint(
        "workarea-width",
        "workarea width",
        "width of the work area",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WORKAREA_WIDTH, pspec);

    pspec = g_param_spec_uint(
        "workarea-height",
        "workarea height",
        "height of the work area",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WORKAREA_HEIGHT, pspec);

    /* signals */
    klass->resize_workarea_signal_id = g_signal_new(
        "resize-workarea",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    klass->disconnect_signal_id = g_signal_new(
        "disconnect",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_screen_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaScreenClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_screen_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaScreen),
            0, /* n_preallocs */
            lucca_screen_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaScreenType", &info, 0);
    }
    return type;
}

/* Method definitions */

/* _root_filter: This is called before gdk processes any X events on a root window with a LuccaScreen; it's needed because gdk ignores some important window management events. */
GdkFilterReturn _root_filter(GdkXEvent *xevent, GdkEvent *event, gpointer data) {
    LuccaScreen* screen = LUCCA_SCREEN(data);
    XEvent* xeventunion = (XEvent*)xevent;
    
    if (xevent != NULL) {
        if (xeventunion->type == MapRequest) {
            LuccaWindow* window;
            
            /* an application wants to map a window */
            window = lucca_window_new(xeventunion->xmaprequest.window, screen, /*existing=*/FALSE);
        }
        else if (xeventunion->type == UnmapNotify &&
                    xeventunion->xunmap.send_event == True) { /* the ICCCM suggests treating any UnmapNotify we get, synthetic or not, as an indication that the window is being withdrawn, but we cannot distinguish those caused by lucca_window_configure from those caused by the application withdrawing its window */
            GdkWindow* gdkwindow;
            LuccaWindow* window;
            /* an application wants to withdraw a window */
            
            gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xunmap.window);
            window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
            
            if (window != NULL) {
                lucca_window_withdraw_finish(window);
            }
        }
        else if (xeventunion->type == KeyPress) {
            /* a keyboard event, probably from a grab */
            GList* item;
            unsigned int modifiers = xeventunion->xkey.state & ~screen->lock_mask;
            
            for (item=screen->grab_infos; item!=NULL; item=item->next) {
                GrabInfo* info = (GrabInfo*)item->data;
                
                if (info->keycode == xeventunion->xkey.keycode && info->xmodifiers == modifiers) {
                    GValue value={0,};
                    
                    g_value_init(&value, LUCCA_TYPE_DISPLAY);
                    
                    g_value_set_object(&value, screen->display);
                    
                    g_closure_invoke(info->hotkey->callback,
                                        NULL,
                                        1,
                                        &value,
                                        NULL);

                    g_value_unset(&value);
                    break;
                }
            }
        }
    }
    
    return GDK_FILTER_CONTINUE;
}

/* _global_filter: This is called before gdk processes any X events. */
GdkFilterReturn _global_filter(GdkXEvent *xevent, GdkEvent *event, gpointer data) {
    LuccaScreen* screen = LUCCA_SCREEN(data);
    XEvent* xeventunion = (XEvent*)xevent;
    
    if (xevent != NULL) {
        if (xeventunion->type == DestroyNotify) {
            GdkWindow* gdkwindow;
            LuccaWindow* window;
            /* an application wants to withdraw a window */
            
            gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xdestroywindow.window);
            window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
            
            if (window != NULL) {
                /* This LuccaWindow's window was destroyed. Bad window. */
                window->badwindow = TRUE;
                lucca_window_withdraw_finish(window);
            }
        }
        else if (xeventunion->type == ConfigureRequest) {
            GdkWindow* gdkwindow;
            LuccaWindow* window;
            /* an application wants to move/resize a window */
            
            gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xconfigurerequest.window);
            window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
            
            if (window) {
                window->needs_configurenotify = TRUE;
                
                /* FIXME: emit configure-request signal */
                
                if (window->needs_configurenotify) {
                    /* The ICCCM says that if we do not resize a window in response to a configure request, we need to send a synthetic ConfigureNotify to the client */
                    lucca_window_send_configure(window);
                }
            }
        }
        else if ((xeventunion->type == FocusIn || xeventunion->type == FocusOut) && xeventunion->xfocus.detail != NotifyPointer && xeventunion->xfocus.mode != NotifyGrab) {
            GdkWindow* gdkwindow;
            LuccaWindow* window;
            /* a window has gained or lost the focus */
            
            gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xfocus.window);
            window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
            
            if (window) {
                if (xeventunion->type == FocusIn) {
                    if (!window->ignore_focus) {
                        window->focused = TRUE;
                        g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->gain_focus_signal_id, 0);
                    }
                    else {
                        window->ignore_focus = FALSE;
                    }
                }
                else {
                    if (!window->ignore_focus) {
                        window->focused = FALSE;
                        g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->lose_focus_signal_id, 0);
                    }
                }
            }
        }
        else if (xeventunion->type == EnterNotify || xeventunion->type == LeaveNotify) {
            GdkWindow* gdkwindow;
            LuccaWindow* window;
            /* the pointer has entered or left a window */
            
            if (!screen->display->ignore_crossing) {
                
                gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xcrossing.window);
                window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
                
                if (window) {
                    if (xeventunion->type == EnterNotify) {
                        g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->mouse_in_signal_id, 0);
                    }
                    else {
                        g_signal_emit(window, LUCCA_WINDOW_GET_CLASS(window)->mouse_out_signal_id, 0);
                    }
                }
            }
        }
        else if (xeventunion->type == PropertyNotify) {
            GdkWindow* gdkwindow;
            LuccaWindow* window;
            /* a property changed on a window */
            
            gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xproperty.window);
            window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
            
            if (window) {
                lucca_window_property_notify(window, &(xeventunion->xproperty));
            }
        }
        else if (xeventunion->type == ClientMessage) {
            /* technically this is probably a message to the root window, but it's impossible to tell */
            char* message_type;
            
            message_type = XGetAtomName(screen->display->xdisplay, xeventunion->xclient.message_type);
            
            if (strcmp(message_type, "_NET_WM_STATE") == 0) {
                /* A client or pager wants to change a window's state */
                GdkWindow* gdkwindow;
                LuccaWindow* window;
                
                gdkwindow = gdk_xid_table_lookup_for_display(screen->display->display, xeventunion->xclient.window);
                window = g_hash_table_lookup(screen->display->valid_windows, gdkwindow);
                
                if (window != NULL) {
                    lucca_window_netwmstate_request(window, &xeventunion->xclient);
                }
            }
            
            XFree(message_type);
        }
        else if (xeventunion->type == SelectionClear && xeventunion->xselectionclear.window == screen->xwindow) {
            /* another window has grabbed the window manager selection, probably because it was run with --replace */
            lucca_display_disconnect(screen->display);
        }
        /* FIXME: handle SelectionRequest events */
    }
    
    return GDK_FILTER_CONTINUE;
}

void on_dock_loseownership(LuccaWindow* window, gpointer user_data) {
    LuccaScreen* screen = LUCCA_SCREEN(user_data);
    
    screen->docks = g_list_remove(screen->docks, window);
    
    lucca_screen_recalculate_docks(screen);
    
    g_object_unref(window);
}

void lucca_screen_initialize_real(LuccaScreen* screen) {
    Window* child_list;
    unsigned int child_count;
    Window dummy;
    unsigned int i, j, k;
    
    XModifierKeymap* modmap;
    GdkKeymap* keymap;
    
    GSList* item;
    
    screen->initialized = TRUE;    
    
    /* select for window management events on the root window */
    gdk_error_trap_push();
    lucca_display_add_x_events(screen->display, screen->root, SubstructureRedirectMask|SubstructureNotifyMask);
    gdk_flush();
    if (gdk_error_trap_pop()) {
        /* XSelectInput failed; this shouldn't happen because we've already acquired the WM_Sn selection and waited for other window managers to shut down */
        g_error("Couldn't select for SubstructureRedirectMask events on the default screen.");
        return;
    }
    
    /* make GDK forward those events to us */
    gdk_window_add_filter(screen->root, _root_filter, screen);
    gdk_window_add_filter(NULL, _global_filter, screen);

    /* get all the "lock" modifiers and meta/super/hyper */
    screen->lock_mask = 0;
    screen->meta_mask = 0;
    screen->super_mask = 0;
    screen->hyper_mask = 0;
    
    modmap = XGetModifierMapping(screen->display->xdisplay);
    keymap = gdk_keymap_get_for_display(screen->display->display);
    
    /* modmap->modifiermap is an array of modmap->max_keypermod symbols for each of the 8 modifiers */
    for (i=0; i<8; i++) {
        for (j=0; j<modmap->max_keypermod; j++) {
            KeyCode keycode = modmap->modifiermap[i*modmap->max_keypermod+j];
            guint* keyvals;
            gint keyval_count;

            gdk_keymap_get_entries_for_keycode(keymap, (guint)keycode, NULL, &keyvals, &keyval_count);

            for (k=0; k<keyval_count; k++) {
            
                switch (keyvals[k]) {
                case GDK_Num_Lock: case GDK_Scroll_Lock: case GDK_Caps_Lock: case GDK_Shift_Lock:
                    screen->lock_mask |= (1 << i);
                    break;
                case GDK_Meta_L: case GDK_Meta_R:
                    screen->meta_mask |= (1 << i);
                    break;
                case GDK_Super_L: case GDK_Super_R:
                    screen->super_mask |= (1 << i);
                    break;
                case GDK_Hyper_L: case GDK_Hyper_R:
                    screen->hyper_mask |= (1 << i);
                    break;
                }
            }
            
            g_free(keyvals);
        }
    }
    
    XFreeModifiermap(modmap);
    
    /* set window manager properties on the root window */

    /* set up hotkeys that have already been added */
    for (item=screen->display->hotkey_infos; item!=NULL; item=item->next) {
        lucca_screen_add_hotkey(screen, (HotkeyInfo*)item->data);
    }

    /* create a LuccaWindow and emit a signal for existing windows */
    XQueryTree(screen->display->xdisplay, screen->xroot, &dummy, &dummy, &child_list, &child_count);
    for (i=0; i<child_count; i++) {
        lucca_window_new(child_list[i], screen, /*existing=*/TRUE);
    }
    if (child_list != NULL)
        XFree(child_list);
}

gboolean _remove_previous_wm_filter(gpointer data);

GdkFilterReturn _previous_wm_filter(GdkXEvent* xevent, GdkEvent* event, gpointer data) {
    LuccaScreen* screen = LUCCA_SCREEN(data);
    XEvent* xev = (XEvent*)xevent;
    
    if (screen->initialized == FALSE && xev != NULL && xev->type == DestroyNotify) {
        /* the previous window manager has shut down; start the real initialization */
        lucca_screen_initialize_real(screen);
        
        /* add an idle handler to remove this filter because GDK doesn't like filters that add or remove filters */
        g_idle_add(_remove_previous_wm_filter, screen);
    }
    
    return GDK_FILTER_CONTINUE;
}

gboolean _remove_previous_wm_filter(gpointer data) {
    LuccaScreen* screen = LUCCA_SCREEN(data);
    
    gdk_window_remove_filter(screen->previous_wm, _previous_wm_filter, screen);
    g_object_unref(screen->previous_wm);
    screen->previous_wm = NULL;
    
    return FALSE;
}

void lucca_screen_initialize(LuccaScreen* screen, GdkScreen* gdkscreen, LuccaDisplay* display, gboolean replace, GError** error) {
    gchar* selection_name;
    Atom selection_atom;
    Window x_previous_wm;
    
    screen->display = g_object_ref(display);
    g_object_ref(display);
    screen->screen = g_object_ref(gdkscreen);
    g_object_ref(gdkscreen);
    screen->xscreen = GDK_SCREEN_XSCREEN(gdkscreen);
    screen->root = g_object_ref(gdk_screen_get_root_window(gdkscreen));
    g_object_ref(screen->root);
    screen->xroot = GDK_WINDOW_XWINDOW(screen->root);
    
    /* is there already a window manager running? */
    selection_name = g_strdup_printf("WM_S%d", GDK_SCREEN_XNUMBER(screen->screen));
    selection_atom = XInternAtom(screen->display->xdisplay, selection_name, False);
    g_free(selection_name);
    
    x_previous_wm = XGetSelectionOwner(screen->display->xdisplay, selection_atom);
    screen->previous_wm = gdk_window_foreign_new_for_display(screen->display->display, x_previous_wm);
    
    /* FIXME: can the selection owner change from None to a real window in this time? */
    
    if (screen->previous_wm != NULL) {
        if (replace == TRUE) {
            /* make sure we'll get an event when the current window manager shuts down */
            lucca_display_add_x_events(screen->display, screen->previous_wm, StructureNotifyMask);
    
            /* create an invisible window for IPC */
            screen->window = gtk_invisible_new_for_screen(screen->screen);
            g_object_ref_sink(screen->window);
            screen->xwindow = GDK_WINDOW_XWINDOW(screen->window->window);
            
            XSetSelectionOwner(screen->display->xdisplay, selection_atom, GDK_WINDOW_XWINDOW(screen->window->window), CurrentTime);
            
            /* FIXME: check if we really got the selection */
            
            /* wait for the current window manager to shut down */
            gdk_window_add_filter(screen->previous_wm, _previous_wm_filter, screen);
        }
        else {
            g_set_error(error, LUCCA_DISPLAY_ERROR, LUCCA_DISPLAY_ERROR_EXISTINGWM, "There's already a window manager running on this screen.");
            return;
        }
    }
    else {
        /* create an invisible window for IPC */
        screen->window = gtk_invisible_new_for_screen(screen->screen);
        g_object_ref_sink(screen->window);
        screen->xwindow = GDK_WINDOW_XWINDOW(screen->window->window);

        XSetSelectionOwner(screen->display->xdisplay, selection_atom, GDK_WINDOW_XWINDOW(screen->window->window), CurrentTime);;
        
        /* FIXME: check if we really got the selection */
    }
    
    /* FIXME: handle SelectionRequest events */
    
    screen->display->valid = TRUE;
    screen->valid = TRUE;
    
    screen->workarea_x = 0;
    screen->workarea_y = 0;
    screen->workarea_width = (guint)gdk_screen_get_width(screen->screen);
    screen->workarea_height = (guint)gdk_screen_get_height(screen->screen);
    
    /* emit a signal for the new screen */
    g_signal_emit(display, LUCCA_DISPLAY_GET_CLASS(display)->new_screen_signal_id, 0, screen);
    
    if (screen->previous_wm == NULL) {
        lucca_screen_initialize_real(screen);
    }
}

void lucca_screen_recalculate_docks(LuccaScreen* screen) {
    guint workarea_x, workarea_y, workarea_width, workarea_height;
    GList* item;
    
    workarea_x = 0;
    workarea_y = 0;
    workarea_width = (guint)gdk_screen_get_width(screen->screen);
    workarea_height = (guint)gdk_screen_get_height(screen->screen);
    
    for (item=screen->docks; item!=NULL; item=item->next) {
        LuccaWindow* window = LUCCA_WINDOW(item->data);
        gint x, y, width, height;
        
        if (window->strut_left) {
            if (window->strut_left > workarea_width || window->strut_left < 0)
                continue;
        
            x = workarea_x;
            if (window->left_start_y >= workarea_y)
                y = window->left_start_y;
            else
                y = workarea_y;
            width = window->strut_left;
            if (window->left_end_y <= workarea_y+workarea_height-1)
                height = window->left_end_y - y + 1;
            else
                height = workarea_height + workarea_y - y + 1;
            
            workarea_x += window->strut_left;
            workarea_width -= window->strut_left;
            
            lucca_window_configure(window, screen->root, x, y, width, height, TRUE);
        }
        else if (window->strut_right) {
            if (window->strut_right > workarea_width || window->strut_right < 0)
                continue;
        
            x = workarea_width + workarea_x - window->strut_right;
            if (window->right_start_y >= workarea_y)
                y = window->right_start_y;
            else
                y = workarea_y;
            width = window->strut_right;
            if (window->right_end_y <= workarea_y+workarea_height-1)
                height = window->right_end_y - y + 1;
            else
                height = workarea_height + workarea_y - y + 1;
            
            workarea_width -= window->strut_right;
            
            lucca_window_configure(window, screen->root, x, y, width, height, TRUE);
        }
        else if (window->strut_top) {
            if (window->strut_top > workarea_height || window->strut_top < 0)
                continue;
        
            y = workarea_y;
            if (window->top_start_x >= workarea_x)
                x = window->top_start_x;
            else
                x = workarea_x;
            height = window->strut_top;
            if (window->top_end_x <= workarea_x+workarea_width-1)
                width = window->top_end_x - x + 1;
            else
                width = workarea_width + workarea_x - x + 1;
            
            workarea_y += window->strut_top;
            workarea_height -= window->strut_top;
            
            lucca_window_configure(window, screen->root, x, y, width, height, TRUE);
        }
        else if (window->strut_bottom) {
            if (window->strut_bottom > workarea_height || window->strut_bottom < 0)
                continue;
        
            y = workarea_height + workarea_y - window->strut_bottom;
            if (window->bottom_start_x >= workarea_x)
                x = window->bottom_start_x;
            else
                x = workarea_x;
            height = window->strut_bottom;
            if (window->bottom_end_x <= workarea_x+workarea_width-1)
                width = window->bottom_end_x - x + 1;
            else
                width = workarea_width + workarea_x - x + 1;
            
            workarea_height -= window->strut_bottom;
            
            lucca_window_configure(window, screen->root, x, y, width, height, TRUE);
        }
        else {
            lucca_window_configure(window, screen->root, 0, 0, 1, 1, FALSE);
        }
    }
    
    if (workarea_x != screen->workarea_x || workarea_y != screen->workarea_y || workarea_width != screen->workarea_width || workarea_height != screen->workarea_height) {
        screen->workarea_x = workarea_x;
        screen->workarea_y = workarea_y;
        screen->workarea_width = workarea_width;
        screen->workarea_height = workarea_height;
        
        g_signal_emit(screen, LUCCA_SCREEN_GET_CLASS(screen)->resize_workarea_signal_id, 0);
    }
}

LuccaDisplay* lucca_screen_get_display(LuccaScreen* screen) {
    return screen->display;
}

GdkWindow* lucca_screen_get_root(LuccaScreen* screen) {
    return screen->root;
}

gboolean lucca_screen_dock_window(LuccaScreen* screen, LuccaWindow* window) {
    GClosure* ownership_callback;
    
    screen->docks = g_list_append(screen->docks, g_object_ref(window));
    
    ownership_callback = g_cclosure_new(G_CALLBACK(on_dock_loseownership), screen, NULL);
    g_closure_set_marshal(ownership_callback, g_cclosure_marshal_VOID__VOID);
    lucca_window_change_owner(window, ownership_callback);
    
    lucca_screen_recalculate_docks(screen);
    
    return TRUE;
}

void lucca_screen_add_hotkey(LuccaScreen* screen, HotkeyInfo* info) {
    GdkKeymap* keymap;
    GdkKeymapKey* keys;
    gint key_count;
    guint i;
    guint error;
    unsigned int xmodifiers;

    keymap = gdk_keymap_get_for_display(screen->display->display);

    /* convert GDK modifiers into X modifiers; GDK_META_MASK etc. actually map to other bits that are determined at runtime */
    xmodifiers = info->modifiers & 0x1fff;
    if (info->modifiers & GDK_SUPER_MASK)
        xmodifiers |= screen->super_mask;
    if (info->modifiers & GDK_HYPER_MASK)
        xmodifiers |= screen->hyper_mask;
    if (info->modifiers & GDK_META_MASK)
        xmodifiers |= screen->meta_mask;
    xmodifiers &= ~screen->lock_mask;
    
    /* convert the keyval to "keycodes" */
    gdk_keymap_get_entries_for_keyval(keymap, info->keyval, &keys, &key_count);

    gdk_error_trap_push();

    for (i=0; i<key_count; i++) {
        GdkKeymapKey* key = &(keys[i]);
        unsigned int lock_modifiers=screen->lock_mask;
        GrabInfo* grab_info;
        
        /* we need to run XGrabKey with every possible combination of "lock" modifiers, effectively ignoring them */
        do {
            XGrabKey(screen->display->xdisplay, (int)key->keycode, xmodifiers|lock_modifiers, screen->xroot, True, GrabModeAsync, GrabModeAsync);
        
            lock_modifiers = (lock_modifiers-1)&screen->lock_mask;
        } while (lock_modifiers != screen->lock_mask);
        
        /* keep track of the keycode and X modifiers; because we ignored the "group" and "level", we may get events from this grab that map to a different keyval */
        grab_info = g_slice_new(GrabInfo);
        grab_info->screen = screen;
        grab_info->keycode = (unsigned int)key->keycode;
        grab_info->xmodifiers = xmodifiers;
        grab_info->hotkey = info;
        
        screen->grab_infos = g_list_prepend(screen->grab_infos, grab_info);
    }
    
    g_free(keys);
    
    gdk_flush();
    
    error = gdk_error_trap_pop();
    
    if (error == BadAccess) {
        /* Another client has grabbed this key. Curses. */
    }
    else if (error != 0) {
        g_error("Unexpected X error: %i", error);
    }
}

void lucca_screen_disconnect(LuccaScreen* screen) {
    XWindowAttributes xattr;
    XSetWindowAttributes xsetattr;
    
    /* stop listening for SubstructureRedirect on the root */    
    XGetWindowAttributes(screen->display->xdisplay, screen->xroot, &xattr);
    xsetattr.event_mask = xattr.your_event_mask&~SubstructureRedirectMask;
    XChangeWindowAttributes(screen->display->xdisplay, screen->xroot, CWEventMask, &xsetattr);
    
    /* remove event filters */
    gdk_window_remove_filter(screen->root, _root_filter, screen);
    gdk_window_remove_filter(NULL, _global_filter, screen);
    
    /* destroy the IPC window */
    g_object_unref(screen->window);
    screen->window = NULL;
    screen->xwindow = None;
    
    /* it's not necessary to XUngrabKey because the grab window no longer exists */
    
    screen->valid = FALSE;
    
    /* set the focus to PointerRoot */
    XSetInputFocus(screen->display->xdisplay, PointerRoot, RevertToPointerRoot, gdk_x11_display_get_user_time(screen->display->display));
    
    /* emit the disconnect event */
    g_signal_emit(screen, LUCCA_SCREEN_GET_CLASS(screen)->disconnect_signal_id, 0);
}

