/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_DISPLAY_PRIVATE_H
#define LUCCA_DISPLAY_PRIVATE_H

#include <gdk/gdkx.h>
#include <X11/Xlib.h>

#include <string.h>

#include "lucca-bin.h"
#include "lucca-display.h"

/* Private definitons for the X Window Management Module */

struct _LuccaDisplay {
    GObject parent;
    /* instance members */
    gboolean valid;
    GdkDisplay* display;
    Display* xdisplay;
    struct _LuccaScreen* default_screen;
    
    GHashTable* valid_windows; /* maps GdkWindow's to LuccaWindow's */

    GSList* hotkey_infos;

    gchar* wm_name;

    gboolean ignore_crossing;

    /* atoms */
    Atom xatom_atom;
    Atom xcardinal_atom;
    
    gboolean dispose_has_run;
};

struct _LuccaDisplayClass {
    GObjectClass parent;
    /* class members */
    gint new_screen_signal_id;
    gint new_window_signal_id;
};

struct _LuccaScreen {
    GObject parent;
    /* instance members */
    gboolean valid;
    gboolean initialized;
    GdkScreen* screen;
    Screen* xscreen;
    LuccaDisplay* display;
    GdkWindow* root;
    Window xroot;

    GList* docks;
    guint workarea_x, workarea_y, workarea_width, workarea_height;
    
    GdkWindow* previous_wm;
    
    GtkWidget* window;
    Window xwindow;
    
    int lock_mask; /* All "lock" modifier bits; hotkeys must be grabbed with every combination of these */
    int meta_mask;
    int super_mask;
    int hyper_mask;
    
    GList* grab_infos;
    
    gboolean dispose_has_run;
};

struct _LuccaScreenClass {
    GObjectClass parent;
    /* class members */
    gint resize_workarea_signal_id;
    gint disconnect_signal_id;
};

typedef gulong ICCCM_STATE; /* possible values: WithdrawnState, NormalState, IconicState; according to the ICCCM, managed windows are either Normal or Iconic, depending on whether the window would be visible when not obscured */
typedef struct {
    ICCCM_STATE state;  /* current state according to the window manager, in ICCCM terms */
    Window icon;    /* icon window; this is specified in the ICCCM but never used in practice; it will always be None */
} ICCCM_WM_STATE;   /* type for the WM_STATE hint */

struct _LuccaWindow {
    GObject parent;
    /* instance members */
    gboolean valid;
    gboolean badwindow; /* if TRUE, this window doesn't exist anymore, but the rest of the program might not know that yet */
    gboolean internal;
    LuccaDisplay* display;
    GdkWindow* window;
    Window xwindow;

    LuccaBin* bin;
    
    XWindowAttributes xattr;
    XWMHints* xwmhints;
    XSizeHints xsizehints;
    long xsizehints_supplied;
    GData* properties_set;
    LuccaType type;
    
    gboolean supports_delete_window;
    
    gboolean focused;
    
    gboolean needs_configurenotify;
    
    gboolean ignore_focus;
    
    GdkWindow* window_parent;
    
    ICCCM_STATE icccm_state;
    
    LuccaState requested_states;
    LuccaState netwm_states;
    
    gchar* requested_title;
    
    GClosure* owner_callback;
    
    /* _NET_WM_STRUT_PARTIAL info */
    gint strut_left, strut_right, strut_top, strut_bottom, left_start_y, left_end_y, right_start_y, right_end_y, top_start_x, top_end_x, bottom_start_x, bottom_end_x;
    
    gboolean dispose_has_run;
};

struct _LuccaWindowClass {
    GObjectClass parent;
    /* class members */
    gint withdraw_signal_id;
    gint gain_focus_signal_id;
    gint lose_focus_signal_id;
    gint mouse_in_signal_id;
    gint mouse_out_signal_id;
    gint state_request_signal_id;
};

/* structs for storing information about keyboard grabs */
typedef struct {
    LuccaDisplay* display;
    guint keyval; /* these are apparently the same as an X KeySym */
    GdkModifierType modifiers;
    GClosure* callback;
} HotkeyInfo;

typedef struct {
    LuccaScreen* screen;
    unsigned int keycode;
    unsigned int xmodifiers;
    HotkeyInfo* hotkey;
} GrabInfo;

/* private methods */

/* lucca_display_add_x_events: select for the given X events in addition to anything already selected */
void lucca_display_add_x_events(LuccaDisplay* display, GdkWindow* window, long event_mask);

/* lucca_screen_init: initializes a LuccaScreen from a GdkScreen, taking over as window manager for that screen */
void lucca_screen_initialize(LuccaScreen* screen, GdkScreen* gdkscreen, LuccaDisplay* display, gboolean replace, GError** error);

/* lucca_screen_recalculate_docks: update positions of docking windows and  */
void lucca_screen_recalculate_docks(LuccaScreen* screen);

/* lucca_screen_add_hotkey: grabs a hotkey */
void lucca_screen_add_hotkey(LuccaScreen* screen, HotkeyInfo* info);

/* lucca_screen_disconnect: stop managing a screen */
void lucca_screen_disconnect(LuccaScreen* screen);

/* lucca_window_new: creates a LuccaWindow from an X Window (and adds to a LuccaDisplay) or returns NULL if the window should not be managed
    existing: TRUE if this is an existing window (as opposed to a window that is currently asking to be mapped)
    internal: TRUE if this window was created with lucca_display_create_internal
     */
LuccaWindow* lucca_window_new(Window xwindow, LuccaScreen* screen, gboolean existing);

/* lucca_window_new_internal: creates a LuccaWindow from a LuccaBin, stealing a reference to the bin */
LuccaWindow* lucca_window_new_internal(LuccaBin* bin, LuccaScreen* screen);

/* lucca_window_get_atoms: get a list of atoms in a window property, to be freed with XFree() */
Atom* lucca_window_get_atoms(LuccaWindow* window, gchar*  property, guint* length);

/* lucca_window_get_utf8: Gets a utf8-encoded property from a window and writes it to value
    value should be freed with g_free()  */
gchar* lucca_window_get_utf8(LuccaWindow* window, gchar*  property);

/* lucca_window_get_cardinals: gets a list of CARDINAL values in a window property, to be freed with XFree() */
int* lucca_window_get_cardinals(LuccaWindow* window, gchar*  property, guint* length);

/* lucca_window_withdraw_finish: complete the process of withdrawing a client that has hidden (or destroyed) its window */
void lucca_window_withdraw_finish(LuccaWindow* window);

/* lucca_window_send_configure: send a synthetic ConfigureNotify event to a window */
void lucca_window_send_configure(LuccaWindow* window);

/* lucca_window_property_notify: called when we get a PropertyNotify event for a window */
void lucca_window_property_notify(LuccaWindow* window, XPropertyEvent* event);

/* lucca_window_netwmstate_request: called when we get a ClientMessage of type _NET_WM_STATE */
void lucca_window_netwmstate_request(LuccaWindow* window, XClientMessageEvent* event);

#endif
