/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_DISPLAY_H
#define LUCCA_DISPLAY_H

/* Public declarations for the X Window Management Module */

#include <glib.h>
#include <glib-object.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include "lucca-common.h"

/* definition of LuccaType macros */
/* FIXME: this should probably be defined somewhere else or moved out of lucca-common.h; actual type registration requires an object file, not just a header */
#define LUCCA_TYPE_TYPE lucca_type_get_type()
#define LUCCA_TYPE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_TYPE, LuccaType))
#define LUCCA_TYPE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_TYPE, LuccaType))
#define LUCCA_IS_TYPE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_TYPE))
#define LUCCA_IS_TYPE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_TYPE))
#define LUCCA_TYPE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_TYPE, LuccaType))

GType lucca_type_get_type();

/* Boiler-plate definition of LuccaDisplay */
#define LUCCA_TYPE_DISPLAY lucca_display_get_type()
#define LUCCA_DISPLAY(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_DISPLAY, LuccaDisplay))
#define LUCCA_DISPLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_DISPLAY, LuccaDisplayClass))
#define LUCCA_IS_DISPLAY(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_DISPLAY))
#define LUCCA_IS_DISPLAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_DISPLAY))
#define LUCCA_DISPLAY_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_DISPLAY, LuccaDisplayClass))

typedef struct _LuccaDisplay LuccaDisplay;
typedef struct _LuccaDisplayClass LuccaDisplayClass;

GType lucca_display_get_type();

/* Boiler-plate definition of LuccaScreen */
#define LUCCA_TYPE_SCREEN lucca_screen_get_type()
#define LUCCA_SCREEN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_SCREEN, LuccaScreen))
#define LUCCA_SCREEN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_SCREEN, LuccaScreenClass))
#define LUCCA_IS_SCREEN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_SCREEN))
#define LUCCA_IS_SCREEN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_SCREEN))
#define LUCCA_SCREEN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_SCREEN, LuccaScreenClass))

typedef struct _LuccaScreen LuccaScreen;
typedef struct _LuccaScreenClass LuccaScreenClass;

GType lucca_screen_get_type();

/* Boiler-plate definition of LuccaWindow */
#define LUCCA_TYPE_WINDOW lucca_window_get_type()
#define LUCCA_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_WINDOW, LuccaWindow))
#define LUCCA_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_WINDOW, LuccaWindowClass))
#define LUCCA_IS_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_WINDOW))
#define LUCCA_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_WINDOW))
#define LUCCA_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_WINDOW, LuccaWindowClass))

typedef struct _LuccaWindow LuccaWindow;
typedef struct _LuccaWindowClass LuccaWindowClass;

GType lucca_window_get_type();

/* error constants: */
#define LUCCA_DISPLAY_ERROR lucca_display_error_quark()
GQuark lucca_display_error_quark();
enum {
    LUCCA_DISPLAY_ERROR_EXISTINGWM,
} LuccaDisplayError;

/* LuccaDisplay method definitions */

/* lucca_display_connect: take over as window manager of a display */
void lucca_display_connect(LuccaDisplay* lucca_display, GdkDisplay* display, const gchar* name, gboolean replace, GError** error);

/* lucca_display_disconnect: stop managing a display */
void lucca_display_disconnect(LuccaDisplay* lucca_display);

/* lucca_display_get_default_screen: returns the default screen of a display */
LuccaScreen* lucca_display_get_default_screen(LuccaDisplay* display);

/* lucca_display_get_windows: return a new GList containing all valid LuccaWindow's for this display;  */
GList* lucca_display_get_windows(LuccaDisplay* display);

/* lucca_display_create_internal: create a new internal window on the display */
LuccaWindow* lucca_display_create_internal(LuccaDisplay* display);

/* lucca_display_add_hotkey: grab a key and call callback when it is pressed */
void lucca_display_add_hotkey(LuccaDisplay* display, guint keyval, GdkModifierType modifiers, GClosure* callback);

/* LuccaScreen method definitions */

/* lucca_screen_get_display: returns the LuccaDisplay that created a screen */
LuccaDisplay* lucca_screen_get_display(LuccaScreen* screen);

/* lucca_screen_get_root: returns the root window of a screen */
GdkWindow* lucca_screen_get_root(LuccaScreen* screen);

/* lucca_screen_dock_window: attempts to dock the window to a screen edge and returns TRUE if it succeeds */
gboolean lucca_screen_dock_window(LuccaScreen* screen, LuccaWindow* window);

/* LuccaWindow method definitions */

/* lucca_window_get_geometry_hints: gets the geometry hints of a window */
void lucca_window_get_geometry_hints(LuccaWindow* window, GdkGeometry* geometry, GdkWindowHints* geom_mask);

/* lucca_window_get_requested_states: gets the set of requested states for the window */
LuccaState lucca_window_get_requested_states(LuccaWindow* window);

/* lucca_window_set_states: set the "state" of the window */
void lucca_window_set_states(LuccaWindow* window, LuccaState states);

/* lucca_window_get_states: get the "state" of the window */
LuccaState lucca_window_get_states(LuccaWindow* window);

/* lucca_window_configure: map a window with the specified parent, size, and position or hide the window */
void lucca_window_configure(LuccaWindow* window, GdkWindow* parent, gint x, gint y, gint width, gint height, gboolean visible);

/* lucca_window_change_owner: become the "owner" of a window and call the previous owner's callback */
void lucca_window_change_owner(LuccaWindow* window, GClosure* callback);

/* lucca_window_close: ask the application that owns this window to close it */
void lucca_window_close(LuccaWindow* window);

/* lucca_window_focus: give a window the focus */
void lucca_window_give_focus(LuccaWindow* window);

/* lucca_window_withdraw: hide a window and stop managing it */
void lucca_window_withdraw(LuccaWindow* window);

/* lucca_window_transient_for: returns a list of LuccaWindow's that this window should be above */
GList* lucca_window_transient_for(LuccaWindow* window);

#endif
