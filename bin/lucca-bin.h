/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_BIN_H
#define LUCCA_BIN_H

#include <glib.h>
#include <glib-object.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gtk/gtkwindow.h>

/*
  LuccaBin is a GtkBin that has no border and accepts any fixed size and 
  parent GdkWindow. It is used to implement internal LuccaWindow objects.
*/

/* Boiler-plate definition of LuccaBin */
#define LUCCA_TYPE_BIN lucca_bin_get_type()
#define LUCCA_BIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_BIN, LuccaBin))
#define LUCCA_BIN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_BIN, LuccaBinClass))
#define LUCCA_IS_BIN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_BIN))
#define LUCCA_IS_BIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_BIN))
#define LUCCA_BIN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_BIN, LuccaBinClass))

typedef struct _LuccaBin LuccaBin;
typedef struct _LuccaBinClass LuccaBinClass;

GType lucca_bin_get_type();

struct _LuccaBin {
    GtkWindow parent;
    /* instance members */
    
    /* < private > */
    GdkWindow* parent_window;
    
    gboolean dispose_has_run;
};

struct _LuccaBinClass {
    GtkWindowClass parent;
    /* class members */
};

/* method declarations */
void lucca_bin_set_parent(LuccaBin* bin, GdkWindow* parent);

void lucca_bin_move_resize(LuccaBin* bin, gint x, gint y, gint width, gint height);

#endif

