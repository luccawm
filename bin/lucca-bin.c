/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-bin.h"

/* Implementation of LuccaBin */

static GtkWidgetClass* parent_class = NULL;
static GtkBinClass* bin_class = NULL;

/* method override declarations */
static void lucca_bin_realize(GtkWidget* widget);
static void lucca_bin_size_allocate(GtkWidget* widget, GtkAllocation *allocation);

static void lucca_bin_init(GTypeInstance* instance, gpointer g_class) {
    LuccaBin* bin = LUCCA_BIN(instance);
    GtkWindow* window = GTK_WINDOW(instance);

    window->type = GTK_WINDOW_POPUP;
    
    bin->dispose_has_run = FALSE;
    bin->parent_window = NULL;
}

static void lucca_bin_dispose(GObject* obj) {
    LuccaBin* bin = LUCCA_BIN(obj);
    
    if (bin->dispose_has_run) {
        return;
    }
    bin->dispose_has_run = TRUE;
    
    g_object_unref(bin->parent_window);
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_bin_class_init(gpointer g_class, gpointer class_data) {
    LuccaBinClass* klass = (LuccaBinClass*)g_class;
    GObjectClass* gobject_class = G_OBJECT_CLASS(klass);
    GtkWidgetClass* widget_class = GTK_WIDGET_CLASS(g_class);

    /* method overrides */
    gobject_class->dispose = lucca_bin_dispose;
    widget_class->realize = lucca_bin_realize;
    widget_class->size_allocate = lucca_bin_size_allocate;

    parent_class = g_type_class_peek_parent(klass);
    
    bin_class = g_type_class_peek(GTK_TYPE_BIN);
}

GType lucca_bin_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaBinClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_bin_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaBin),
            0, /* n_preallocs */
            lucca_bin_init
        };
        type = g_type_register_static (GTK_TYPE_WINDOW, "LuccaBinType", &info, 0);
    }
    return type;
}

/* method override defintions */
static void lucca_bin_realize(GtkWidget* widget) {
    LuccaBin* bin = LUCCA_BIN(widget);
    GtkWindow* window = GTK_WINDOW(widget);
    GdkWindowAttr window_attributes;
    gint attributes_mask=0;

    window->type = GTK_WINDOW_POPUP; /* FIXME: For a reason I do not understand, this is reset to GTK_WINDOW_TOPLEVEL when a LuccaBin is created; there's probably a better way to deal with this problem, but I don't know what it is */

    window_attributes.window_type = GDK_WINDOW_TEMP;
    
    if (window->title != NULL) {
        window_attributes.title = window->title;
        attributes_mask |= GDK_WA_TITLE;
    }
    
    window_attributes.wmclass_name = window->wmclass_name;
    window_attributes.wmclass_class = window->wmclass_class;
    attributes_mask |= GDK_WA_WMCLASS;
    
    window_attributes.wclass = GDK_INPUT_OUTPUT;

    window_attributes.visual = gtk_widget_get_visual(widget);
    window_attributes.colormap = gtk_widget_get_colormap(widget);
    attributes_mask |= GDK_WA_VISUAL|GDK_WA_COLORMAP;
    
    window_attributes.x = widget->allocation.x;
    window_attributes.y = widget->allocation.y;
    window_attributes.width = widget->allocation.width;
    window_attributes.height = widget->allocation.height;
    attributes_mask |= GDK_WA_X|GDK_WA_Y;

    window_attributes.event_mask = gtk_widget_get_events (widget);
    window_attributes.event_mask |= GDK_EXPOSURE_MASK|GDK_KEY_PRESS_MASK|GDK_KEY_RELEASE_MASK|GDK_ENTER_NOTIFY_MASK|GDK_LEAVE_NOTIFY_MASK|GDK_FOCUS_CHANGE_MASK|GDK_STRUCTURE_MASK; /* GtkWindows set these events; I have no idea why */
    
    window_attributes.override_redirect = TRUE;
    attributes_mask |= GDK_WA_NOREDIR;
    
    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
    
    widget->window = gdk_window_new(bin->parent_window, &window_attributes, attributes_mask);
    
    gdk_window_set_user_data(widget->window, bin);
    
    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
}

static void _size_allocate(LuccaBin* bin) {
    GtkBin* gtkbin = GTK_BIN(bin);
    GtkWidget* widget = GTK_WIDGET(bin);
    GtkAllocation child_allocation;
    
    child_allocation.x = 0;
    child_allocation.y = 0;
    child_allocation.width = widget->allocation.width;
    child_allocation.height = widget->allocation.height;
    
    if (gtkbin->child)
        gtk_widget_size_allocate(gtkbin->child, &child_allocation);
}

static void lucca_bin_size_allocate(GtkWidget* widget, GtkAllocation *allocation) {
    LuccaBin* bin = LUCCA_BIN(widget);

    widget->allocation = *allocation;
    
    _size_allocate(bin);
}

void lucca_bin_set_parent(LuccaBin* bin, GdkWindow* parent) {
    GtkWidget* widget = GTK_WIDGET(bin);
    gboolean remap=FALSE;
    
    if (bin->parent_window)
        g_object_unref(bin->parent_window);
    bin->parent_window = g_object_ref(parent);
    
    if (GTK_WIDGET_REALIZED(widget)) {
        if (GTK_WIDGET_MAPPED(widget)) {
            gtk_widget_unmap(widget);
            remap = TRUE;
        }
        
        gdk_window_reparent(widget->window, bin->parent_window, widget->allocation.x, widget->allocation.y);
        
        if (remap) {
            gtk_widget_map(widget);
        }
    }
}

void lucca_bin_move_resize(LuccaBin* bin, gint x, gint y, gint width, gint height) {
    GtkWidget* widget = GTK_WIDGET(bin);
    
    gtk_widget_set_size_request(widget, width, height);
    
    widget->allocation.x = x;
    widget->allocation.y = y;
    
    if (GTK_WIDGET_REALIZED(widget)) {
        gtk_window_move(GTK_WINDOW(widget), x, y);
        gtk_window_resize(GTK_WINDOW(widget), width, height);
        
        _size_allocate(bin);
    }
}

