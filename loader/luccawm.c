/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-display.h"
#include "lucca-core.h"

#ifdef HAVE_GNOME
#include <libgnome/libgnome.h>
#include <libgnomeui/libgnomeui.h>
#endif

#ifdef LUCCA_DEBUG
#include <signal.h>
#include "crashcatcher.h"
#endif

LuccaDisplay* display;

#ifdef HAVE_GNOME
void on_die(GnomeClient* client, gpointer user_data) {
    lucca_display_disconnect(display);
}
#endif

#ifdef LUCCA_DEBUG
const int debug_signals[] = {SIGSEGV, SIGILL, SIGFPE, SIGBUS};

void log_handler (const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data) {
    char errdesc[512];
    
    snprintf(errdesc, sizeof(errdesc)/sizeof(char), "Fatal error in %s: %s", log_domain, message);
    
    cc_crash_log(errdesc);
}
#endif

int main(int argc, char** argv) {
    LuccaApplication* application;
    GError* err=NULL;
    gboolean replace;
    GOptionContext* option_context;
    GOptionEntry option_entries[] = {
        {"replace", 0, 0, G_OPTION_ARG_NONE, &replace, "Replace the running window manager with Lucca WM", NULL},
        {0,}};
#ifdef HAVE_GNOME
    GnomeProgram* program;
    GnomeClient* client;
    gchar* current_dir;
#else
    gboolean result;
#endif
    
    option_context = g_option_context_new(NULL);
    
    g_option_context_add_main_entries(option_context, option_entries, NULL);
    
#ifdef HAVE_GNOME
    program = gnome_program_init("luccawm", "0.1.2", LIBGNOMEUI_MODULE, argc, argv, GNOME_PARAM_GOPTION_CONTEXT, option_context, GNOME_PARAM_NONE);
    
    client = gnome_master_client();
    g_object_ref_sink(client);
    
    g_signal_connect(client, "die", G_CALLBACK(on_die), NULL);
    
    gnome_client_set_restart_command(client, 1, argv);
    current_dir = g_get_current_dir();
    gnome_client_set_current_directory(client, current_dir);
    g_free(current_dir);
    
    gnome_client_set_restart_style(client, GNOME_RESTART_IMMEDIATELY);
#else
    g_option_context_add_group(option_context, gtk_get_option_group(TRUE));
    
    result = g_option_context_parse(option_context, &argc, &argv, &err);
    
    g_option_context_free(option_context);
    
    if (result == FALSE) {
        g_printerr("%s\n", err->message);
        return 1;
    }
#endif
    
#ifdef LUCCA_DEBUG
    /* debug fatal signals */
    cc_install_handlers(sizeof(debug_signals)/sizeof(int), (int*)debug_signals, "lucca_crash.log", NULL);
    
    /* debug fatal log messages */
    g_log_set_handler(NULL, G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL, log_handler, NULL);
    
    /* debug all GTK log messages */
    g_log_set_handler("Gtk", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
    
    /* debug all GLib log messages */
    g_log_set_handler("GLib", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
    
    /* debug all GObject log messages */
    g_log_set_handler("GLib-GObject", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, log_handler, NULL);
#endif
    
    display = g_object_new(LUCCA_TYPE_DISPLAY, NULL);
    
    application = g_object_new(LUCCA_TYPE_APPLICATION, "display", display, NULL);
    
    lucca_display_connect(display, gdk_display_get_default(), "LuccaWM", replace, &err);

    if (err != NULL) {
        g_printerr("%s\n", err->message);
        return -1;
    }
    
    gtk_main();
    
#ifdef HAVE_GNOME
    gnome_client_disconnect(client); /* this is needed so the session manager doesn't think this was a crash */
    
    g_object_unref(client);
    g_object_unref(program);
#endif
    
    return 0;
}

