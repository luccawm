# Copyright (c) 2008 Vincent Povirk
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import subprocess

project_env = Environment()
Export('project_env')

if ARGUMENTS.get('debug', 1) not in [0, '0']:
    project_env.MergeFlags('-g -DLUCCA_DEBUG')
    debug = True
else:
    project_env.MergeFlags('-O')
    debug = False
Export('debug')

if ARGUMENTS.get('usegnome', 1) not in [0, '0']:
    usegnome = True
else:
    usegnome = False
Export('usegnome')

if ARGUMENTS.get('hacks', 1) not in [0, '0']:
    hacks = True
else:
    hacks = False
Export('hacks')

project_env.MergeFlags('-Wall -Wno-unused-function -Werror')

def pkgconfig(env, package):
    #this is annoying; because ParseConfig doesn't like arguments that contain spaces, I have to do this manually
    pc = subprocess.Popen(['pkg-config', package, '--cflags', '--libs'], stdout=subprocess.PIPE)
    flags = pc.stdout.read()
    if pc.wait() == 0: #pkg-config exited successfully
        env.MergeFlags(flags)
    else: #pkg-config failed and printed an error message
        Exit(1)
Export('pkgconfig')

Mkdir('include')
project_include = Dir('include') #directory for this project's headers
project_env.Prepend(CPPPATH=project_include)
Export('project_include')

#bin module
bin_files, bin_config = SConscript('bin/SConscript')
Export('bin_files bin_config')

#Layout module
layout_files, layout_config = SConscript('layout/SConscript')
Export('layout_files layout_config')

#X Window Management module
display_files, display_config = SConscript('xwm/SConscript')
Export('display_files display_config')

if debug:
    #crashcatcher
    cc_files, cc_config = SConscript('crashcatcher/SConscript')
    Export('cc_files cc_config')

#Core module
core_files, core_config = SConscript('core/SConscript')
Export('core_files core_config')

#loader program
main_program = SConscript('loader/SConscript')
main_program = project_env.Install('.', main_program)
Default(main_program)

if ARGUMENTS.get('test', 1) not in [0, '0']:
    #test programs
    SConscript('layout/test/SConscript')
    SConscript('xwm/test/SConscript')

