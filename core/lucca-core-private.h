/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_CORE_PRIVATE_H
#define LUCCA_CORE_PRIVATE_H

#include "lucca-core.h"

struct _LuccaApplication {
    GObject parent;
    /* instance members */
    LuccaDisplay* display;
    guint bordersize;
    
    GSList* workareas;
    
    gboolean starting;
    
    gulong newscreen_connect_id;
    gulong newwindow_connect_id;
    
    gboolean dispose_has_run;
};

struct _LuccaApplicationClass {
    GObjectClass parent;
    /* class members */
    gint cancel_preview_signal_id;
};

struct _LuccaWorkArea {
    GObject parent;
    /* instance members */
    LuccaApplication* app;
    LuccaDisplay* display;
    LuccaScreen* screen;
    LuccaLayout* layout;
    LuccaWindow* window;
    GtkWidget* eventbox;

    GSList* tiles;

    gulong newtile_connect_id;
    
    gboolean is_resizing;
    gint resize_origin_x;
    gint resize_origin_y;
    GList* all_tile_candidates;
    GSList* top_tile_candidates;
    GSList* bottom_tile_candidates;
    GSList* left_tile_candidates;
    GSList* right_tile_candidates;
    LuccaGraphicalTile* resizing_tile;
    gint resizing_tile_newx;
    gint resizing_tile_newy;
    gint resizing_tile_newwidth;
    gint resizing_tile_newheight;
    
    gboolean kb_resizing;
    LuccaGraphicalTile* kb_resize_tile;
    gint kb_resize_dx, kb_resize_dy;
    gint kb_size_amount;
    gboolean kb_keydown;
    
    LuccaWindow* kb_instruction_window;
    gint kb_instruction_x, kb_instruction_y, kb_instruction_width, kb_instruction_height;
    
    gboolean dragging;
    LuccaGraphicalTile* drag_tile;
    LuccaWindow* drag_window;
    GtkWidget* drag_titlebar_preview;
    
    gboolean placing_tile;
    LuccaWindow* place_window;
    LuccaWindow* placement_instruction_window;
    GtkWidget* placement_instruction_frame;
    GtkWidget* placement_instruction_label;
    LuccaWindow* placement_preview_bar;
    
    /* virtual cursor: this is used to find the "current" location when using keyboard commands */
    gint virtual_x, virtual_y;
    
    gboolean dispose_has_run;
};

struct _LuccaWorkAreaClass {
    GObjectClass parent;
    /* class members */
};

struct _LuccaGraphicalTile {
    GObject parent;
    /* instance members */
    LuccaWorkArea* workarea;
    LuccaLayoutTile* tile;
    LuccaWindow* window;
    
    LuccaWindow* visible_window;
    GSList* window_infos; /* ordered from left to right tabs */
    GSList* window_infos_recent; /* ordered with most-recently-focused windows first */
    
    gint clientx, clienty, clientwidth, clientheight;
    
    GtkWidget* eventbox;
    GtkWidget* toplevel_vbox;
    GtkWidget* titlebar_hbox;
    GtkWidget* titlebar_separator;
    GtkWidget* clientspace_widget;
    
    GtkTooltips* tooltips;
    
    gboolean maybe_dragging;
    gint drag_origin_x, drag_origin_y;
    
    gboolean is_previewing;
    LuccaWindow* preview_parent;
    
    gboolean deleting;
    
    gboolean dispose_has_run;
};

struct _LuccaGraphicalTileClass {
    GObjectClass parent;
    /* class members */
};

/* private methods */

/* lucca_workarea_get_graphicaltile: returns the LuccaGraphicalTile created for a given LuccaLayoutTile */
LuccaGraphicalTile* lucca_workarea_get_graphicaltile(LuccaWorkArea* workarea, LuccaLayoutTile* layouttile);

/* lucca_workarea_take_cursor: moves the virtual cursor so that it is within a LuccaGraphicalTile */
void lucca_workarea_take_cursor(LuccaWorkArea* workarea, LuccaGraphicalTile* tile);

/* lucca_workarea_start_drag: handle dragging a titlebar from the given tile */
void lucca_workarea_start_drag(LuccaWorkArea* workarea, LuccaGraphicalTile* tile, LuccaWindow* window, guint32 timestamp);

/* lucca_workarea_start_addtile: allow the user to click to place a new tile for the window */
void lucca_workarea_start_addtile(LuccaWorkArea* workarea, LuccaWindow* window);

/* lucca_graphicaltile_resize_preview: causes a tile to temporarily display its contents with the given geometry */
void lucca_graphicaltile_resize_preview(LuccaGraphicalTile* tile, gint x, gint y, gint width, gint height);

/* lucca_graphicaltile_cancel_preview: causes a tile to revert to its normal geometry */
void lucca_graphicaltile_cancel_preview(LuccaGraphicalTile* tile);

/* lucca_graphicaltile_focus: gives a tile's window the focus and moves the virtual cursor into the tile */
void lucca_graphicaltile_focus(LuccaGraphicalTile* tile);

/* lucca_graphicaltile_cycle: move through the tab bar forward or backwards */
void lucca_graphicaltile_cycle(LuccaGraphicalTile* tile, gboolean forward);

/* lucca_graphicaltile_split: split in the given direction, putting the given window in the new tile */
void lucca_graphicaltile_split(LuccaGraphicalTile* tile, LuccaWindow* window, LuccaSide side);

#endif

