/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-core-private.h"

/* Implementation of LuccaApplication */

static GObjectClass* parent_class = NULL;

/* event listener declarations */
static void on_newscreen(LuccaDisplay* display, LuccaScreen* screen, gpointer user_data);
static void on_newwindow(LuccaDisplay* display, LuccaWindow* window, gpointer user_data);

/* property id's */
enum {
    PROP_DISPLAY=1,
    PROP_BORDERSIZE,
};

static void lucca_application_init(GTypeInstance* instance, gpointer g_class) {
    LuccaApplication* application = LUCCA_APPLICATION(instance);

    application->dispose_has_run = FALSE;

    application->display = NULL;
    application->workareas = NULL;
    
    application->bordersize = 5;
    
    application->starting = TRUE;
    
    application->newscreen_connect_id = 0;
}

static void lucca_application_dispose(GObject* obj) {
    LuccaApplication* application = LUCCA_APPLICATION(obj);
    GSList* item;
    
    if (application->dispose_has_run) {
        return;
    }
    application->dispose_has_run = TRUE;
    
    if (application->newscreen_connect_id) {
        g_signal_handler_disconnect(application->display, application->newscreen_connect_id);
    }
    
    if (application->newwindow_connect_id) {
        g_signal_handler_disconnect(application->display, application->newwindow_connect_id);
    }
    
    g_object_unref(application->display);
    
    for (item=application->workareas; item!=NULL; item=item->next) {
        g_object_unref(item->data);
    }
    g_slist_free(application->workareas);
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

static void lucca_application_set_property(GObject* object, guint property_id, const GValue* value, GParamSpec* pspec) {
    LuccaApplication* application = LUCCA_APPLICATION(object);
    
    switch (property_id) {
    case PROP_DISPLAY:
        application->display = g_object_ref(g_value_get_object(value));
        
        application->newscreen_connect_id = g_signal_connect(application->display, "new-screen", G_CALLBACK(on_newscreen), application);
        application->newwindow_connect_id = g_signal_connect(application->display, "new-window", G_CALLBACK(on_newwindow), application);
        break;
    case PROP_BORDERSIZE:
        application->bordersize = g_value_get_uint(value);
        /* FIXME: emit a notify event so the LuccaScreen can adjust this */
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_application_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaApplication* application = LUCCA_APPLICATION(object);
    
    switch (property_id) {
    case PROP_DISPLAY:
        g_value_set_object(value, application->display);
        break;
    case PROP_BORDERSIZE:
        g_value_set_uint(value, application->bordersize);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_application_class_init(gpointer g_class, gpointer class_data) {
    LuccaApplicationClass* klass = (LuccaApplicationClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_application_dispose;
    gobclass->set_property = lucca_application_set_property;
    gobclass->get_property = lucca_application_get_property;
    
    /* properties */
    pspec = g_param_spec_object(
        "display",
        "display",
        "The LuccaDisplay this object is linked to",
        LUCCA_TYPE_DISPLAY,
        G_PARAM_READABLE|G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property(gobclass, PROP_DISPLAY, pspec);
    
    pspec = g_param_spec_uint(
        "border-size",
        "border size",
        "The size of the borders between tiles",
        0, /* minimum value */
        G_MAXUINT, /* maximum value */
        0, /* default value */
        G_PARAM_READABLE|G_PARAM_WRITABLE);
    g_object_class_install_property(gobclass, PROP_BORDERSIZE, pspec);
    
    /* signals */
    klass->cancel_preview_signal_id = g_signal_new(
        "cancel-preview",
        G_TYPE_FROM_CLASS(g_class),
        G_SIGNAL_RUN_LAST,
        0, /* class_offset */
        NULL, /* accumulator */
        NULL, /* accu_data */
        g_cclosure_marshal_VOID__VOID,
        G_TYPE_NONE,
        0 /* n_params */
    );

    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_application_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaApplicationClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_application_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaApplication),
            0, /* n_preallocs */
            lucca_application_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaApplicationType", &info, 0);
    }
    return type;
}

gboolean _finished_startup(gpointer data) {
    LuccaApplication* application = LUCCA_APPLICATION(data);
    
    application->starting = FALSE;
    
    return FALSE;
}

/* event listeners */
static void on_newscreen(LuccaDisplay* display, LuccaScreen* screen, gpointer user_data) {
    LuccaWorkArea* workarea;
    LuccaApplication* application;
    
    application = LUCCA_APPLICATION(user_data);
    
    g_idle_add(_finished_startup, application);
    
    workarea = g_object_new(LUCCA_TYPE_WORKAREA,
                                "application", application,
                                "screen", screen,
                                NULL);
    
    application->workareas = g_slist_prepend(application->workareas, workarea);
}

static void on_newwindow(LuccaDisplay* display, LuccaWindow* window, gpointer user_data) {
    LuccaApplication* application = LUCCA_APPLICATION(user_data);
    LuccaWorkArea* workarea = LUCCA_WORKAREA(application->workareas->data);
    LuccaGraphicalTile* tile=NULL;
    LuccaType type;
    
    /* FIXME: Because z co-ordinates are not implemented, the stacking order depends on the order in which windows were mapped. Existing full screen windows would have been mapped before the workarea window. Therefore, the workarea window would be on top of any existing fullscreen window that we do not hide or reparent. As a temporary work-around, we unfullscreen all existing windows when starting up. */
    if (application->starting)
        lucca_window_set_states(window, lucca_window_get_states(window)&~LUCCA_STATE_FULLSCREEN);
    
    g_object_get(window, "type", &type, NULL); /* FIXME: is this safe for enum types? */
    
    /* if it's a docked window, try to dock it */
    
    if (type == LUCCA_TYPE_DOCK && lucca_screen_dock_window(lucca_display_get_default_screen(display), window)) {
        return;
    }
    
    /* it's a normal window; put it at the virtual cursor, show it, and focus it */
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile == NULL)
        tile = LUCCA_GRAPHICALTILE(workarea->tiles->data);
    
    lucca_graphicaltile_add_window(LUCCA_GRAPHICALTILE(tile), window);
    lucca_graphicaltile_set_visible_window(LUCCA_GRAPHICALTILE(tile), window);
    lucca_graphicaltile_focus(LUCCA_GRAPHICALTILE(tile));
}

