/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-core-private.h"

#include <stdlib.h>

/* Implementation of LuccaGraphicalTile */

static GObjectClass* parent_class = NULL;

/* struct used to store a managed LuccaWindow and its titlebar widgets */
typedef struct {
    LuccaWindow* window;
    LuccaGraphicalTile* tile;
    
    GtkWidget* eventbox;
    GtkWidget* hbox;
    GtkWidget* title_label;
    GtkWidget* ins_button;
    GtkWidget* close_button;
    
    gulong gain_focus_handler_id;
    gulong lose_focus_handler_id;
    gulong mouse_in_handler_id;
    gulong titlechange_handler_id;
    gulong staterequest_handler_id;
} WindowInfo;

/* FIXME: make it possible to translate these */
const char* no_window_msg =
    "(no window)";

const char* fullscreen_window_msg =
    "'%s' is in fullscreen mode.\n"
    "\n"
    "Click on the titlebar to activate it.";

/* event listener declarations */
static void on_clientarea_resize(GtkWidget* widget, GtkAllocation* allocation, gpointer user_data);

static gboolean on_titlebar_buttondown(GtkWidget* widget, GdkEventButton* event, gpointer user_data);

static gboolean on_titlebar_motion(GtkWidget* widget, GdkEventMotion* event, gpointer user_data);

static void on_ins_click(GtkButton* button, gpointer user_data);

static void on_close_click(GtkButton* button, gpointer user_data);

static void on_tile_configure(LuccaLayoutTile* tile, gpointer user_data);

static void on_tile_delete(LuccaLayoutTile* tile, gpointer user_data);

static void on_lost_ownership(LuccaWindow* window, gpointer user_data);

static void on_gain_focus(LuccaWindow* window, gpointer user_data);

static void on_lose_focus(LuccaWindow* window, gpointer user_data);

static void on_mouse_in(LuccaWindow* window, gpointer user_data);

static void on_titlechange(LuccaWindow* window, GParamSpec* arg1, gpointer user_data);

static gboolean on_previewparent_expose(GtkWidget* widget, gpointer user_data);

static void on_staterequest(LuccaWindow* window, LuccaState states, gpointer user_data);

/* property id's */
enum {
    PROP_WORKAREA=1,
    PROP_TILE,
    PROP_APPLICATION
};

static void lucca_graphicaltile_init(GTypeInstance* instance, gpointer g_class) {
    LuccaGraphicalTile* tile = LUCCA_GRAPHICALTILE(instance);

    tile->dispose_has_run = FALSE;
    
    tile->workarea = NULL;
    tile->tile = NULL;
    tile->window = NULL;
    
    tile->is_previewing = FALSE;
    tile->preview_parent = NULL;
    
    tile->deleting = FALSE;
    
    tile->maybe_dragging = FALSE;
    
    tile->tooltips = NULL;
    
    tile->visible_window = NULL;
    tile->window_infos = NULL;
    tile->window_infos_recent = NULL;
}

static void lucca_graphicaltile_dispose(GObject* obj) {
    LuccaGraphicalTile* tile = LUCCA_GRAPHICALTILE(obj);
    GSList* item;
    
    if (tile->dispose_has_run) {
        return;
    }
    tile->dispose_has_run = TRUE;
    
    if (tile->tile)
        g_object_unref(tile->tile);
    
    if (tile->window)
        g_object_unref(tile->window);
    
    if (tile->preview_parent)
        g_object_unref(tile->preview_parent);
    
    if (tile->tooltips)
        g_object_unref(tile->tooltips);
    
    for (item=tile->window_infos; item!=NULL; item=item->next) {
        WindowInfo* info = (WindowInfo*)item->data;
        
        g_signal_handler_disconnect(info->window, info->gain_focus_handler_id);
        g_signal_handler_disconnect(info->window, info->lose_focus_handler_id);
        g_signal_handler_disconnect(info->window, info->mouse_in_handler_id);
        g_signal_handler_disconnect(info->window, info->titlechange_handler_id);
        g_signal_handler_disconnect(info->window, info->staterequest_handler_id);

        g_object_unref(info->window);
        
        g_slice_free(WindowInfo, item->data);
    }
    g_slist_free(tile->window_infos);
    g_slist_free(tile->window_infos_recent);
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

/* lucca_graphicaltile_refresh_states: sets the states of windows in a LuccaGraphicalTile */
static void lucca_graphicaltile_refresh_states(LuccaGraphicalTile* tile) {
    gint x, y, width, height, workarea_width, workarea_height;
    LuccaState tile_states=0;
    GSList* item;

    g_object_get(tile->tile,
        "x", &x,
        "y", &y,
        "width", &width,
        "height", &height,
        NULL);

    g_object_get(tile->workarea->layout,
        "width", &workarea_width,
        "height", &workarea_height,
        NULL);

    if (x == 0 && width == workarea_width)
        tile_states |= LUCCA_STATE_MAXIMIZED_HORZ;

    if (y == 0 && height == workarea_height)
        tile_states |= LUCCA_STATE_MAXIMIZED_VIRT;
    
    for (item = tile->window_infos; item != NULL; item = item->next) {
        WindowInfo* info = (WindowInfo*)item->data;
        LuccaState window_states = tile_states;
        
        if (lucca_window_get_states(info->window)&LUCCA_STATE_FULLSCREEN) {
            gboolean focused;
            
            window_states |= LUCCA_STATE_FULLSCREEN;
            
            g_object_get(info->window, "focused", &focused, NULL);
            
            if (!focused) {
                window_states |= LUCCA_STATE_HIDDEN;
            }
        }
        
        if (info->window != tile->visible_window)
            window_states |= LUCCA_STATE_HIDDEN;
        
        lucca_window_set_states(info->window, window_states);
    }
}

/* lucca_graphicaltile_refresh_position: sets a LuccaGraphicalTile's size and position to match its LuccaLayoutTile */
static void lucca_graphicaltile_refresh_position(LuccaGraphicalTile* tile) {
    gint x, y, width, height;
    GtkRequisition requisition;

    if (tile->is_previewing)
        return;
    
    lucca_graphicaltile_refresh_states(tile);
    
    g_object_get(tile->tile,
        "x", &x,
        "y", &y,
        "width", &width,
        "height", &height,
        NULL);

    gtk_widget_size_request(tile->toplevel_vbox, &requisition);

    lucca_window_configure(tile->window, GTK_WIDGET(tile->workarea->eventbox)->window, x, y, width, height, TRUE);
}

/* lucca_graphicaltile_construct: called after all construction properties have been set */
static void lucca_graphicaltile_construct(LuccaGraphicalTile* tile) {
    GtkWindow* tile_gtkwindow;
    GdkCursor* cursor;
    
    tile->window = g_object_ref(lucca_display_create_internal(tile->workarea->display));
    
    g_object_get(tile->window, "window", &tile_gtkwindow, NULL);

    tile->eventbox = gtk_event_box_new();
    tile->toplevel_vbox = gtk_vbox_new(FALSE, 0);
    tile->titlebar_hbox = gtk_hbox_new(TRUE, 0);
    tile->titlebar_separator = gtk_hseparator_new();
    tile->clientspace_widget = gtk_label_new(no_window_msg);
    
    gtk_container_add(GTK_CONTAINER(tile_gtkwindow), tile->eventbox);
    gtk_container_add(GTK_CONTAINER(tile->eventbox), tile->toplevel_vbox);
    
    gtk_box_pack_start(GTK_BOX(tile->toplevel_vbox), 
                        tile->titlebar_hbox,
                        FALSE, /* expand */
                        FALSE, /* fill */
                        0 /* padding */
                        );
    
    gtk_box_pack_start(GTK_BOX(tile->toplevel_vbox), 
                        tile->titlebar_separator,
                        FALSE, /* expand */
                        FALSE, /* fill */
                        0 /* padding */
                        );
    
    gtk_box_pack_end(GTK_BOX(tile->toplevel_vbox), 
                        tile->clientspace_widget,
                        TRUE, /* expand */
                        TRUE, /* fill */
                        0 /* padding */
                        );

    gtk_widget_show(tile->eventbox);
    gtk_widget_show(tile->toplevel_vbox);
    gtk_widget_show(tile->titlebar_hbox);
    gtk_widget_show(tile->titlebar_separator);
    gtk_widget_show(tile->clientspace_widget);
    
    cursor = gdk_cursor_new(GDK_LEFT_PTR);
    
    gdk_window_set_cursor(GTK_WIDGET(tile_gtkwindow)->window, cursor);
    
    gdk_cursor_unref(cursor);
    
    lucca_graphicaltile_refresh_position(tile);
    
    g_signal_connect_after(tile->clientspace_widget, "size-allocate", G_CALLBACK(on_clientarea_resize), tile);

    g_signal_connect(tile->tile, "configure", G_CALLBACK(on_tile_configure), tile);
    g_signal_connect(tile->tile, "delete", G_CALLBACK(on_tile_delete), tile);

    gtk_widget_add_events(tile->eventbox, GDK_EXPOSURE_MASK);

    tile->tooltips = gtk_tooltips_new();
    g_object_ref_sink(tile->tooltips);

    g_object_unref(tile_gtkwindow);
}

static void lucca_graphicaltile_set_property(GObject* object, guint property_id, const GValue* value, GParamSpec* pspec) {
    LuccaGraphicalTile* tile = LUCCA_GRAPHICALTILE(object);
    
    switch (property_id) {
    case PROP_WORKAREA:
        tile->workarea = g_object_ref(g_value_get_object(value));
        if (tile->tile != NULL)
            lucca_graphicaltile_construct(tile);
        break;
    case PROP_TILE:
        tile->tile = g_object_ref(g_value_get_object(value));
        if (tile->workarea != NULL)
            lucca_graphicaltile_construct(tile);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_graphicaltile_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaGraphicalTile* tile = LUCCA_GRAPHICALTILE(object);
    
    switch (property_id) {
    case PROP_WORKAREA:
        g_value_set_object(value, tile->workarea);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_graphicaltile_class_init(gpointer g_class, gpointer class_data) {
    LuccaGraphicalTileClass* klass = (LuccaGraphicalTileClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_graphicaltile_dispose;
    gobclass->set_property = lucca_graphicaltile_set_property;
    gobclass->get_property = lucca_graphicaltile_get_property;
    
    /* properties */
    pspec = g_param_spec_object(
        "workarea",
        "workarea",
        "The LuccaWorkArea that created this object",
        LUCCA_TYPE_WORKAREA,
        G_PARAM_READABLE|G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property(gobclass, PROP_WORKAREA, pspec);
    
    pspec = g_param_spec_object(
        "tile",
        "layout tile",
        "The LuccaLayoutTile that controls this object's size and position",
        LUCCA_TYPE_LAYOUTTILE,
        G_PARAM_READABLE|G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property(gobclass, PROP_TILE, pspec);
    
    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_graphicaltile_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaGraphicalTileClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_graphicaltile_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaGraphicalTile),
            0, /* n_preallocs */
            lucca_graphicaltile_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaGraphicalTileType", &info, 0);
    }
    return type;
}

/* Event listeners */

/* called when the label representing client area is resized */
static void on_clientarea_resize(GtkWidget* widget, GtkAllocation* allocation, gpointer user_data) {
    LuccaGraphicalTile* tile = LUCCA_GRAPHICALTILE(user_data);
    GtkWindow* tile_gtkwindow;
    GSList* item;

    g_object_get(tile->window, "window", &tile_gtkwindow, NULL);
    
    for (item=tile->window_infos; item!=NULL; item=item->next) {
        WindowInfo* info = (WindowInfo*)item->data;
        
        if (!(lucca_window_get_states(info->window)&LUCCA_STATE_FULLSCREEN)) {
            lucca_window_configure(info->window,
                                    GTK_WIDGET(tile_gtkwindow)->window,
                                    allocation->x, allocation->y,
                                    allocation->width, allocation->height,
                                    info->window == tile->visible_window);
        }
    }
    
    g_object_unref(tile_gtkwindow);
}

/* called when a mouse button is pressed on the titlebar */
static gboolean on_titlebar_buttondown(GtkWidget* widget, GdkEventButton* event, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    
    lucca_graphicaltile_set_visible_window(info->tile, info->window);
    
    lucca_graphicaltile_focus(info->tile);
    
    info->tile->maybe_dragging = TRUE;
    info->tile->drag_origin_x = (gint)event->x;
    info->tile->drag_origin_y = (gint)event->y;
    
    return FALSE;
}

/* called when the mouse moves when a button is pressed on the titlebar */
static gboolean on_titlebar_motion(GtkWidget* widget, GdkEventMotion* event, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    
    if (info->tile->maybe_dragging && event->state & (GDK_BUTTON1_MASK|GDK_BUTTON2_MASK|GDK_BUTTON3_MASK|GDK_BUTTON4_MASK|GDK_BUTTON5_MASK)) {
        gint drag_threshold;
        GtkSettings* settings;
        
        /* did the mouse move far enough for this to be considered a "drag"? */
        settings = gtk_widget_get_settings(info->eventbox);
        g_object_get(settings, "gtk-dnd-drag-threshold", &drag_threshold, NULL);
        
        if (abs((gint)event->x - info->tile->drag_origin_x) >= drag_threshold || abs((gint)event->y - info->tile->drag_origin_y) >= drag_threshold) {
            lucca_workarea_start_drag(info->tile->workarea, info->tile, info->window, event->time);
            info->tile->maybe_dragging = FALSE;
        }
    }
    
    return FALSE;
}

/* called when the "new tile" button is clicked */
static void on_ins_click(GtkButton* button, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    
    lucca_workarea_start_addtile(info->tile->workarea, info->window);
}

/* called when the close button is clicked */
static void on_close_click(GtkButton* button, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    
    lucca_window_close(info->window);
}

/* called when our LayoutTile is resized */
static void on_tile_configure(LuccaLayoutTile* tile, gpointer user_data) {
    LuccaGraphicalTile* graphicaltile = LUCCA_GRAPHICALTILE(user_data);
    
    lucca_graphicaltile_cancel_preview(graphicaltile);

    lucca_graphicaltile_refresh_position(graphicaltile);
    
    if (lucca_layout_get_tile_at_position(graphicaltile->workarea->layout, graphicaltile->workarea->virtual_x, graphicaltile->workarea->virtual_y) == tile) {
        lucca_graphicaltile_focus(graphicaltile);
    }
}

/* called when our LayoutTile is deleted */
static void on_tile_delete(LuccaLayoutTile* tile, gpointer user_data) {
    LuccaGraphicalTile* graphicaltile = LUCCA_GRAPHICALTILE(user_data);
    LuccaGraphicalTile* refuge_tile;
    guint x, y;
    GSList* item;
    
    graphicaltile->deleting = TRUE; /* this makes us not bother trying to delete windows from this tile as they're being moved to another tile; our window is about to go away anyway and we should be collected soon so it doesn't matter */
    
    /* figure out where our windows should go; if we were deleted, it's because another tile is now using the space, so we look for the tile there */
    g_object_get(tile, "x", &x, "y", &y, NULL);
    refuge_tile = lucca_workarea_get_tile_at_position(graphicaltile->workarea, x, y);
    
    /* now move our windows to someplace safe; we can iterate the list safely because deleting==TRUE means items won't be deleted as we go */
    for (item=graphicaltile->window_infos; item!=NULL; item=item->next){
        WindowInfo* info = (WindowInfo*)item->data;
        
        lucca_graphicaltile_add_window(refuge_tile, info->window);
    }
    
    /* hide this tile's window */
    lucca_window_withdraw(graphicaltile->window);
    
    /* remove this tile's entry from the workarea's list */
    graphicaltile->workarea->tiles = g_slist_remove(graphicaltile->workarea->tiles, graphicaltile);
    g_object_unref(graphicaltile);
}

void _update_clientspace_msg(LuccaGraphicalTile* tile) {
    if (tile->visible_window != NULL && lucca_window_get_states(tile->visible_window)&LUCCA_STATE_FULLSCREEN) {
        gchar* title;
        gchar* msg;
        
        g_object_get(tile->visible_window, "requested-title", &title, NULL);
        
        msg = g_strdup_printf(fullscreen_window_msg, title);
        
        gtk_label_set_text(GTK_LABEL(tile->clientspace_widget), msg);
        
        g_free(title);
        g_free(msg);
    }
    else {
        gtk_label_set_text(GTK_LABEL(tile->clientspace_widget), no_window_msg);
    }
}

/* called when another object takes over management of a window */
static void on_lost_ownership(LuccaWindow* window, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    
    if (info->tile->deleting)
        return;
    
    /* delete the window's titlebar */
    gtk_container_remove(GTK_CONTAINER(info->tile->titlebar_hbox), info->eventbox);
    
    /* remove its info structure from the lists */
    info->tile->window_infos = g_slist_remove(info->tile->window_infos, info);
    info->tile->window_infos_recent = g_slist_remove(info->tile->window_infos_recent, info);
    
    /* if this was the visible window, choose another one */
    if (info->window == info->tile->visible_window) {
        if (info->tile->window_infos_recent != NULL) {
            gint x, y, width, height;
            
            lucca_graphicaltile_set_visible_window(info->tile, ((WindowInfo*)info->tile->window_infos_recent->data)->window);
            
            /* if this tile has the virtual cursor (focus was here last), focus the new visible window */
            g_object_get(info->tile->tile, "x", &x, "y", &y, "width", &width, "height", &height, NULL);
            
            if (info->tile->workarea->virtual_x >= x && info->tile->workarea->virtual_x < x + width &&
                info->tile->workarea->virtual_y >= y && info->tile->workarea->virtual_y < y + height) {
                lucca_window_give_focus(info->tile->visible_window);
            }
        }
        else {
            info->tile->visible_window = NULL;
            
            _update_clientspace_msg(info->tile);
        }
    }

    /* free resources associated with owning this window */
    g_signal_handler_disconnect(info->window, info->gain_focus_handler_id);
    g_signal_handler_disconnect(info->window, info->lose_focus_handler_id);
    g_signal_handler_disconnect(info->window, info->mouse_in_handler_id);
    g_signal_handler_disconnect(info->window, info->titlechange_handler_id);
    g_signal_handler_disconnect(info->window, info->staterequest_handler_id);
    g_object_unref(info->window);
    
    g_slice_free(WindowInfo, info);
}

static void _refresh_colors(WindowInfo* info) {
    gboolean focused;
    
    g_object_get(info->window, "focused", &focused, NULL);
    
    if (focused) {
        gtk_widget_modify_base(info->eventbox, GTK_STATE_NORMAL, &(info->eventbox->style->bg[GTK_STATE_SELECTED]));
        gtk_widget_modify_bg(info->eventbox, GTK_STATE_NORMAL, &(info->eventbox->style->bg[GTK_STATE_SELECTED]));
        
        gtk_widget_modify_base(info->hbox, GTK_STATE_NORMAL, &(info->hbox->style->bg[GTK_STATE_SELECTED]));
        gtk_widget_modify_bg(info->hbox, GTK_STATE_NORMAL, &(info->hbox->style->bg[GTK_STATE_SELECTED]));
        
        gtk_widget_modify_bg(info->title_label, GTK_STATE_NORMAL, &(info->title_label->style->bg[GTK_STATE_SELECTED]));
        gtk_widget_modify_fg(info->title_label, GTK_STATE_NORMAL, &(info->title_label->style->fg[GTK_STATE_SELECTED]));
    }
    else if (info->window == info->tile->visible_window) {
        /* I can't rely on PRELIGHT or ACTIVE to look sufficiently different from NORMAL so I go with 25% SELECTED, 75% NORMAL */
        GdkColor background = {0};
        
        gtk_widget_modify_base(info->eventbox, GTK_STATE_NORMAL, NULL);
        gtk_widget_modify_bg(info->eventbox, GTK_STATE_NORMAL, NULL);
        
        background.red = info->eventbox->style->bg[GTK_STATE_SELECTED].red/4 + info->eventbox->style->bg[GTK_STATE_NORMAL].red/4*3;
        background.green = info->eventbox->style->bg[GTK_STATE_SELECTED].green/4 + info->eventbox->style->bg[GTK_STATE_NORMAL].green/4*3;
        background.blue = info->eventbox->style->bg[GTK_STATE_SELECTED].blue/4 + info->eventbox->style->bg[GTK_STATE_NORMAL].blue/4*3;

        gdk_colormap_alloc_color(gtk_widget_get_colormap(info->eventbox), &background, FALSE, TRUE);
        
        gtk_widget_modify_base(info->eventbox, GTK_STATE_NORMAL, &background);
        gtk_widget_modify_bg(info->eventbox, GTK_STATE_NORMAL, &background);
        
        gtk_widget_modify_base(info->hbox, GTK_STATE_NORMAL, &background);
        gtk_widget_modify_bg(info->hbox, GTK_STATE_NORMAL, &background);
        
        gtk_widget_modify_bg(info->title_label, GTK_STATE_NORMAL, &background);
        gtk_widget_modify_fg(info->title_label, GTK_STATE_NORMAL, NULL);
    }
    else {
        gtk_widget_modify_base(info->eventbox, GTK_STATE_NORMAL, NULL);
        gtk_widget_modify_bg(info->eventbox, GTK_STATE_NORMAL, NULL);
        
        gtk_widget_modify_base(info->hbox, GTK_STATE_NORMAL, NULL);
        gtk_widget_modify_bg(info->hbox, GTK_STATE_NORMAL, NULL);
        
        gtk_widget_modify_bg(info->title_label, GTK_STATE_NORMAL, NULL);
        gtk_widget_modify_fg(info->title_label, GTK_STATE_NORMAL, NULL);
    }
}

static void on_gain_focus(LuccaWindow* window, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    
    _refresh_colors(info);

    /* mark this window as the most recently-focused window */
    info->tile->window_infos_recent = g_slist_prepend(g_slist_remove(info->tile->window_infos_recent, info), info);
    
    lucca_workarea_take_cursor(info->tile->workarea, info->tile);
    
    if (lucca_window_get_states(info->window)&LUCCA_STATE_FULLSCREEN) {
        gint screen_width, screen_height;
        
        g_object_get(info->tile->workarea->screen, "width", &screen_width, "height", &screen_height, NULL);
        
        lucca_window_configure(info->window, lucca_screen_get_root(info->tile->workarea->screen), 0, 0, screen_width, screen_height, TRUE);
    }
    
    lucca_graphicaltile_refresh_states(info->tile);
}

static void on_lose_focus(LuccaWindow* window, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;

    _refresh_colors(info);

    if (lucca_window_get_states(info->window)&LUCCA_STATE_FULLSCREEN) {
        gint screen_width, screen_height;
        
        g_object_get(info->tile->workarea->screen, "width", &screen_width, "height", &screen_height, NULL);
        
        lucca_window_configure(info->window, lucca_screen_get_root(info->tile->workarea->screen), 0, 0, screen_width, screen_height, FALSE);
    }
    
    lucca_graphicaltile_refresh_states(info->tile);
}

static void on_mouse_in(LuccaWindow* window, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;

    lucca_graphicaltile_focus(info->tile);
}

static void on_titlechange(LuccaWindow* window, GParamSpec* arg1, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;
    gchar* title;
    
    g_object_get(window, "requested-title", &title, NULL);
    
    gtk_label_set_text(GTK_LABEL(info->title_label), title);
    
    gtk_tooltips_set_tip(info->tile->tooltips, info->title_label, title, NULL);
    
    g_free(title);
}

static gboolean on_previewparent_expose(GtkWidget* widget, gpointer user_data) {
    /* LuccaGraphicalTile* tile = LUCCA_GRAPHICALTILE(user_data); */
    
    gdk_draw_rectangle(widget->window, widget->style->fg_gc[GTK_STATE_SELECTED], TRUE, 0, 0, widget->allocation.width, widget->allocation.height);
    
    return TRUE;
}

/* called when the window wants to change its state */
static void on_staterequest(LuccaWindow* window, LuccaState states, gpointer user_data) {
    WindowInfo* info = (WindowInfo*)user_data;

    if (states&LUCCA_STATE_FULLSCREEN && !(lucca_window_get_states(window)&LUCCA_STATE_FULLSCREEN)) {
        gint screen_width, screen_height;
        gboolean focused;
        
        g_object_get(window, "focused", &focused, NULL);
        
        lucca_window_set_states(window, lucca_window_get_states(window)|LUCCA_STATE_FULLSCREEN);
        
        lucca_graphicaltile_refresh_states(info->tile);

        g_object_get(info->tile->workarea->screen, "width", &screen_width, "height", &screen_height, NULL);
        
        lucca_window_configure(info->window, lucca_screen_get_root(info->tile->workarea->screen), 0, 0, screen_width, screen_height, focused);
    }
    else if (lucca_window_get_states(window)&LUCCA_STATE_FULLSCREEN && !(states&LUCCA_STATE_FULLSCREEN)) {
        GtkWindow* tile_gtkwindow;
        GtkAllocation allocation;
    
        lucca_window_set_states(window, lucca_window_get_states(window)&~LUCCA_STATE_FULLSCREEN);

        lucca_graphicaltile_refresh_states(info->tile);
        
        g_object_get(info->tile->window, "window", &tile_gtkwindow, NULL);

        allocation = info->tile->clientspace_widget->allocation;
        
        lucca_window_configure(info->tile->visible_window, GTK_WIDGET(tile_gtkwindow)->window, allocation.x, allocation.y, allocation.width, allocation.height, TRUE);
        
        g_object_unref(tile_gtkwindow);
    }
    
    _update_clientspace_msg(info->tile);
}

/* Method definitions */

LuccaWindow* lucca_graphicaltile_get_visible_window(LuccaGraphicalTile* tile) {
    return tile->visible_window;
}

void lucca_graphicaltile_set_visible_window(LuccaGraphicalTile* tile, LuccaWindow* window) {
    GtkWindow* tile_gtkwindow;
    GtkAllocation allocation;
    gboolean focused;
    WindowInfo* info;
    GSList* item;
    LuccaWindow* prev_visible_window;
    
    if (tile->visible_window == window)
        return;

    prev_visible_window = tile->visible_window;

    for (item=tile->window_infos; item!=NULL; item=item->next) {
        info = (WindowInfo*)item->data;
        
        if (info->window == window)
            break;
    }
    
    g_object_get(window, "focused", &focused, NULL);
    
    g_object_get(tile->window, "window", &tile_gtkwindow, NULL);

    allocation = tile->clientspace_widget->allocation;
    
    if (tile->visible_window != NULL) {
        lucca_window_configure(tile->visible_window, GTK_WIDGET(tile_gtkwindow)->window, allocation.x, allocation.y, allocation.width, allocation.height, FALSE);
    }
    
    tile->visible_window = window;
    
    if (!(lucca_window_get_states(window)&LUCCA_STATE_FULLSCREEN)) {
        lucca_window_configure(tile->visible_window, GTK_WIDGET(tile_gtkwindow)->window, allocation.x, allocation.y, allocation.width, allocation.height, TRUE);
    }
    else if (focused) {
        gint screen_width, screen_height;
        
        g_object_get(tile->workarea->screen, "width", &screen_width, "height", &screen_height, NULL);
        
        lucca_window_configure(tile->visible_window, lucca_screen_get_root(tile->workarea->screen), 0, 0, screen_width, screen_height, TRUE);
    }
    
    _update_clientspace_msg(tile);

    if (focused)
        lucca_window_give_focus(window);

    lucca_graphicaltile_refresh_states(tile);
    
    _refresh_colors(info);
    
    for (item=tile->window_infos; item!=NULL; item=item->next) {
        info = (WindowInfo*)item->data;
        
        if (info->window == prev_visible_window) {
            _refresh_colors(info);
            break;
        }
    }

    g_object_unref(tile_gtkwindow);
}

void lucca_graphicaltile_add_window(LuccaGraphicalTile* tile, LuccaWindow* window) {
    WindowInfo* info;
    gchar* title;
    GClosure* ownership_callback;
    
    info = g_slice_new(WindowInfo);
    
    g_object_get(window, "requested-title", &title, NULL);
    
    info->window = g_object_ref(window);
    info->tile = tile;
    
    /* create a titlebar for this window */
    info->eventbox = gtk_event_box_new();
    info->hbox = gtk_hbox_new(FALSE, 0);
    info->title_label = gtk_label_new(title);
    info->ins_button = gtk_button_new_with_label("⍇⍈");
    info->close_button = gtk_button_new_with_label("x");
    
    gtk_button_set_relief(GTK_BUTTON(info->ins_button), GTK_RELIEF_HALF);
    gtk_button_set_relief(GTK_BUTTON(info->close_button), GTK_RELIEF_HALF);
    
    gtk_container_add(GTK_CONTAINER(info->eventbox), info->hbox);
    
    gtk_box_pack_start(GTK_BOX(info->hbox),
                        info->title_label,
                        TRUE, /* expand */
                        TRUE, /* fill */
                        0 /* padding */
                        );    gint x, y, width, height;
    
    g_object_get(tile->tile,
        "x", &x,
        "y", &y,
        "width", &width,
        "height", &height,
        NULL);
    
    gtk_box_pack_end(GTK_BOX(info->hbox),
                        info->close_button,
                        FALSE, /* expand */
                        FALSE, /* fill */
                        0 /* padding */
                        );
    
    gtk_box_pack_end(GTK_BOX(info->hbox),
                        info->ins_button,
                        FALSE, /* expand */
                        FALSE, /* fill */
                        2 /* padding */
                        );

    gtk_widget_show_all(info->eventbox);

    gtk_widget_add_events(info->eventbox, GDK_BUTTON_MOTION_MASK);
    g_signal_connect(info->eventbox, "button-press-event", G_CALLBACK(on_titlebar_buttondown), info);
    g_signal_connect(info->eventbox, "motion-notify-event", G_CALLBACK(on_titlebar_motion), info);
    g_signal_connect(info->ins_button, "clicked", G_CALLBACK(on_ins_click), info);
    g_signal_connect(info->close_button, "clicked", G_CALLBACK(on_close_click), info);
    info->gain_focus_handler_id = g_signal_connect(window, "gain-focus", G_CALLBACK(on_gain_focus), info);
    info->lose_focus_handler_id = g_signal_connect(window, "lose-focus", G_CALLBACK(on_lose_focus), info);
    info->mouse_in_handler_id = g_signal_connect(window, "mouse-in", G_CALLBACK(on_mouse_in), info);
    info->titlechange_handler_id = g_signal_connect(window, "notify::requested-title", G_CALLBACK(on_titlechange), info);
    info->staterequest_handler_id = g_signal_connect(window, "state-request", G_CALLBACK(on_staterequest), info);

    gtk_tooltips_set_tip(tile->tooltips, info->title_label, title, NULL);
    gtk_tooltips_set_tip(tile->tooltips, info->ins_button, "Split Tile", "Create a new tile by cutting a current one in half.");
    gtk_tooltips_set_tip(tile->tooltips, info->close_button, "Close", NULL);

    /* add the titlebar to the titlebar area of this tile */
    gtk_box_pack_start(GTK_BOX(tile->titlebar_hbox),
                        info->eventbox,
                        TRUE, /* expand */
                        TRUE, /* fill */
                        0 /* padding */
                        );

    /* add the window to our list */
    tile->window_infos = g_slist_append(tile->window_infos, info);
    tile->window_infos_recent = g_slist_append(tile->window_infos_recent, info);

    /* take over "ownership" of this window */
    ownership_callback = g_cclosure_new(G_CALLBACK(on_lost_ownership), info, NULL);
    g_closure_set_marshal(ownership_callback, g_cclosure_marshal_VOID__VOID);
    lucca_window_change_owner(window, ownership_callback);

    /* recalculate the titlebar size and client space, ensuring that the window won't be initially mapped with a wrong size */
    lucca_graphicaltile_refresh_position(tile);

    /* make this window visible if the tile was empty */
    if (tile->visible_window == NULL) {
        lucca_graphicaltile_set_visible_window(tile, window);
        
        /* if the virtual cursor is on this tile, focus the window */
        if (lucca_workarea_get_tile_at_position(tile->workarea, tile->workarea->virtual_x, tile->workarea->virtual_y) == tile)
            lucca_window_give_focus(window);
    }
    else {
        /* if we don't make this window visible, hide it */
        GtkWindow* tile_gtkwindow;

        g_object_get(tile->window, "window", &tile_gtkwindow, NULL);
        
        if (tile->visible_window != NULL) {
            lucca_window_configure(window,
                                    GTK_WIDGET(tile_gtkwindow)->window,
                                    tile->clientspace_widget->allocation.x, tile->clientspace_widget->allocation.y,
                                    tile->clientspace_widget->allocation.width, tile->clientspace_widget->allocation.height,
                                    FALSE);
        }
        
        g_object_unref(tile_gtkwindow);
    }
    
    lucca_graphicaltile_refresh_states(tile);

    g_free(title);
}

void lucca_graphicaltile_resize_preview(LuccaGraphicalTile* tile, gint x, gint y, gint width, gint height) {
    guint bordersize;
    GtkWindow* preview_gtkparent;
    gint workarea_x, workarea_y;
    
    if (!tile->is_previewing) {
        tile->preview_parent = lucca_display_create_internal(tile->workarea->display);
        tile->is_previewing = TRUE;
    }
    
    g_object_get(tile->workarea->layout, "border-size", &bordersize, NULL);

    g_object_get(tile->preview_parent, "window", &preview_gtkparent, NULL);
    
    g_object_get(tile->workarea->screen, "workarea-x", &workarea_x, "workarea_y", &workarea_y, NULL);

    g_signal_connect(preview_gtkparent, "expose-event", G_CALLBACK(on_previewparent_expose), tile);

    lucca_window_configure(tile->preview_parent, lucca_screen_get_root(tile->workarea->screen), x+workarea_x-bordersize, y+workarea_y-bordersize, width+bordersize*2, height+bordersize*2, TRUE);
    
    lucca_window_configure(tile->window, GTK_WIDGET(preview_gtkparent)->window, bordersize, bordersize, width, height, TRUE);
}

void lucca_graphicaltile_cancel_preview(LuccaGraphicalTile* tile) {
    if (tile->is_previewing) {
        tile->is_previewing = FALSE;
        
        lucca_graphicaltile_refresh_position(tile);
        
        lucca_window_withdraw(tile->preview_parent);
        
        g_object_unref(tile->preview_parent);
        tile->preview_parent = NULL;
    }
}

GList* lucca_graphicaltile_get_windows(LuccaGraphicalTile* tile) {
    GList* result=NULL;
    GSList* item;
    
    for (item=tile->window_infos; item!=NULL; item=item->next) {
        WindowInfo* windowinfo = (WindowInfo*)item->data;
        
        result = g_list_prepend(result, windowinfo->window);
    }
    
    return result;
}

void lucca_graphicaltile_focus(LuccaGraphicalTile* tile) {
    if (tile->visible_window != NULL) {
        if (lucca_window_get_states(tile->visible_window)&LUCCA_STATE_FULLSCREEN) {
            gint screen_width, screen_height;
            
            g_object_get(tile->workarea->screen, "width", &screen_width, "height", &screen_height, NULL);
            
            lucca_window_configure(tile->visible_window, lucca_screen_get_root(tile->workarea->screen), 0, 0, screen_width, screen_height, TRUE);
        }
   
        lucca_window_give_focus(tile->visible_window);
    }
        
    lucca_workarea_take_cursor(tile->workarea, tile);
}

void lucca_graphicaltile_cycle(LuccaGraphicalTile* tile, gboolean forward) {
    GSList* item;
    GSList* list;
    WindowInfo* info;
    
    if (tile->visible_window == NULL)
        return;
    
    if (!forward) {
        list = g_slist_reverse(g_slist_copy(tile->window_infos));
    }
    else {
        list = tile->window_infos;
    }

    for (item=list; item!=NULL; item=item->next) {
        info = (WindowInfo*)item->data;
        
        if (info->window == tile->visible_window) {
            break;
        }
    }
    
    if (item == NULL || item->next == NULL) {
        info = (WindowInfo*)list->data;
    }
    else {
        info = (WindowInfo*)item->next->data;
    }

    if (!forward) {
        g_slist_free(list);
    }
    
    lucca_graphicaltile_set_visible_window(tile, info->window);
    
    lucca_graphicaltile_focus(tile);
}

void lucca_graphicaltile_split(LuccaGraphicalTile* tile, LuccaWindow* window, LuccaSide side) {
    LuccaLayoutTile* newlayouttile;
    LuccaGraphicalTile* newgraphicaltile;
    GdkGeometry tile_geometry;
    GdkWindowHints tile_geom_mask=0;
    
    tile_geometry.min_width = 70;
    tile_geometry.min_height = 70;
    tile_geom_mask |= GDK_HINT_MIN_SIZE;
    
    newlayouttile = lucca_layouttile_split(tile->tile, side, tile_geometry, tile_geom_mask);
    
    if (newlayouttile != NULL) {
        newgraphicaltile = lucca_workarea_get_graphicaltile(tile->workarea, newlayouttile);
        
        if (window != NULL) {
            lucca_graphicaltile_add_window(newgraphicaltile, window);
        }
        
        lucca_graphicaltile_focus(newgraphicaltile);
    }
}

