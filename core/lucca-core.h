/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef LUCCA_CORE_H
#define LUCCA_CORE_H

/* Public declarations for the Core Module */

#include <glib.h>
#include <glib-object.h>
#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include "lucca-layout.h"
#include "lucca-display.h"

/* Boiler-plate definition of LuccaApplication */
#define LUCCA_TYPE_APPLICATION lucca_application_get_type()
#define LUCCA_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_APPLICATION, LuccaApplication))
#define LUCCA_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_APPLICATION, LuccaApplicationClass))
#define LUCCA_IS_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_APPLICATION))
#define LUCCA_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_APPLICATION))
#define LUCCA_APPLICATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_APPLICATION, LuccaApplicationClass))

typedef struct _LuccaApplication LuccaApplication;
typedef struct _LuccaApplicationClass LuccaApplicationClass;

GType lucca_application_get_type();

/* Boiler-plate definition of LuccaWorkArea */
#define LUCCA_TYPE_WORKAREA lucca_workarea_get_type()
#define LUCCA_WORKAREA(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_WORKAREA, LuccaWorkArea))
#define LUCCA_WORKAREA_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_WORKAREA, LuccaWorkAreaClass))
#define LUCCA_IS_WORKAREA(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_WORKAREA))
#define LUCCA_IS_WORKAREA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_WORKAREA))
#define LUCCA_WORKAREA_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_WORKAREA, LuccaWorkAreaClass))

typedef struct _LuccaWorkArea LuccaWorkArea;
typedef struct _LuccaWorkAreaClass LuccaWorkAreaClass;

GType lucca_workarea_get_type();

/* Boiler-plate definition of LuccaGraphicalTile */
#define LUCCA_TYPE_GRAPHICALTILE lucca_graphicaltile_get_type()
#define LUCCA_GRAPHICALTILE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), LUCCA_TYPE_GRAPHICALTILE, LuccaGraphicalTile))
#define LUCCA_GRAPHICALTILE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), LUCCA_TYPE_GRAPHICALTILE, LuccaGraphicalTileClass))
#define LUCCA_IS_GRAPHICALTILE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LUCCA_TYPE_GRAPHICALTILE))
#define LUCCA_IS_GRAPHICALTILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LUCCA_TYPE_GRAPHICALTILE))
#define LUCCA_GRAPHICALTILE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), LUCCA_TYPE_GRAPHICALTILE, LuccaGraphicalTileClass))

typedef struct _LuccaGraphicalTile LuccaGraphicalTile;
typedef struct _LuccaGraphicalTileClass LuccaGraphicalTileClass;

GType lucca_graphicaltile_get_type();

/* method declarations */

/* lucca_workarea_get_tiles: return a list of GraphicalTiles in this workarea */
GList* lucca_workarea_get_tiles(LuccaWorkArea* workarea);

/* lucca_workaea_get_tile_at_position: return the GraphicalTile at the given position */
LuccaGraphicalTile* lucca_workarea_get_tile_at_position(LuccaWorkArea* workarea, gint x, gint y);

/* lucca_graphicaltile_get_windows: return the LuccaWindow's in a tile */
GList* lucca_graphicaltile_get_windows(LuccaGraphicalTile* tile);

/* lucca_graphicaltile_add_window: add a window to be displayed in a graphical tile */
void lucca_graphicaltile_add_window(LuccaGraphicalTile* tile, LuccaWindow* window);

/* lucca_graphicaltile_get_visible_window: get the visible window in this tile, possibly NULL */
LuccaWindow* lucca_graphicaltile_get_visible_window(LuccaGraphicalTile* tile);

/* lucca_graphicaltile_set_visible_window: make a window in a graphical tile the visible one */
void lucca_graphicaltile_set_visible_window(LuccaGraphicalTile* tile, LuccaWindow* window);

#endif

