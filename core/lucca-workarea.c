/* Copyright (c) 2008 Vincent Povirk

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "lucca-core-private.h"

#include <gdk/gdkkeysyms.h>

/* Implementation of LuccaWorkArea */

static GObjectClass* parent_class = NULL;

/* arrow key symbols */
const guint left_keysyms[] = { GDK_Left, GDK_KP_Left, GDK_KP_4, GDK_H, GDK_h, GDK_S, GDK_s, 0 };
const guint down_keysyms[] = { GDK_Down, GDK_KP_Down, GDK_KP_2, GDK_J, GDK_j, GDK_D, GDK_d, 0 };
const guint up_keysyms[] = { GDK_Up, GDK_KP_Up, GDK_KP_8, GDK_K, GDK_k, GDK_E, GDK_e, 0 };
const guint right_keysyms[] = { GDK_Right, GDK_KP_Right, GDK_KP_6, GDK_L, GDK_l, GDK_F, GDK_f, 0 };

/* FIXME: make it possible to translate this */
const char* kb_resize_msg =
    "Use the arrow keys to resize.\n"
    "\n"
    "Press Enter to accept the change or Escape to cancel.";

const char* placement_msg =
    "Click to place '%s' in a new tile.\n"
    "\n"
    "Press Escape to cancel.";

/* event listener declarations */
static void on_resize_workarea(LuccaScreen* screen, gpointer user_data);

static void on_newtile(LuccaLayout* layout, LuccaLayoutTile* tile, gpointer user_data);

static gboolean on_motion(GtkWidget* widget, GdkEventMotion* event, gpointer user_data);

static gboolean on_buttondown(GtkWidget* widget, GdkEventButton* event, gpointer user_data);

static gboolean on_buttonup(GtkWidget* widget, GdkEventButton* event, gpointer user_data);

static gboolean on_expose(GtkWidget* widget, GdkEventExpose* event, gpointer user_data);

static void on_close_key(LuccaDisplay* display, gpointer user_data);

static void on_switch_key(LuccaDisplay* display, gpointer user_data);

static void on_reverse_switch_key(LuccaDisplay* display, gpointer user_data);

static void on_move_left_key(LuccaDisplay* display, gpointer user_data);

static void on_move_down_key(LuccaDisplay* display, gpointer user_data);

static void on_move_up_key(LuccaDisplay* display, gpointer user_data);

static void on_move_right_key(LuccaDisplay* display, gpointer user_data);

static void on_drag_left_key(LuccaDisplay* display, gpointer user_data);

static void on_drag_down_key(LuccaDisplay* display, gpointer user_data);

static void on_drag_up_key(LuccaDisplay* display, gpointer user_data);

static void on_drag_right_key(LuccaDisplay* display, gpointer user_data);

static void on_split_left_key(LuccaDisplay* display, gpointer user_data);

static void on_split_down_key(LuccaDisplay* display, gpointer user_data);

static void on_split_up_key(LuccaDisplay* display, gpointer user_data);

static void on_split_right_key(LuccaDisplay* display, gpointer user_data);

static void on_resize_key(LuccaDisplay* display, gpointer user_data);

static gboolean on_keypress(GtkWidget *widget, GdkEventKey *event, gpointer user_data);

static gboolean on_keyrelease(GtkWidget *widget, GdkEventKey *event, gpointer user_data);

static void on_disconnect(LuccaScreen* screen, gpointer user_data);

/* property id's */
enum {
    PROP_APPLICATION=1,
    PROP_SCREEN,
    PROP_LAYOUT,
    PROP_WINDOW,
};

static void lucca_workarea_init(GTypeInstance* instance, gpointer g_class) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(instance);

    workarea->dispose_has_run = FALSE;

    workarea->newtile_connect_id = 0;

    workarea->app = NULL;
    workarea->display = NULL;
    workarea->screen = NULL;
    workarea->layout = NULL;
    workarea->window = NULL;
    workarea->eventbox = NULL;
    
    workarea->tiles = NULL;
    
    workarea->is_resizing = FALSE;
    
    workarea->kb_resizing = FALSE;
    
    workarea->kb_instruction_window = NULL;
    
    workarea->dragging = FALSE;
    workarea->placing_tile = FALSE;
    workarea->placement_instruction_window = NULL;
    
    workarea->virtual_x = 0;
    workarea->virtual_y = 0;
}

static void lucca_workarea_dispose(GObject* obj) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(obj);
    GSList* item;
    
    if (workarea->dispose_has_run) {
        return;
    }
    workarea->dispose_has_run = TRUE;
    
    if (workarea->newtile_connect_id)
        g_signal_handler_disconnect(workarea->layout, workarea->newtile_connect_id);
    
    g_object_unref(workarea->app);
    g_object_unref(workarea->display);
    g_object_unref(workarea->screen);
    g_object_unref(workarea->layout);
    g_object_unref(workarea->window);
    g_object_unref(workarea->kb_instruction_window);
    g_object_unref(workarea->placement_instruction_window);
    
    for (item=workarea->tiles; item!=NULL; item=item->next) {
        g_object_unref(item->data);
    }
    
    G_OBJECT_CLASS(parent_class)->dispose(obj);
}

/* lucca_workarea_construct: called after all construction properties have been set */
static void lucca_workarea_construct(LuccaWorkArea* workarea) {
    LuccaDisplay* display;
    guint bordersize;
    guint x, y, width, height;
    GtkWindow* workarea_gtkwindow;
    GdkCursor* cursor;
    GClosure* callback;
    
    GdkGeometry tile_geometry;
    GdkWindowHints tile_geom_mask=0;
    
    gint i;
    
    GtkRequisition instruction_size;
    GtkWindow* instruction_gtkwindow;
    GtkWidget* label;
    GtkWidget* frame;
    
    tile_geometry.min_width = 70;
    tile_geometry.min_height = 70;
    tile_geom_mask |= GDK_HINT_MIN_SIZE;
    
    g_object_get(workarea->screen,
        "workarea-x", &x, 
        "workarea-y", &y, 
        "workarea-width", &width, 
        "workarea-height", &height, 
        NULL);
    
    display = lucca_screen_get_display(workarea->screen);
    
    workarea->display = g_object_ref(display);
    
    /* create a window for the work area */
    workarea->window = g_object_ref(lucca_display_create_internal(display));
    
    g_object_get(workarea->window, "window", &workarea_gtkwindow, NULL);

    workarea->eventbox = gtk_event_box_new();

    gtk_widget_show(workarea->eventbox);
    
    gtk_container_add(GTK_CONTAINER(workarea_gtkwindow), workarea->eventbox);

    lucca_window_configure(
        workarea->window,
        lucca_screen_get_root(workarea->screen), /* parent */
        x, y, width, height, /* x, y, width, height */
        TRUE /* visible */
        );

    /* create a layout for this screen */
    g_object_get(workarea->app, "border-size", &bordersize, NULL);
    
    workarea->layout = g_object_new(LUCCA_TYPE_LAYOUT, NULL);
    
    workarea->newtile_connect_id = g_signal_connect(workarea->layout, "new-tile", G_CALLBACK(on_newtile), workarea);
    
    g_signal_connect(workarea->eventbox, "motion-notify-event", G_CALLBACK(on_motion), workarea);
    g_signal_connect(workarea->eventbox, "button-press-event", G_CALLBACK(on_buttondown), workarea);
    g_signal_connect(workarea->eventbox, "button-release-event", G_CALLBACK(on_buttonup), workarea);
    g_signal_connect(workarea->eventbox, "expose-event", G_CALLBACK(on_expose), workarea);
    g_signal_connect(workarea->eventbox, "key-press-event", G_CALLBACK(on_keypress), workarea);
    g_signal_connect(workarea->eventbox, "key-release-event", G_CALLBACK(on_keyrelease), workarea);
    gtk_widget_add_events(GTK_WIDGET(workarea->eventbox), GDK_POINTER_MOTION_MASK|GDK_BUTTON1_MOTION_MASK|GDK_BUTTON_PRESS_MASK|GDK_BUTTON_RELEASE_MASK|GDK_KEY_PRESS_MASK);
    /* gtk_widget_add_events(GTK_WIDGET(workarea->eventbox), GDK_POINTER_MOTION_HINT_MASK); Disabled because we can't seem to get events properly with this mask. */
    g_object_unref(workarea_gtkwindow);
    
    cursor = gdk_cursor_new(GDK_FLEUR);
    
    gdk_window_set_cursor(GTK_WIDGET(workarea->eventbox)->window, cursor);
    
    gdk_cursor_unref(cursor);
    
    lucca_layout_initialize(workarea->layout, width, height, bordersize, tile_geometry, tile_geom_mask);
    
    g_signal_connect(workarea->screen, "resize-workarea", G_CALLBACK(on_resize_workarea), workarea);
    g_signal_connect(workarea->screen, "disconnect", G_CALLBACK(on_disconnect), workarea);
    
    /* set up hotkeys */
    callback = g_cclosure_new(G_CALLBACK(on_close_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    lucca_display_add_hotkey(display, GDK_F4, GDK_MOD1_MASK, callback);
    
    callback = g_cclosure_new(G_CALLBACK(on_switch_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    lucca_display_add_hotkey(display, GDK_Tab, GDK_MOD1_MASK, callback);
    
    callback = g_cclosure_new(G_CALLBACK(on_reverse_switch_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    lucca_display_add_hotkey(display, GDK_Tab, GDK_MOD1_MASK|GDK_SHIFT_MASK, callback);
    
    callback = g_cclosure_new(G_CALLBACK(on_move_left_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; left_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, left_keysyms[i], GDK_MOD1_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_move_down_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; down_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, down_keysyms[i], GDK_MOD1_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_move_up_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; up_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, up_keysyms[i], GDK_MOD1_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_move_right_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; right_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, right_keysyms[i], GDK_MOD1_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_drag_left_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; left_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, left_keysyms[i], GDK_MOD1_MASK|GDK_SHIFT_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_drag_down_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; down_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, down_keysyms[i], GDK_MOD1_MASK|GDK_SHIFT_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_drag_up_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; up_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, up_keysyms[i], GDK_MOD1_MASK|GDK_SHIFT_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_drag_right_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; right_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, right_keysyms[i], GDK_MOD1_MASK|GDK_SHIFT_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_split_left_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; left_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, left_keysyms[i], GDK_MOD1_MASK|GDK_CONTROL_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_split_down_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; down_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, down_keysyms[i], GDK_MOD1_MASK|GDK_CONTROL_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_split_up_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; up_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, up_keysyms[i], GDK_MOD1_MASK|GDK_CONTROL_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_split_right_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    for (i=0; right_keysyms[i] != 0; i++) {
        lucca_display_add_hotkey(display, right_keysyms[i], GDK_MOD1_MASK|GDK_CONTROL_MASK, callback);
    }
    
    callback = g_cclosure_new(G_CALLBACK(on_resize_key), workarea, NULL);
    g_closure_set_marshal(callback, g_cclosure_marshal_VOID__VOID);
    lucca_display_add_hotkey(display, GDK_R, GDK_MOD1_MASK, callback);
    
    /* create a window to display instructions when resizing with the keyboard */
    workarea->kb_instruction_window = lucca_display_create_internal(workarea->display);
    
    label = gtk_label_new(kb_resize_msg);
    gtk_widget_show(label);
    
    frame = gtk_frame_new(NULL);
    gtk_widget_show(frame);
    gtk_container_add(GTK_CONTAINER(frame), label);
    
    g_object_get(workarea->kb_instruction_window, "window", &instruction_gtkwindow, NULL);
    gtk_container_add(GTK_CONTAINER(instruction_gtkwindow), frame);

    lucca_window_configure(workarea->kb_instruction_window, lucca_screen_get_root(workarea->screen), 0, 0, 10, 10, FALSE);
    
    gtk_widget_size_request(frame, &instruction_size);
    
    g_object_get(workarea->screen, "width", &workarea->kb_instruction_x, "height", &workarea->kb_instruction_y, NULL);
    workarea->kb_instruction_x = workarea->kb_instruction_x / 2 - instruction_size.width / 2;
    workarea->kb_instruction_y = workarea->kb_instruction_y / 2 - instruction_size.height / 2;
    workarea->kb_instruction_width = instruction_size.width;
    workarea->kb_instruction_height = instruction_size.height;
    
    lucca_window_configure(workarea->kb_instruction_window, lucca_screen_get_root(workarea->screen), workarea->kb_instruction_x, workarea->kb_instruction_y, workarea->kb_instruction_width, workarea->kb_instruction_height, FALSE);
    
    g_object_unref(instruction_gtkwindow);
    
    /* create a window to display instructions when splitting tiles with the mouse */
    workarea->placement_instruction_window = lucca_display_create_internal(workarea->display);
    
    workarea->placement_instruction_label = gtk_label_new(placement_msg);
    gtk_widget_show(workarea->placement_instruction_label);
    
    workarea->placement_instruction_frame = gtk_frame_new(NULL);
    gtk_widget_show(workarea->placement_instruction_frame);
    gtk_container_add(GTK_CONTAINER(workarea->placement_instruction_frame), workarea->placement_instruction_label);
    
    g_object_get(workarea->placement_instruction_window, "window", &instruction_gtkwindow, NULL);
    gtk_container_add(GTK_CONTAINER(instruction_gtkwindow), workarea->placement_instruction_frame);
    
    lucca_window_configure(workarea->placement_instruction_window, lucca_screen_get_root(workarea->screen), 0, 0, 10, 10, FALSE);
    
    g_object_unref(instruction_gtkwindow);
}

static void lucca_workarea_set_property(GObject* object, guint property_id, const GValue* value, GParamSpec* pspec) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(object);
    
    switch (property_id) {
    case PROP_APPLICATION:
        workarea->app = g_object_ref(g_value_get_object(value));
        if (workarea->screen != NULL)
            lucca_workarea_construct(workarea);
        break;
    case PROP_SCREEN:
        workarea->screen = g_object_ref(g_value_get_object(value));
        if (workarea->app != NULL)
            lucca_workarea_construct(workarea);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_workarea_get_property(GObject* object, guint property_id, GValue* value, GParamSpec* pspec) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(object);
    
    switch (property_id) {
    case PROP_APPLICATION:
        g_value_set_object(value, workarea->app);
        break;
    case PROP_SCREEN:
        g_value_set_object(value, workarea->screen);
        break;
    case PROP_LAYOUT:
        g_value_set_object(value, workarea->layout);
        break;
    case PROP_WINDOW:
        g_value_set_object(value, workarea->window);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
        break;
    }
}

static void lucca_workarea_class_init(gpointer g_class, gpointer class_data) {
    LuccaWorkAreaClass* klass = (LuccaWorkAreaClass*)g_class;
    GObjectClass* gobclass = G_OBJECT_CLASS(g_class);
    GParamSpec *pspec;
    
    /* method overrides */
    gobclass->dispose = lucca_workarea_dispose;
    gobclass->set_property = lucca_workarea_set_property;
    gobclass->get_property = lucca_workarea_get_property;
    
    /* properties */
    pspec = g_param_spec_object(
        "application",
        "application",
        "The LuccaApplication that created this object",
        LUCCA_TYPE_APPLICATION,
        G_PARAM_READABLE|G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property(gobclass, PROP_APPLICATION, pspec);

    pspec = g_param_spec_object(
        "screen",
        "screen",
        "The LuccaScreen whose workarea this object manages",
        LUCCA_TYPE_SCREEN,
        G_PARAM_READABLE|G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property(gobclass, PROP_SCREEN, pspec);

    pspec = g_param_spec_object(
        "layout",
        "layout",
        "The LuccaLayout that controls this workarea's layout",
        LUCCA_TYPE_LAYOUT,
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_LAYOUT, pspec);

    pspec = g_param_spec_object(
        "window",
        "window",
        "The LuccaWindow used to display the contents of this workarea",
        LUCCA_TYPE_WINDOW,
        G_PARAM_READABLE);
    g_object_class_install_property(gobclass, PROP_WINDOW, pspec);

    parent_class = g_type_class_peek_parent(klass);
}

GType lucca_workarea_get_type() {
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (LuccaWorkAreaClass),
            NULL, /* base_init */
            NULL, /* base_finalize */
            lucca_workarea_class_init,
            NULL, /* class_finalize */
            NULL, /* class_data */
            sizeof (LuccaWorkArea),
            0, /* n_preallocs */
            lucca_workarea_init
        };
        type = g_type_register_static (G_TYPE_OBJECT, "LuccaWorkAreaType", &info, 0);
    }
    return type;
}

/* event listeners */

/* called when the work area is resized */
static void on_resize_workarea(LuccaScreen* screen, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    guint x, y, width, height;
    guint bordersize;
    
    g_object_get(workarea->app, "border-size", &bordersize, NULL);
    
    g_object_get(screen, "workarea-x", &x, "workarea-y", &y, "workarea-width", &width, "workarea-height", &height, NULL);

    lucca_window_configure(
        workarea->window,
        lucca_screen_get_root(workarea->screen), /* parent */
        x, y, width, height, /* x, y, width, height */
        TRUE /* visible */
        );

    lucca_layout_resize(workarea->layout, width, height, bordersize);
}

/* called when a tile is added to the layout */
static void on_newtile(LuccaLayout* layout, LuccaLayoutTile* layouttile, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    LuccaGraphicalTile* graphicaltile;
    
    graphicaltile = g_object_new(LUCCA_TYPE_GRAPHICALTILE, "workarea", workarea, "tile", layouttile, NULL);
    
    workarea->tiles = g_slist_prepend(workarea->tiles, graphicaltile);
}

/* called when a button is pressed on the workarea widget, usually at a border */
static gboolean on_buttondown(GtkWidget* widget, GdkEventButton* event, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    gint x, y;
    guint bordersize;
    
    x = (guint)event->x;
    y = (guint)event->y;
    
    /* if the first button was pressed on an area where there isn't a tile, this is a resize attempt */
    if ((event->button == 1) && (!workarea->is_resizing) && (!workarea->kb_resizing) && (lucca_layout_get_tile_at_position(workarea->layout, x, y) == NULL)) {
        GdkGrabStatus status;
        
        GList* i;
        GList* j;
    
        workarea->is_resizing = TRUE;
        
        workarea->resize_origin_x = x;
        workarea->resize_origin_y = y;
        
        bordersize = workarea->app->bordersize*3;
        
        /* identify the tiles that are close enough to be considered "along" this border */
        workarea->all_tile_candidates = lucca_layout_get_tiles_in_rectangle(workarea->layout, x-bordersize, y-bordersize, 2*bordersize+1, 2*bordersize+1);
        
        /* Compare every tile to every other tile. If tile A is fully to the left of any other tile B, and the user moves the cursor to the left, we know that the user cannot be making tile A larger (he would actually be making it smaller). We can make the same check for each cardinal direction. In any case, we'll use this information later to decide which tile is being resized in which directions. */
        workarea->top_tile_candidates = NULL;
        workarea->bottom_tile_candidates = NULL;
        workarea->left_tile_candidates = NULL;
        workarea->right_tile_candidates = NULL;
        for (i=workarea->all_tile_candidates; i!=NULL; i=i->next) {
            guint xi, yi, widthi, heighti;
            g_object_get(G_OBJECT(i->data), "x", &xi, "y", &yi, "width", &widthi, "height", &heighti, NULL);
            for (j=workarea->all_tile_candidates; j!=NULL; j=j->next) {
                guint xj, yj, widthj, heightj;
                g_object_get(G_OBJECT(j->data), "x", &xj, "y", &yj, "width", &widthj, "height", &heightj, NULL);
                
                if (yi+heighti < yj) { /* i is fully above j */
                    if (!g_slist_find(workarea->top_tile_candidates, i->data))
                        workarea->top_tile_candidates = g_slist_prepend(workarea->top_tile_candidates, i->data);
                }
                
                if (yj+heightj < yi) { /* i is fully below j */
                    if (!g_slist_find(workarea->bottom_tile_candidates, i->data))
                        workarea->bottom_tile_candidates = g_slist_prepend(workarea->bottom_tile_candidates, i->data);
                }
                
                if (xi+widthi < xj) { /* i is fully to the left of j */
                    if (!g_slist_find(workarea->left_tile_candidates, i->data))
                        workarea->left_tile_candidates = g_slist_prepend(workarea->left_tile_candidates, i->data);
                }
                
                if (xj+widthj < xi) { /* i is fully to the right of j */
                    if (!g_slist_find(workarea->right_tile_candidates, i->data))
                        workarea->right_tile_candidates = g_slist_prepend(workarea->right_tile_candidates, i->data);
                }
            }
        }
        
        workarea->resizing_tile = NULL;
        
        /* set grabs so we know if the user presses Escape or moves the mouse */
        status = gdk_pointer_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                                    FALSE, /* owner_events */
                                    GDK_POINTER_MOTION_MASK|GDK_BUTTON_RELEASE_MASK, /* event_mask */
                                    NULL, /* confine_to */
                                    NULL, /* cursor */
                                    GDK_CURRENT_TIME /* time_ */
                                    );
        /* FIXME: check status */

        status = gdk_keyboard_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                                    FALSE, /* owner_events */
                                    GDK_CURRENT_TIME /* time_ */
                                    );
        /* FIXME: check status */
        
        gtk_grab_add(GTK_WIDGET(workarea->eventbox));
        
        return TRUE;
    }
    
    return FALSE;
}

static void _calc_resize(LuccaGraphicalTile* tile, gint dx, gint dy, gint* x, gint* y, gint* width, gint* height) {
    guint layoutwidth, layoutheight;
    
    /* get the current position of the resized tile and layout size */
    g_object_get(tile->tile, "x", x, "y", y, "width", width, "height", height, NULL);
    g_object_get(tile->workarea->layout, "width", &layoutwidth, "height", &layoutheight, NULL);
    
    /* calculate the desired new size */
    if (dx > 0) {
        *width += dx;
        if (*x+*width > layoutwidth)
            *width = layoutwidth - *x;
    }
    else if (dx < 0) {
        if (*x+dx < 0)
            dx = -*x;
        *x += dx;
        *width -= dx;
    }
    if (dy > 0) {
        *height += dy;
        if (*y+*height > layoutheight)
            *height = layoutheight - *y;
    }
    else if (dy < 0) {
        if (*y+dy < 0)
            dy = -*y;
        *y += dy;
        *height -= dy;
    }
    
    /* get the expected result of resizing to that position */
    lucca_layouttile_resize_preview(tile->tile, x, y, width, height);
}

static void _calculate_placement(LuccaWorkArea* workarea, LuccaGraphicalTile** tile, LuccaSide* direction) {
    gint pointer_x, pointer_y;
    GdkModifierType mask;
    
    /* find the tile where the mouse is */
    gdk_window_get_pointer(workarea->eventbox->window, &pointer_x, &pointer_y, &mask);
    
    *tile = lucca_workarea_get_tile_at_position(workarea, pointer_x, pointer_y);
    
    if (*tile != NULL) {
        gint x, y, width, height;
        gint min_distance;
        
        g_object_get((*tile)->tile, "x", &x, "y", &y, "width", &width, "height", &height, NULL);
        
        /* find the closest edge */
        min_distance = (pointer_x - x)*height;
        *direction = LUCCA_SIDE_LEFT;
        
        if ((pointer_y - y)*width < min_distance) {
            min_distance = (pointer_y - y)*width;
            *direction = LUCCA_SIDE_TOP;
        }
        
        if ((width + x - pointer_x)*height < min_distance) {
            min_distance = (width + x - pointer_x)*height;
            *direction = LUCCA_SIDE_RIGHT;
        }
        
        if ((height + y - pointer_y)*width < min_distance) {
            min_distance = (height + y - pointer_y)*width;
            *direction = LUCCA_SIDE_BOTTOM;
        }
    }
}

static void _update_placement_preview(LuccaWorkArea* workarea) {
    LuccaGraphicalTile* tile;
    LuccaSide direction;
    
    _calculate_placement(workarea, &tile, &direction);
    
    if (tile != NULL) {
        gint x, y, width, height;
        GtkWindow* window;
        
        g_object_get(tile->tile, "x", &x, "y", &y, "width", &width, "height", &height, NULL);
        
        g_object_get(workarea->window, "window", &window, NULL);
        
        if (lucca_side_horizontal(direction)) {
            lucca_window_configure(workarea->placement_preview_bar, GTK_WIDGET(window)->window, x+(width+workarea->app->bordersize)/2, y, workarea->app->bordersize, height, TRUE);
        }
        else {
            lucca_window_configure(workarea->placement_preview_bar, GTK_WIDGET(window)->window, x, y+(height+workarea->app->bordersize)/2, width, workarea->app->bordersize, TRUE);
        }
        
        g_object_unref(window);
    }
    else {
        lucca_window_withdraw(workarea->placement_preview_bar);
    }
}

/* called when we get a pointer motion event; sometimes this indicates a resize */
static gboolean on_motion(GtkWidget* widget, GdkEventMotion* event, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    if (workarea->is_resizing) {
        gint x, y, dx, dy;
        gboolean positivedx, positivedy;
        LuccaGraphicalTile* new_resizing_tile = NULL;
        GList* i;
        gboolean resize_vertically, resize_horizontally;
        gint newx, newy, newwidth, newheight;
        
        x = (guint)event->x;
        y = (guint)event->y;
        
        dx = x - workarea->resize_origin_x;
        dy = y - workarea->resize_origin_y;
        
        positivedx = (dx > 0);
        positivedy = (dy > 0);
        
        /* Examine each tile candidate to try to find the one that is being made larger. We do this by attempting to prove that it would be made smaller. See on_buttondown above. */
        for (i=workarea->all_tile_candidates; i!=NULL; i=i->next) {
            if (positivedx) {
                /* cursor is moving to the right */
                if (g_slist_find(workarea->right_tile_candidates, i->data)) {
                    continue; /* i is on the right so it would be made smaller */
                }
                /* we only resize in this direction if i is to the left of something */
                if (g_slist_find(workarea->left_tile_candidates, i->data)) {
                    resize_horizontally = TRUE;
                }
                else {
                    resize_horizontally = FALSE;
                }
            }
            else {
                /* cursor is moving to the left */
                if (g_slist_find(workarea->left_tile_candidates, i->data)) {
                    continue; /* i is on the left so it would be made smaller */
                }
                /* we only resize in this direction if i is to the right of something */
                if (g_slist_find(workarea->right_tile_candidates, i->data)) {
                    resize_horizontally = TRUE;
                }
                else {
                    resize_horizontally = FALSE;
                }
            }
            if (positivedy) {
                /* cursor is moving down */
                if (g_slist_find(workarea->bottom_tile_candidates, i->data)) {
                    continue; /* i is on the bottom so it would be made smaller */
                }
                /* we only resize in this direction if i is above something */
                if (g_slist_find(workarea->top_tile_candidates, i->data)) {
                    resize_vertically = TRUE;
                }
                else {
                    resize_vertically = FALSE;
                }
            }
            else {
                /* cursor is moving up */
                if (g_slist_find(workarea->top_tile_candidates, i->data)) {
                    continue; /* i is above something so it would be made smaller */
                }
                /* we only resize in this direction if i is below something */
                if (g_slist_find(workarea->bottom_tile_candidates, i->data)) {
                    resize_vertically = TRUE;
                }
                else {
                    resize_vertically = FALSE;
                }
            }
            
            break;
        }
        
        if (i == NULL) { /* this should only be possible if the user clicked outsize the window area, but check for it anyway */
            /* tell the X server we want more motion events please
            gdk_event_request_motions(event); */
            return TRUE;
        }
        
        new_resizing_tile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(i->data));
        
        if (!resize_horizontally)
            dx = 0;
        if (!resize_vertically)
            dy = 0;
        
        /* calculate the new position */
        _calc_resize(new_resizing_tile, dx, dy, &newx, &newy, &newwidth, &newheight);
        
        /* if we're resizing a different tile from before, cancel the preview for the old tile */
        if (workarea->resizing_tile != new_resizing_tile && workarea->resizing_tile != NULL) {
            lucca_graphicaltile_cancel_preview(workarea->resizing_tile);
        }
        
        workarea->resizing_tile = new_resizing_tile;
        workarea->resizing_tile_newx = newx;
        workarea->resizing_tile_newy = newy;
        workarea->resizing_tile_newwidth = newwidth;
        workarea->resizing_tile_newheight = newheight;
        
        lucca_graphicaltile_resize_preview(new_resizing_tile, newx, newy, newwidth, newheight);
        
        /* tell the X server we want more motion events please
        gdk_event_request_motions(event); */
        
        return TRUE;
    }
    else if (workarea->dragging) {
        GtkRequisition size_request;
        
        gtk_widget_size_request(workarea->drag_titlebar_preview, &size_request);
        
        gtk_window_move(GTK_WINDOW(workarea->drag_titlebar_preview), (gint)event->x_root - size_request.width/2, (gint)event->y_root - size_request.height/2);
    }
    else if (workarea->placing_tile) {
        _update_placement_preview(workarea);
    }
    
    return FALSE;
}

/* called when a button is released after it was pressed on the workarea widget */
static gboolean on_buttonup(GtkWidget* widget, GdkEventButton* event, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    if (event->button == 1 && workarea->is_resizing) {
        if (workarea->resizing_tile)
            lucca_layouttile_resize(workarea->resizing_tile->tile, workarea->resizing_tile_newx, workarea->resizing_tile_newy, workarea->resizing_tile_newwidth, workarea->resizing_tile_newheight);
        
        workarea->is_resizing = FALSE;
        
        g_list_free(workarea->all_tile_candidates);
        g_slist_free(workarea->top_tile_candidates);
        g_slist_free(workarea->bottom_tile_candidates);
        g_slist_free(workarea->left_tile_candidates);
        g_slist_free(workarea->right_tile_candidates);
        
        gdk_pointer_ungrab(event->time);
        gdk_keyboard_ungrab(event->time);
        
        gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
        
        if (workarea->resizing_tile)
            lucca_graphicaltile_cancel_preview(workarea->resizing_tile);
        return TRUE;
    }
    else if (workarea->dragging) {
        LuccaGraphicalTile* drop_tile;
        gboolean valid;
        
        workarea->dragging = FALSE;
        
        drop_tile = lucca_workarea_get_tile_at_position(workarea, (gint)event->x, (gint)event->y);
        
        g_object_get(workarea->drag_window, "valid", &valid, NULL);
        
        if (valid && drop_tile != NULL && drop_tile != workarea->drag_tile) {
            lucca_workarea_take_cursor(workarea, drop_tile);
            
            lucca_graphicaltile_add_window(drop_tile, workarea->drag_window);
            lucca_graphicaltile_set_visible_window(drop_tile, workarea->drag_window);
        }
        
        gtk_widget_hide(workarea->drag_titlebar_preview);
        
        g_object_unref(workarea->drag_tile);
        g_object_unref(workarea->drag_window);
        g_object_unref(workarea->drag_titlebar_preview);
        
        gdk_pointer_ungrab(event->time);
        gdk_keyboard_ungrab(event->time);
        
        gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
    }
    else if (workarea->placing_tile) {
        LuccaGraphicalTile* tile;
        LuccaSide direction;
        gboolean valid;
        
        workarea->placing_tile = FALSE;
        
        g_object_get(workarea->place_window, "valid", &valid, NULL);
        
        _calculate_placement(workarea, &tile, &direction);
        
        if (valid && tile != NULL) {
            lucca_graphicaltile_split(tile, workarea->place_window, direction);
        }
        
        lucca_window_withdraw(workarea->placement_preview_bar);
        lucca_window_withdraw(workarea->placement_instruction_window);
        
        g_object_unref(workarea->place_window);
        /* FIXME: g_object_unref(workarea->placement_preview_bar); */
        
        gdk_pointer_ungrab(event->time);
        gdk_keyboard_ungrab(event->time);
        
        gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
    }
    
    return FALSE;
}

/* called when we need to paint this window */
static gboolean on_expose(GtkWidget* widget, GdkEventExpose* event, gpointer user_data) {
    gdk_draw_rectangle(widget->window, widget->style->fg_gc[GTK_STATE_NORMAL], TRUE, 0, 0, widget->allocation.width, widget->allocation.height);
    
    return TRUE;
}

/* called when alt+f4 is pressed */
static void on_close_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL && tile->visible_window != NULL)
        lucca_window_close(tile->visible_window);
}

/* called when alt+tab is pressed */
static void on_switch_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL)
        lucca_graphicaltile_cycle(tile, TRUE);
}

/* called when alt+shift+tab is pressed */
static void on_reverse_switch_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL)
        lucca_graphicaltile_cycle(tile, FALSE);
}

static void on_move_left_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL) {
        gint x;
        gint newx;
        GList* new_layouttiles;
        
        g_object_get(tile->tile, "x", &x, NULL);
        
        newx = x - workarea->app->bordersize - 1;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, newx, workarea->virtual_y-(workarea->app->bordersize/2), 1, workarea->app->bordersize);
        
        if (new_layouttiles != NULL) {
            tile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
            
            lucca_graphicaltile_focus(tile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_x = x;
        }
    }
}

static void on_move_down_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL) {
        gint y, height;
        gint newy;
        GList* new_layouttiles;
        
        g_object_get(tile->tile, "y", &y, "height", &height, NULL);
        
        newy = y + height + workarea->app->bordersize;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, workarea->virtual_x-(workarea->app->bordersize/2), newy, workarea->app->bordersize, 1);
        
        if (new_layouttiles != NULL) {
            tile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
            
            lucca_graphicaltile_focus(tile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_y = y + height - 1;
        }
    }
}

static void on_move_up_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL) {
        gint y;
        gint newy;
        GList* new_layouttiles;
        
        g_object_get(tile->tile, "y", &y, NULL);
        
        newy = y - workarea->app->bordersize - 1;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, workarea->virtual_x-(workarea->app->bordersize/2), newy, workarea->app->bordersize, 1);
        
        if (new_layouttiles != NULL) {
            tile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
            
            lucca_graphicaltile_focus(tile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_y = y;
        }
    }
}

static void on_move_right_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (tile != NULL) {
        gint x, width;
        gint newx;
        GList* new_layouttiles;
        
        g_object_get(tile->tile, "x", &x, "width", &width, NULL);
        
        newx = x + width + workarea->app->bordersize;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, newx, workarea->virtual_y-(workarea->app->bordersize/2), 1, workarea->app->bordersize);
        
        if (new_layouttiles != NULL) {
            tile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
            
            lucca_graphicaltile_focus(tile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_x = x + width - 1;
        }
    }
}

static void on_drag_left_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* currenttile;
    
    currenttile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (currenttile != NULL) {
        gint x;
        gint newx;
        GList* new_layouttiles;
        
        g_object_get(currenttile->tile, "x", &x, NULL);
        
        newx = x - workarea->app->bordersize - 1;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, newx, workarea->virtual_y-(workarea->app->bordersize/2), 1, workarea->app->bordersize);
        
        if (new_layouttiles != NULL) {
            LuccaGraphicalTile* newtile;
            LuccaWindow* window = currenttile->visible_window;
            
            newtile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
        
            lucca_workarea_take_cursor(workarea, newtile);
            
            if (window != NULL) {
                lucca_graphicaltile_add_window(newtile, window);
                
                lucca_graphicaltile_set_visible_window(newtile, window);
            }
            
            lucca_graphicaltile_focus(newtile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_x = x;
        }
    }
}

static void on_drag_down_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* currenttile;
    
    currenttile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (currenttile != NULL) {
        gint y, height;
        gint newy;
        GList* new_layouttiles;
        
        g_object_get(currenttile->tile, "y", &y, "height", &height, NULL);
        
        newy = y + height + workarea->app->bordersize;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, workarea->virtual_x-(workarea->app->bordersize/2), newy, workarea->app->bordersize, 1);
        
        if (new_layouttiles != NULL) {
            LuccaGraphicalTile* newtile;
            LuccaWindow* window = currenttile->visible_window;
            
            newtile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
        
            lucca_workarea_take_cursor(workarea, newtile);
            
            if (window != NULL) {
                lucca_graphicaltile_add_window(newtile, window);
                
                lucca_graphicaltile_set_visible_window(newtile, window);
            }
            
            lucca_graphicaltile_focus(newtile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_y = y + height - 1;
        }
    }
}

static void on_drag_up_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* currenttile;
    
    currenttile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (currenttile != NULL) {
        gint y;
        gint newy;
        GList* new_layouttiles;
        
        g_object_get(currenttile->tile, "y", &y, NULL);
        
        newy = y - workarea->app->bordersize - 1;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, workarea->virtual_x-(workarea->app->bordersize/2), newy, workarea->app->bordersize, 1);
        
        if (new_layouttiles != NULL) {
            LuccaGraphicalTile* newtile;
            LuccaWindow* window = currenttile->visible_window;
            
            newtile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
        
            lucca_workarea_take_cursor(workarea, newtile);
            
            if (window != NULL) {
                lucca_graphicaltile_add_window(newtile, window);
                
                lucca_graphicaltile_set_visible_window(newtile, window);
            }
            
            lucca_graphicaltile_focus(newtile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_y = y;
        }
    }
}

static void on_drag_right_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* currenttile;
    
    currenttile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    if (currenttile != NULL) {
        gint x, width;
        gint newx;
        GList* new_layouttiles;
        
        g_object_get(currenttile->tile, "x", &x, "width", &width, NULL);
        
        newx = x + width + workarea->app->bordersize;
        
        new_layouttiles = lucca_layout_get_tiles_in_rectangle(workarea->layout, newx, workarea->virtual_y-(workarea->app->bordersize/2), 1, workarea->app->bordersize);
        
        if (new_layouttiles != NULL) {
            LuccaGraphicalTile* newtile;
            LuccaWindow* window = currenttile->visible_window;
        
            newtile = lucca_workarea_get_graphicaltile(workarea, LUCCA_LAYOUTTILE(new_layouttiles->data));
            
            lucca_workarea_take_cursor(workarea, newtile);
            
            if (window != NULL) {
                lucca_graphicaltile_add_window(newtile, window);
                
                lucca_graphicaltile_set_visible_window(newtile, window);
            }
            
            lucca_graphicaltile_focus(newtile);
            
            g_list_free(new_layouttiles);
        }
        else {
            workarea->virtual_x = x + width - 1;
        }
    }
}

static void on_split_left_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    lucca_graphicaltile_split(tile, tile->visible_window, LUCCA_SIDE_LEFT);
}

static void on_split_down_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    lucca_graphicaltile_split(tile, tile->visible_window, LUCCA_SIDE_BOTTOM);
}

static void on_split_up_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    lucca_graphicaltile_split(tile, tile->visible_window, LUCCA_SIDE_TOP);
}

static void on_split_right_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    LuccaGraphicalTile* tile;
    
    tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    lucca_graphicaltile_split(tile, tile->visible_window, LUCCA_SIDE_RIGHT);
}

static void _do_kb_preview(LuccaWorkArea* workarea) {
    if (workarea->kb_resizing) {
        gint x, y, width, height;
        gint oldx, oldy, oldwidth, oldheight;
        
        /* find the resulting new size if this tile is resized */
        _calc_resize(workarea->kb_resize_tile, workarea->kb_resize_dx, workarea->kb_resize_dy, &x, &y, &width, &height);
        
        /* update the dx and dy values based on the actual result */
        g_object_get(workarea->kb_resize_tile->tile, "x", &oldx, "y", &oldy, "width", &oldwidth, "height", &oldheight, NULL);

        if (workarea->kb_resize_dx > 0 && (width - oldwidth + x - oldx) < workarea->kb_resize_dx) {
            workarea->kb_resize_dx = width - oldwidth + x - oldx;
        }
        else if (workarea->kb_resize_dx < 0 && (x - oldx) > workarea->kb_resize_dx) {
            workarea->kb_resize_dx = x - oldx;
        }
        if (workarea->kb_resize_dy > 0 && (height - oldheight + y - oldy) < workarea->kb_resize_dy) {
            workarea->kb_resize_dy = height - oldheight + y - oldy;
        }
        else if (workarea->kb_resize_dy < 0 && (y - oldy) > workarea->kb_resize_dy) {
            workarea->kb_resize_dy = y - oldy;
        }
        
        /* preview the tile */
        lucca_graphicaltile_resize_preview(workarea->kb_resize_tile, x, y, width, height);
    }
}

/* called when Alt+R is pressed */
static void on_resize_key(LuccaDisplay* display, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    GdkGrabStatus status;
    
    status = gdk_pointer_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                              FALSE, /* owner_events */
                              0, /* event_mask */
                              NULL, /* confine_to */
                              NULL, /* cursor */
                              GDK_CURRENT_TIME /* time_ */
                              );
    /* FIXME: check status */
    
    status = gdk_keyboard_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                               FALSE, /* owner_events */
                               GDK_CURRENT_TIME /* time_ */
                               );
    /* FIXME: check status */
    
    gtk_grab_add(GTK_WIDGET(workarea->eventbox));
    
    workarea->kb_resizing = TRUE;
    
    workarea->kb_resize_tile = lucca_workarea_get_tile_at_position(workarea, workarea->virtual_x, workarea->virtual_y);
    
    workarea->kb_resize_dx = 0;
    workarea->kb_resize_dy = 0;
    
    workarea->kb_keydown = FALSE;
    workarea->kb_size_amount = 1;
    
    _do_kb_preview(workarea);
    
    lucca_window_configure(workarea->kb_instruction_window, lucca_screen_get_root(workarea->screen), workarea->kb_instruction_x, workarea->kb_instruction_y, workarea->kb_instruction_width, workarea->kb_instruction_height, TRUE);
}

static gboolean _in_keysym_list(const guint* haystack, guint needle) {
    int i;
    
    for (i=0; haystack[i] != 0; i++) {
        if (haystack[i] == needle)
            return TRUE;
    }
    
    return FALSE;
}

static void _kb_resize_block(LuccaWorkArea* workarea, LuccaSide side) {
    guint x, y, width, height;
    guint oldx, oldy, oldwidth, oldheight;
    guint layoutwidth, layoutheight;
    
    /* get the current position of the resized tile and layout size */
    g_object_get(workarea->kb_resize_tile->tile, "x", &x, "y", &y, "width", &width, "height", &height, NULL);
    g_object_get(workarea->layout, "width", &layoutwidth, "height", &layoutheight, NULL);
    
    /* calculate the desired new size */
    if (workarea->kb_resize_dx > 0) {
        width += workarea->kb_resize_dx;
        if (x+width > layoutwidth)
            width = layoutwidth - x;
    }
    else if (workarea->kb_resize_dx < 0) {
        if (x+workarea->kb_resize_dx < 0)
            workarea->kb_resize_dx = -x;
        x += workarea->kb_resize_dx;
        width -= workarea->kb_resize_dx;
    }
    if (workarea->kb_resize_dy > 0) {
        height += workarea->kb_resize_dy;
        if (y+height > layoutheight)
            height = layoutheight - y;
    }
    else if (workarea->kb_resize_dy < 0) {
        if (y+workarea->kb_resize_dy < 0)
            workarea->kb_resize_dy = -y;
        y += workarea->kb_resize_dy;
        height -= workarea->kb_resize_dy;
    }
    
    /* get the expected result of resizing to that position */
    lucca_layouttile_resize_block(workarea->kb_resize_tile->tile, side, &x, &y, &width, &height);
        
    /* update the dx and dy values based on the result */
    g_object_get(workarea->kb_resize_tile->tile, "x", &oldx, "y", &oldy, "width", &oldwidth, "height", &oldheight, NULL);

    if (side == LUCCA_SIDE_RIGHT) {
        workarea->kb_resize_dx = width - oldwidth + x - oldx;
    }
    else if (side == LUCCA_SIDE_LEFT) {
        workarea->kb_resize_dx = x - oldx;
    }
    else if (side == LUCCA_SIDE_BOTTOM) {
        workarea->kb_resize_dy = height - oldheight + y - oldy;
    }
    else if (side == LUCCA_SIDE_TOP) {
        workarea->kb_resize_dy = y - oldy;
    }
}

/* called when the workarea's event box gets a keypress event; this will usually only happen if there's an active keyboard grab or focus is PointerRoot and the mouse is on a border */
static gboolean on_keypress(GtkWidget *widget, GdkEventKey *event, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    if (workarea->kb_resizing) {
        if (workarea->kb_keydown) /* accelerate if a key is held down */
            workarea->kb_size_amount = workarea->kb_size_amount + 1;

        if (event->keyval == GDK_Escape) { /* Cancel */
            lucca_window_configure(workarea->kb_instruction_window, lucca_screen_get_root(workarea->screen), workarea->kb_instruction_x, workarea->kb_instruction_y, workarea->kb_instruction_width, workarea->kb_instruction_height, FALSE);

            workarea->kb_resizing = FALSE;
            
            lucca_graphicaltile_cancel_preview(workarea->kb_resize_tile);
            
            gdk_pointer_ungrab(event->time);
            gdk_keyboard_ungrab(event->time);
            
            gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
            
            lucca_graphicaltile_focus(workarea->kb_resize_tile);
        }
        else if (event->keyval == GDK_Return) { /* Commit resize */
            gint x, y, width, height;
            
            lucca_window_configure(workarea->kb_instruction_window, lucca_screen_get_root(workarea->screen), workarea->kb_instruction_x, workarea->kb_instruction_y, workarea->kb_instruction_width, workarea->kb_instruction_height, FALSE);
            
            _calc_resize(workarea->kb_resize_tile, workarea->kb_resize_dx, workarea->kb_resize_dy, &x, &y, &width, &height);
            
            lucca_layouttile_resize(workarea->kb_resize_tile->tile, x, y, width, height);
            
            workarea->kb_resizing = FALSE;
            
            lucca_graphicaltile_cancel_preview(workarea->kb_resize_tile);
            
            gdk_pointer_ungrab(event->time);
            gdk_keyboard_ungrab(event->time);
            
            gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
            
            lucca_graphicaltile_focus(workarea->kb_resize_tile);
        }
        else if (event->state & GDK_CONTROL_MASK) { /* block resize */
            if (_in_keysym_list(left_keysyms, event->keyval)) {
                if (workarea->kb_resize_dx > 0)
                    workarea->kb_resize_dx = 0;
                else {
                    _kb_resize_block(workarea, LUCCA_SIDE_LEFT);
                }
                
                _do_kb_preview(workarea);
            }
            else if (_in_keysym_list(down_keysyms, event->keyval)) {
                if (workarea->kb_resize_dy < 0)
                    workarea->kb_resize_dy = 0;
                else {
                    _kb_resize_block(workarea, LUCCA_SIDE_BOTTOM);
                }
                
                _do_kb_preview(workarea);
            }
            else if (_in_keysym_list(up_keysyms, event->keyval)) {
                if (workarea->kb_resize_dy > 0)
                    workarea->kb_resize_dy = 0;
                else {
                    _kb_resize_block(workarea, LUCCA_SIDE_TOP);
                }
                
                _do_kb_preview(workarea);
            }
            else if (_in_keysym_list(right_keysyms, event->keyval)) {
                if (workarea->kb_resize_dx > 0)
                    workarea->kb_resize_dx = 0;
                else {
                    _kb_resize_block(workarea, LUCCA_SIDE_RIGHT);
                }
                
                _do_kb_preview(workarea);
            }
        }
        else {
            if (_in_keysym_list(left_keysyms, event->keyval)) {
                workarea->kb_resize_dx -= workarea->kb_size_amount;
                
                _do_kb_preview(workarea);
            }
            else if (_in_keysym_list(down_keysyms, event->keyval)) {
                workarea->kb_resize_dy += workarea->kb_size_amount;
                
                _do_kb_preview(workarea);
            }
            else if (_in_keysym_list(up_keysyms, event->keyval)) {
                workarea->kb_resize_dy -= workarea->kb_size_amount;
                
                _do_kb_preview(workarea);
            }
            else if (_in_keysym_list(right_keysyms, event->keyval)) {
                workarea->kb_resize_dx += workarea->kb_size_amount;
                
                _do_kb_preview(workarea);
            }
        }
    }
    else if (workarea->is_resizing) {
        if (event->keyval == GDK_Escape) {
            workarea->is_resizing = FALSE;
            
            g_list_free(workarea->all_tile_candidates);
            g_slist_free(workarea->top_tile_candidates);
            g_slist_free(workarea->bottom_tile_candidates);
            g_slist_free(workarea->left_tile_candidates);
            g_slist_free(workarea->right_tile_candidates);
            
            gdk_pointer_ungrab(event->time);
            gdk_keyboard_ungrab(event->time);
            
            gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
            
            if (workarea->resizing_tile)
                lucca_graphicaltile_cancel_preview(workarea->resizing_tile);
        }
    }
    else if (workarea->dragging) {
        if (event->keyval == GDK_Escape) {
            workarea->dragging = FALSE;
            
            gtk_widget_hide(workarea->drag_titlebar_preview);
            
            g_object_unref(workarea->drag_tile);
            g_object_unref(workarea->drag_window);
            g_object_unref(workarea->drag_titlebar_preview);
            
            gdk_pointer_ungrab(event->time);
            gdk_keyboard_ungrab(event->time);
            
            gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
        }
    }
    else if (workarea->placing_tile) {
        if (event->keyval == GDK_Escape) {
            workarea->placing_tile = FALSE;
            
            lucca_window_withdraw(workarea->placement_preview_bar);
            lucca_window_withdraw(workarea->placement_instruction_window);
            
            g_object_unref(workarea->place_window);
            /* FIXME: g_object_unref(workarea->placement_preview_bar); */
            
            gdk_pointer_ungrab(event->time);
            gdk_keyboard_ungrab(event->time);
            
            gtk_grab_remove(GTK_WIDGET(workarea->eventbox));
        }
    }
    
    workarea->kb_keydown = TRUE;
    
    return FALSE;
}

static gboolean on_keyrelease(GtkWidget *widget, GdkEventKey *event, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    workarea->kb_keydown = FALSE;
    workarea->kb_size_amount = 1;
    
    return FALSE;
}

static void on_disconnect(LuccaScreen* screen, gpointer user_data) {
    LuccaWorkArea* workarea = LUCCA_WORKAREA(user_data);
    
    lucca_window_withdraw(workarea->window);
    
    gtk_main_quit(); /* FIXME: this should be handled in the loader */
}

/* method definitions */
GList* lucca_workarea_get_tiles(LuccaWorkArea* workarea) {
    GList* result=NULL;
    GSList* item;
    
    for (item=workarea->tiles; item!=NULL; item=item->next) {
        result = g_list_prepend(result, item->data);
    }
    
    return result;
}

LuccaGraphicalTile* lucca_workarea_get_graphicaltile(LuccaWorkArea* workarea, LuccaLayoutTile* layouttile) {
    GSList* item;
    
    /* something more sophisticated than sequential search may be appropriate, but I am lazy */
    for (item=workarea->tiles; item!=NULL; item=item->next) {
        LuccaGraphicalTile* tile = (LuccaGraphicalTile*)item->data;
    
        if (tile->tile == layouttile)
            return tile;
    }
    
    return NULL;
}

LuccaGraphicalTile* lucca_workarea_get_tile_at_position(LuccaWorkArea* workarea, gint x, gint y) {
    LuccaLayoutTile* layouttile;
    
    layouttile = lucca_layout_get_tile_at_position(workarea->layout, x, y);
    
    if (layouttile != NULL) {
        return lucca_workarea_get_graphicaltile(workarea, layouttile);
    }
    else {
        return NULL;
    }
}

void lucca_workarea_take_cursor(LuccaWorkArea* workarea, LuccaGraphicalTile* tile) {
    gint x, y, width, height;
    
    g_object_get(tile->tile, "x", &x, "y", &y, "width", &width, "height", &height, NULL);
    
    if (x > workarea->virtual_x)
        workarea->virtual_x = x;
    if (x+width-1 < workarea->virtual_x)
        workarea->virtual_x = x+width-1;
    if (y > workarea->virtual_y)
        workarea->virtual_y = y;
    if (y+height-1 < workarea->virtual_y)
        workarea->virtual_y = y+height-1;
}

void lucca_workarea_start_drag(LuccaWorkArea* workarea, LuccaGraphicalTile* tile, LuccaWindow* window, guint32 timestamp) {
    GtkWidget* label;
    GtkWidget* frame;
    gchar* title;
    gint status;
    
    workarea->dragging = TRUE;
    workarea->drag_tile = g_object_ref(tile);
    workarea->drag_window = g_object_ref(window);
    workarea->drag_titlebar_preview = g_object_ref_sink(gtk_window_new(GTK_WINDOW_POPUP));
    
    g_object_get(window, "requested-title", &title, NULL);
    
    /* FIXME: make sure workarea->drag_titlebar_preview is on the correct screen */
    
    frame = gtk_frame_new(NULL);
    gtk_container_add(GTK_CONTAINER(workarea->drag_titlebar_preview), frame);
    
    label = gtk_label_new(title);
    gtk_container_add(GTK_CONTAINER(frame), label);
    
    gtk_window_set_position(GTK_WINDOW(workarea->drag_titlebar_preview), GTK_WIN_POS_MOUSE);
    
    status = gdk_pointer_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                                FALSE, /* owner_events */
                                GDK_POINTER_MOTION_MASK|GDK_BUTTON_MOTION_MASK|GDK_BUTTON_RELEASE_MASK, /* event_mask */
                                NULL, /* confine_to */
                                NULL, /* cursor */
                                timestamp /* time_ */
                                );
    /* FIXME: check status */

    status = gdk_keyboard_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                                FALSE, /* owner_events */
                                timestamp /* time_ */
                                );
    /* FIXME: check status */
    
    gtk_grab_add(GTK_WIDGET(workarea->eventbox));
    
    gtk_widget_show(label);
    gtk_widget_show(frame);
    gtk_widget_show(workarea->drag_titlebar_preview);
    
    gtk_widget_modify_bg(workarea->drag_titlebar_preview, GTK_STATE_NORMAL, &(workarea->drag_titlebar_preview->style->bg[GTK_STATE_SELECTED]));
    gtk_widget_modify_fg(workarea->drag_titlebar_preview, GTK_STATE_NORMAL, &(workarea->drag_titlebar_preview->style->fg[GTK_STATE_SELECTED]));
    
    gtk_widget_modify_bg(label, GTK_STATE_NORMAL, &(label->style->bg[GTK_STATE_SELECTED]));
    gtk_widget_modify_fg(label, GTK_STATE_NORMAL, &(label->style->fg[GTK_STATE_SELECTED]));
    
    g_free(title);
}

void lucca_workarea_start_addtile(LuccaWorkArea* workarea, LuccaWindow* window) {
    GtkWindow* preview_gtkwindow;
    gint status;
    gint x, y;
    gchar* title;
    gchar* instruction_msg;
    GtkRequisition instruction_size;
    
    workarea->placing_tile = TRUE;
    workarea->place_window = g_object_ref(window);
    workarea->placement_preview_bar = lucca_display_create_internal(workarea->display);
    
    g_object_get(workarea->placement_preview_bar, "window", &preview_gtkwindow, NULL);
    
    g_signal_connect(preview_gtkwindow, "expose-event", G_CALLBACK(on_expose), workarea);
    
    _update_placement_preview(workarea);
    
    g_object_get(window, "requested-title", &title, NULL);
    
    instruction_msg = g_strdup_printf(placement_msg, title);
    
    gtk_label_set_text(GTK_LABEL(workarea->placement_instruction_label), instruction_msg);
    
    g_free(title);
    g_free(instruction_msg);
    
    gtk_widget_size_request(workarea->placement_instruction_frame, &instruction_size);
    
    g_object_get(workarea->screen, "width", &x, "height", &y, NULL);
    x = x / 2 - instruction_size.width / 2;
    y = y / 2 - instruction_size.height / 2;
    
    lucca_window_configure(workarea->placement_instruction_window, lucca_screen_get_root(workarea->screen), x, y, instruction_size.width, instruction_size.height, TRUE);
    
    status = gdk_pointer_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                                FALSE, /* owner_events */
                                GDK_POINTER_MOTION_MASK|GDK_BUTTON_MOTION_MASK|GDK_BUTTON_RELEASE_MASK, /* event_mask */
                                NULL, /* confine_to */
                                NULL, /* cursor */
                                GDK_CURRENT_TIME /* time_ */
                                );
    /* FIXME: check status */

    status = gdk_keyboard_grab(GTK_WIDGET(workarea->eventbox)->window, /* window */
                                FALSE, /* owner_events */
                                GDK_CURRENT_TIME /* time_ */
                                );
    /* FIXME: check status */
    
    gtk_grab_add(GTK_WIDGET(workarea->eventbox));
    
    g_object_unref(preview_gtkwindow);
}

